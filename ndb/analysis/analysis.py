#!/usr/bin/env python
"""
Program to better understand filtering choices for NDB, via microbenchmarks.

Runs a combination of benchmarks and unit tests for these topology types:
- Fat Tree
- Tree
"""

import json
from optparse import OptionParser
import struct
import os
import sys
sys.path.append('../')

from mininet.topolib import TreeTopo
from ripl.dctopo import FatTreeTopo
import numpy as np
import matplotlib.pyplot as plt

from postcard_analysis import PostcardAnalysis
from filters import FILTER_ORDER, FILTERS, FILTER_PLOT_NAMES
from queries import DEF_QUERY_ORDER, all_to_all_queries, all_to_all_tree_queries
from queries import TREE_PAPER_QUERY_ORDER, QUERY_DISPLAY_NAMES
from queries import random_tree_queries
from analysis_lib import allocate_ips, allocate_dpids
from traffic import all_to_all_tm, random_tm
import plot_defaults
from clemson import ClemsonAnalysis, ClemsonSmallAnalysis, CLEMSON_QUERY_ORDER
from clemson import ClemsonNikhilAnalysis

plot_defaults.apply()

DEF_DATA_DIR = 'data'
DEF_PLOT_DIR = 'plot'

DATA_EXT = 'pca'  # Postcard analysis

SIMPLE_ANALYSES = [
    'tree_3sw_4h_aa',
    'tree_3sw_4h_random',
    'fattree_k4_aa'
]


# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'tree_3sw_4h_aa': (lambda: FourNodeTreeAllToAllAnalysis()),
    'tree_3sw_4h_random': (lambda: FourNodeTreeRandomAnalysis()),
    'fattree_k4_aa': (lambda: FatTreeAllToAllAnalysis()),
    'clemson': (lambda: ClemsonAnalysis()),
    'clemson_small': (lambda: ClemsonSmallAnalysis()),
    'clemson_nikhil': (lambda: ClemsonNikhilAnalysis())
}
ALL_DATA_ANALYSES = DATA_ANALYSIS_FCNS.keys()

QUERY_ORDERS = {
    'default': DEF_QUERY_ORDER,
    'tree_paper': TREE_PAPER_QUERY_ORDER,
    'clemson': CLEMSON_QUERY_ORDER,
}


# Plot-specific options
COLORS = ["r", "g", "b", "c", "m", "y", "k", "purple"]

PLOT_EXT = 'pdf'

DPI = 300


def parse_args():
    opts = OptionParser()
    opts.add_option("-g", "--generate",  action = "store_true",
                    default = False,
                    help = "generate data?")
    opts.add_option("-f", "--force", action = "store_true",
                    default = False,
                    help = "re-compute analysis even if data is there?")
    opts.add_option("-s", "--save_data", action = "store_true",
                    default = False,
                    help = "save generated data to files?")
    opts.add_option("--save_plot",  action = "store_true",
                    default = False,
                    help = "save plot?")
    opts.add_option("-p", "--show_plot",  action = "store_true",
                    default = False,
                    help = "show plot?")
    opts.add_option("-n", "--normalize", action = "store_true",
                    default = False,
                    help = "normalize plot data?")
    opts.add_option("-a", "--analyses", type = 'string',
                    default = None,
                    help = "comma-sep list of analyses in [%s]" % ALL_DATA_ANALYSES)
    opts.add_option("--query_order", type = 'string',
                    default = None,
                    help = "query order in [%s]" % QUERY_ORDERS.keys())
    opts.add_option("--data_dir", type = 'string',
                    default = DEF_DATA_DIR,
                    help = "directory for data files")
    opts.add_option("--plot_dir", type = 'string',
                    default = DEF_PLOT_DIR,
                    help = "directory for data files")
    options, arguments = opts.parse_args()

    # Validate analyses
    if not options.analyses:
        options.analyses = SIMPLE_ANALYSES
    else:
        options.analyses = options.analyses.split(',')
        for analysis in options.analyses:
            if analysis not in ALL_DATA_ANALYSES:
                raise Exception("Unknown analysis %s; must be in [%s]" % (analysis, ALL_DATA_ANALYSES))

    if not options.query_order:
        if options.analyses == ['clemson']:
            options.query_order = 'clemson'
        else:
            options.query_order = 'default'
    else:
        order = options.query_order
        if order not in QUERY_ORDERS.keys():
            raise Exception("Unknown query order %s; must be in [%s]" % (order, QUERY_ORDERS.keys()))
    options.query_order = QUERY_ORDERS[options.query_order]

    return options


def AllToAllPostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths, traffic, query_fcns):
    """All-to-all traffic matrix w/equal flows.
    
    t: Topo object
    host_to_ip: map of host names to IP 4B string
    sw_to_dpid: map of sw names to dpid uints
    path_lengths: string of paths lengths from one node
    traffic: unitless postcard rate
    fcns: postcard analysis functions, w/signature:
        fcn(t, path_lengths, leftmost, rightmost)
    """

    tm = all_to_all_tm(t, host_to_ip, traffic = 1)
    queries = []
    total = None
    for fcn in query_fcns:
        total, queries_to_add = fcn(t, path_lengths, sw_to_dpid, host_to_ip)
        queries += queries_to_add
    return PostcardAnalysis(t, [tm], [queries], 'shortest-path', FILTERS, total, sw_to_dpid)


def RandomPostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths, traffic, query_fcns, reps = 10):
    """Random traffic matrix w/equal flows.
    
    t: Topo object
    host_to_ip: map of host names to IP 4B string
    sw_to_dpid: map of sw names to dpid uints
    path_lengths: string of paths lengths from one node
    traffic: unitless postcard rate
    fcns: postcard analysis functions, w/signature:
        fcn(t, path_lengths, leftmost, rightmost)
    reps: number of repetitions
    """
    queries = []
    total = None
    for fcn in query_fcns:
        total, queries_to_add = fcn(t, path_lengths, sw_to_dpid, host_to_ip)
        queries += queries_to_add
    tms = []
    for rep in range(reps):
        tms.append(random_tm(t, host_to_ip, traffic = 1))
    return PostcardAnalysis(t, tms, [queries], 'shortest-path', FILTERS, total, sw_to_dpid)


def FourNodeTreeAllToAllAnalysis():
    """Simple four-host tree topology w/all-to-all traffic matrix."""
    t = TreeTopo(depth = 2, fanout = 2)
    host_to_ip = allocate_ips(t)
    sw_to_dpid = allocate_dpids(t)
    # All-to-all TM w/4 hosts yield one one-hop path, 2 3-hop paths.
    path_lengths = [1] + [3] * 2
    traffic = 1
    query_fcns = [all_to_all_queries, all_to_all_tree_queries]
    return AllToAllPostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths, traffic, query_fcns)


def FourNodeTreeRandomAnalysis():
    """Simple four-host tree topology w/all-to-one-random traffic matrix."""
    t = TreeTopo(depth = 2, fanout = 2)
    host_to_ip = allocate_ips(t)
    sw_to_dpid = allocate_dpids(t)
    # All-to-all TM w/4 hosts yield one one-hop path, 2 3-hop paths.
    path_lengths = [1] + [3] * 2
    traffic = 1
    query_fcns = [random_tree_queries]
    return RandomPostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths, traffic, query_fcns)


def FatTreeAllToAllAnalysis():
    """Fat tree topology w/all-to-all traffic matrix."""
    t = FatTreeTopo()
    host_to_ip = {}
    for i, h in enumerate(t.hosts()):
        ip_uint = FatTreeTopo.FatTreeNodeID(name = h).dpid + 0x0a000000
        ip_str = struct.pack('>I', ip_uint)
        host_to_ip[h] = ip_str
    sw_to_dpid = {}
    for i, sw in enumerate(t.switches()):
        sw_dpid = FatTreeTopo.FatTreeNodeID(name = sw).dpid
        sw_to_dpid[sw] = sw_dpid

    path_lengths = [1] + [3] * 2 + [5] * 12
    traffic = 1
    query_fcns = [all_to_all_queries]
    return AllToAllPostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths, traffic, query_fcns)


def load_data(data_dir):
    if os.path.exists(data_dir):
        data = {}  # data[analysis filename] = PostcardAnalysis results dict
        print "Reading data from %s" % data_dir
        for filename in os.listdir(data_dir):
            found_data = False
            ext = os.path.splitext(filename)[1]
            print "Loading %s" % filename
            if DATA_EXT in filename:
                found_data = True
                with open(os.path.join(data_dir, filename), 'r') as f:
                    data[filename] = json.load(f)
            if not found_data:
                return None
        return data
    else:
        return None


def generate_data(data_dir, analyses):
    data = {}  # data[analysis filename] = PostcardAnalysis results dict
    for analysis_name in analyses:
        fcn = DATA_ANALYSIS_FCNS[analysis_name]
        print "Generating data: %s()" % analysis_name
        data[analysis_name] = fcn()
    return data


def save_data(data_dir, data):
    print "Writing data to %s" % data_dir
    if not data:
        raise Exception("no data to write")
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    for filename, vals in data.iteritems():
        full_filename = os.path.join(data_dir, filename + '.' + DATA_EXT)
        with open(full_filename, 'w') as f:
            json.dump(vals, f, indent = 4)
            print "Wrote %s" % filename


def plot_data(plot_dir, data, show_plot, save_plot, normalize, query_order,
             filter_order = FILTER_ORDER):
    """
    
    NOTE: Query order is the actual order of queries, not just the name.
    """
    print "Plotting graphs to %s" % plot_dir
    for analysis_name, analysis_data in data.iteritems():
        print "Plotting data: %s" % analysis_name
        #print data['fournodetree_aa']['opt']['null']
    
        fig = plt.figure()
        fig.set_size_inches(15, 3.4)
        ax = fig.add_subplot(111)

        # Check that each filter in the data set is in the ordering.
        filter_list = analysis_data.itervalues().next()
        for filter in filter_list:
            if filter not in filter_order:
                raise Exception("Filter %s in data set not in FILTER_ORDER; please add." % filter)
        filter_plot_order = [f for f in filter_order if (f in filter_list)]
        assert len(filter_list) == len(filter_plot_order)

        # Check that each query in the data set is in the ordering.
        # Commented out; maybe I want a standard query with fewer displays.
        query_list = analysis_data.keys()
        #for query_name in query_list:
        #    if query_name not in query_order:
        #        raise Exception("Query %s in data set not in query_order; please add." % query_name)
        query_plot_order = [q for q in query_order if q in query_list]
        #assert len(query_list) == len(query_plot_order)

        #N = len(analysis_data)
        N = len(query_plot_order)
        ind = np.arange(N)  # the x locations for the groups
        total_bar_width = 0.5
        each_bar_width = total_bar_width / len(filter_plot_order) # the width of the bars
        group_x_shift = (1.0 - total_bar_width) / 2
        # With rotated text, helps to slightly shift the names over so they
        # apear to align better with the bars above.
        manual_x_shift = each_bar_width
        rects = []
        highest_total = 0
        for i, filter in enumerate(filter_plot_order):
            height_fcn = np.mean
            err_height_fcn = np.std
            heights = [height_fcn(analysis_data[query][filter]) for query in query_plot_order]
            err_heights = [err_height_fcn(analysis_data[query][filter]) for query in query_plot_order]
            total = max(analysis_data['none']['null'])
            if normalize:
                heights = [float(height) / total for height in heights]
                err_heights = [float(err_height) / total for err_height in err_heights]
                total = 1.0
            if total > highest_total:
                highest_total = total
            #print data['fournodetree_aa']['any']['null']
            # list of err vals:
            # http://matplotlib.org/api/axes_api.html#matplotlib.axes.Axes.errorbar
            bar = ax.bar(ind + (each_bar_width * i + group_x_shift), heights, 
                         each_bar_width,
                         yerr = err_heights, ecolor='black', error_kw={'elinewidth':0.8},
                         color = COLORS[i])
            rects.append(bar)

        if normalize:
            y_unit = 'fraction'
        else:
            y_unit = 'number'
        ax.set_ylabel('%s of postcards' % y_unit)
        #ax.set_title('%s of postcards, by query and optimization' % y_unit)
        ax.set_xticks(ind + total_bar_width * 0.5 + group_x_shift + manual_x_shift)
        query_display_names = []
        for q in query_plot_order:
            if q in QUERY_DISPLAY_NAMES.keys():
                query_display_names.append(QUERY_DISPLAY_NAMES[q])
            else:
                query_display_names.append(q)
        ax.set_xticklabels(query_display_names, rotation = 45, horizontalalignment = 'right' )
        #ax.set_xlabel('queries')
        max_y = highest_total

        # MPL: push legend off to the side.
        # from http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
        # Shrink current axis by 10%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.89, box.height])

        # Put a legend to the right of the current axis
        filter_plot_names = [FILTER_PLOT_NAMES[f] for f in filter_plot_order]
        ax.legend([rect[0] for rect in rects], filter_plot_names, loc = 'center left',
                  title = 'postcard filter types', bbox_to_anchor = (1, 0.25))

        ax.set_ylim([0, max_y])

        if save_plot:
            if not os.path.exists(plot_dir):
                os.makedirs(plot_dir)
            filepath = os.path.join(plot_dir, analysis_name + '.' + PLOT_EXT)
            print "Writing file to %s" % filepath
            fig.savefig(filepath, dpi = DPI)
        if show_plot:
            plt.show()

        plt.close(fig)


def main():
    options = parse_args()
    data = load_data(options.data_dir)
    # Ensure that we have some data first
    if (data and options.force) or options.generate:
        data = generate_data(options.data_dir, options.analyses)
        if options.save_data:
            save_data(options.data_dir, data)
    elif data is None:
        print "No data input provided; exiting."
        exit()

    assert data is not None

    if options.show_plot or options.save_plot:
        plot_data(options.plot_dir, data, options.show_plot, options.save_plot,
                  options.normalize, options.query_order)


if __name__ == '__main__':
    main()
