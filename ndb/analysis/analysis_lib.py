"""
Random stuff put in here, including topology stuff.
"""
from collections import defaultdict
import string
import struct

# Why does this work?
# See http://blogs.fluidinfo.com/terry/2012/05/26/autovivification-in-python-nested-defaultdicts-with-a-specific-final-type/
def autodict():
     return defaultdict(autodict)


def allocate_ips(t):
    """Returns map of host names to IPs, given Topo object."""
    # TODO: see if we can reuse the dpid mapping Mininet would use.
    host_to_ip = {}
    for i, h in enumerate(t.hosts()):
        host_to_ip[h] = get_ip(i)
    return host_to_ip


def allocate_dpids(t):
    """Return mapping from switch string names to DPIDs, given Topo object."""
    # TODO: see if we can reuse the dpid mapping Mininet would use.
    sw_to_dpid = {}
    for i, sw in enumerate(t.switches()):
        sw_to_dpid[sw] = i + 1
    return sw_to_dpid


def get_ip(index):
    """Convert an index into a valid IP 4B string. Shifts by one."""
    assert index < 255
    return "\x0a\x00\x00" + struct.pack('B', index + 1)


def ip_dd(ip):
    """Convert 4B IP digits string to dotted-decimal string for BPF use."""
    return string.join(["%d" % ord(ip) for ip in ip],'.')