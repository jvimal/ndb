#!/usr/bin/env python

import sys
from collections import defaultdict 

import dpkt
import ipaddr
from mininet.topo import Topo

from analysis_lib import ip_dd, autodict
from traffic import DpktFlow
from queries import process_queries
from filters import FILTERS
from postcard_analysis import PostcardAnalysis

# Change both at the same time.
# Get this from 'capinfos $filename'.
# Total pkts: 8050  
# Number we can parse (currently): ipv4 pkts
NUM_PKTS_IN_TRACE = 7867

DEF_TRACE = 'pcaps/clemson/core-capture.cap.1M.pcap'
SMALL_TRACE = 'pcaps/clemson/core-capture.cap.100K.pcap'

CLEMSON_SW_TO_DPID = {
    'core': 1,
    'dc': 2,
    'campus': 3,
    'internet': 4,
    'wireless': 2, # Not really a switch, but whatever.
}

CLEMSON_AREAS = ['dc', 'campus', 'internet']

# special wireless controller(s).
wireless_nets = ['172.19.3.0/24']

dc_nets = [
    '130.127.8.0/24',
    '130.127.9.0/24',
    '130.127.10.0/24',
    '130.127.11.0/24',
    '130.127.28.0/24',
    '130.127.69.0/24',
    '130.127.60.0/24',
    '130.127.235.0/24',
    '130.127.236.0/24',
    '130.127.237.0/24',
    '130.127.238.0/24',
    '130.127.239.0/24',
    '130.127.240.0/24',
    '130.127.241.0/24',
    '130.127.239.0/24',
    '130.127.201.0/24',
    '130.127.255.250/32',
    '130.127.255.251/32',
    '130.127.255.246/32',
    '130.127.255.247/32',
    '130.127.255.248/32',
] + wireless_nets

campus_nets = [
    '130.127.0.0/16',
    '172.19.36.47/32'
]


# Everything else is to the internet.

dc_nets_objs = [ipaddr.IPv4Network(a) for a in dc_nets]
campus_nets_objs = [ipaddr.IPv4Network(a) for a in campus_nets]

dc_ipv6_nets = [
    '2620:103:a000::0/48'
]

# Special wireless controller
wireless_ipv6_net = '2620:103:a002::0/48'
campus_ipv6_nets = [
    '2620:103:a004::0/48',
    wireless_ipv6_net,
]


def hostify(name):
    """Given raw location (e.g. 'campus'), return corresponding host."""
    return "h_%s" % name


"""
Define Clemson topo for the purposes of simple path-length analysis.

NOTE: This topo is wrong.  It's deliberately simplified for now.
A correct once would "go to the edge", that is, it would add the extra hops
from the distribution  switches in the DCs and campus to the 6509's at the
edge.
"""
class ClemsonTopo(Topo):
    def __init__(self, **opts):
        super(ClemsonTopo, self).__init__(**opts)
        areas = ['dc', 'campus', 'internet']
        for area in ['core'] + areas:
            #self.add_switch(area)
            self.addSwitch(area)
        # Create a single host per area, directly connected, to re-use TM
        # code that assumes we use named hosts to enumerate paths.
        # Without this, it becomes harder to create the full postcard path,
        # because we won't know the first-hop-switch ingress and
        # last-hop-switch egress ports.
        for area in areas:
            self.addHost(hostify(area))
        for area in areas:
            self.addLink('core', area)
            self.addLink(hostify(area), area)


def ipv4_in_net(ip, nets_objs):
    ip_obj = ipaddr.IPv4Address(ip)
    for net_obj in nets_objs:
        if ip_obj in net_obj:
            return True
    return False


def locate_ipv4(ip):
    """Given ip addr, return location as string."""
    if ipv4_in_net(ip, dc_nets_objs):
        return 'dc'
    elif ipv4_in_net(ip, campus_nets_objs):
        return 'campus'
    else:
        return 'internet'


def parse_clemson_trace(filename, fcn):
    """Parse 'filename' and apply fcn to each valid packet"""

    f = open(filename)
    pcap = dpkt.pcap.Reader(f)
    
    SEP = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    PRINT_INTERVAL = 10000
    
    valid = 0
    pkts = 0
    errors = defaultdict(int)
    
    # Track # ips per location (dc, campus, ...)
    locations = defaultdict(int)
    
    # Track # pkts per Eth prototype (IPv4, IPv6, other)
    protos = defaultdict(int)

    for ts, buf in pcap:
        try:
            eth = dpkt.ethernet.Ethernet(buf)
            valid += 1
    
            if eth.type == dpkt.ethernet.ETH_TYPE_IP:
                ip = eth.data
                src = ip.src
                dst = ip.dst
                for ip_addr in [src, dst]:
                    # ipaddr constructor expects dotted-decimal strs or uints.
                    locations[locate_ipv4(ip_dd(ip_addr))] += 1
                # Only put this _after_ all checks; could have an exception.
                protos['ip'] += 1

                # Should really be outside the IP block.
                if fcn:
                    try:
                        fcn(eth)
                    except:
                        print "Unexpected error:", sys.exc_info()
                        raise Exception("WTF?  Should have no errors here.")
            
            elif eth.type == dpkt.ethernet.ETH_TYPE_IP6:
                protos['ip6'] += 1
            else:
                protos['other'] += 1



        except ipaddr.AddressValueError as ave:
            errors['AddressValueError'] += 1
        except ValueError as ve:
            errors['ValueError'] += 1
        except IndexError as ie:
            errors['IndexError'] += 1
    
        pkts += 1
        if pkts % PRINT_INTERVAL == 0:
            print '.',
            # Force screen output; otherwise, no output.
            sys.stdout.flush()
    
    f.close()
    
    if pkts >= PRINT_INTERVAL:
        print
    
    print "total pkts: %s" % pkts
    print "valid pkts: %s" % valid
    
    print SEP
    for err, count in errors.iteritems():
        print "%s pkts: %s" % (err, count)
    
    print SEP
    for location, count in locations.iteritems():
        print "%s pkts: %s" % (location, count)
    
    print SEP
    for proto, count in protos.iteritems():
        print "%s pkts: %s" % (proto, count)


def clemson_tm(filename, traffic):
    """Return all-to-all traffic matrix for use in PostcardAnalysis.

    filename: name of file from which to read the packets
    traffic: unitless integer
    """
    def return_ipv4_flow(eth, tm, traffic):
        if eth.type == dpkt.ethernet.ETH_TYPE_IP:
            ip = eth.data
            src = ip.src
            dst = ip.dst
            # ipaddr constructor expects dotted-decimal strs or uints.
            src_loc = hostify(locate_ipv4(ip_dd(src)))
            dst_loc = hostify(locate_ipv4(ip_dd(dst)))
            if src_loc not in tm:
                tm[src_loc] = {}
            if dst_loc not in tm[src_loc]:
                tm[src_loc][dst_loc] = []
            tm[src_loc][dst_loc].append(DpktFlow(traffic, eth))

    # tm[src][dst] = list of Flow objects.
    tm = autodict()
    flows = parse_clemson_trace(filename, lambda eth: return_ipv4_flow(eth, tm, traffic))
    #print tm
    return tm


def bpf_subnets_or(subnets, prepend = ''):
    return "(" + " or ".join([prepend + " net %s" % n for n in subnets]) + ")"

def bpf(area, prepend = ''):
    """Given the name of an area, return a BPF string.
    
    prepend: string to put in from of bfp, such as src or dst.
    """
    s = ''
    if area == 'wireless':
        return bpf_subnets_or(wireless_nets, prepend)        
    elif area == 'dc':
        return bpf_subnets_or(dc_nets, prepend)
    elif area == 'campus':
        # Campus but not DC.
        return 'not ' + bpf_subnets_or(dc_nets, prepend) + \
            ' and ' + bpf_subnets_or(campus_nets, prepend)
    else:
        # Everything else!
        return 'not ' + bpf_subnets_or(dc_nets, prepend) + \
            ' and not ' + bpf_subnets_or(campus_nets, prepend)


def bpf_src_dst(src, dst):
    """Lock down the traffic between two areas as a single BPF."""
    return bpf(src, 'src') + ' and ' + bpf(dst, 'dst')

def clemson_queries_nikhil(t, path_lengths, sw_to_dpid, host_to_ip):
    """
    NOTE: nothing beside sw_to_dpid is actually used here.
    """
    host1 = '172.19.3.83' # 1071 pkts in .1M trace
    # 4 pkts w/the host, all to 85.x.x.x (net).  Hence, all pathlens = 3.
    host2 = '130.127.120.221'
    host2_opt_pcards = 12  # 4 * 3
    # Wireless:
    # Should be on the order of 5262
    # tcpdump -r core-capture.cap.1M.pcap net 172.19.3.0/24 | wc -l
    # Wireshark filter:
    # ip.addr==172.19.3.0/24: 5262 pkts displayed.
    # 

    # Total hack!!!!  Worst-case for everything.
    # TODO: fix this
    total = NUM_PKTS_IN_TRACE * 3

    def area_area_header_query_dir(one, two):
        this_bpf = bpf_src_dst(one, two)
        return [
            '%s_hdr-%s_hdr' % (one, two),
            '{{--bpf "%s"}}.*{{ --bpf "%s"}}$' %
            (this_bpf, this_bpf),
            {}
         ]

    def area_area_loc_query_dir(one, two):
        return [
            '%s_loc-%s_loc' % (one, two),
            '{{--dpid %s}}.*{{--dpid %s}}$' %
            (sw_to_dpid[one], sw_to_dpid[two]),
            {}
         ]

    def area_area_both_query_dir(one, two):
        this_bpf = bpf_src_dst(one, two)
        return [
            '%s_both-%s_both' % (one, two),
            '{{--dpid %s --bpf "%s"}}.*{{--dpid %s --bpf "%s"}}$' %
            (sw_to_dpid[one], this_bpf, sw_to_dpid[two], this_bpf),
            {}
         ]

    queries = [
        ['none', '$', {}],
        ['hostip-%s' % host2, '{{--bpf "host %s"}}.*' % host2, {
            'opt': host2_opt_pcards,
            'ipred_notifies': host2_opt_pcards,
            'ipred_pcards': host2_opt_pcards,
         }],
        ['campus', '{{--bpf "%s"}}.*' % bpf('campus'), {}],
    ]

    queries.append(area_area_loc_query_dir('dc', 'internet'))

    process_queries(queries)
    for q in queries:
        name, phf, expected = q
        #print "name: %s, phf: %s" % (name, phf)
    return total, queries


def clemson_queries(t, path_lengths, sw_to_dpid, host_to_ip):
    """
    NOTE: nothing beside sw_to_dpid is actually used here.
    """
    host1 = '172.19.3.83' # 1071 pkts in .1M trace
    # 4 pkts w/the host, all to 85.x.x.x (net).  Hence, all pathlens = 3.
    host2 = '130.127.120.221'
    host2_opt_pcards = 12  # 4 * 3
    # Wireless:
    # Should be on the order of 5262
    # tcpdump -r core-capture.cap.1M.pcap net 172.19.3.0/24 | wc -l
    # Wireshark filter:
    # ip.addr==172.19.3.0/24: 5262 pkts displayed.
    # 

    # Total hack!!!!  Worst-case for everything.
    # TODO: fix this
    total = NUM_PKTS_IN_TRACE * 3

    def area_area_header_query_dir(one, two):
        this_bpf = bpf_src_dst(one, two)
        return [
            '%s_hdr-%s_hdr' % (one, two),
            '{{--bpf "%s"}}.*{{ --bpf "%s"}}$' %
            (this_bpf, this_bpf),
            {}
         ]

    def area_area_loc_query_dir(one, two):
        return [
            '%s_loc-%s_loc' % (one, two),
            '{{--dpid %s}}.*{{--dpid %s}}$' %
            (sw_to_dpid[one], sw_to_dpid[two]),
            {}
         ]

    def area_area_both_query_dir(one, two):
        this_bpf = bpf_src_dst(one, two)
        return [
            '%s_both-%s_both' % (one, two),
            '{{--dpid %s --bpf "%s"}}.*{{--dpid %s --bpf "%s"}}$' %
            (sw_to_dpid[one], this_bpf, sw_to_dpid[two], this_bpf),
            {}
         ]

    queries = [
        ['none', '$', {}],
        ['any', '.*', {}],
        ['hostip-%s' % host1, '{{--bpf "host %s"}}.*' % host1, {}],
        ['hostip-%s' % host2, '{{--bpf "host %s"}}.*' % host2, {
            'opt': host2_opt_pcards,
            'ipred_notifies': host2_opt_pcards,
            'ipred_pcards': host2_opt_pcards,
         }],
        ['wireless', '{{--bpf "%s"}}.*' % bpf('wireless'), {}],
        ['dc', '{{--bpf "%s"}}.*' % bpf('dc'), {}],
        ['campus', '{{--bpf "%s"}}.*' % bpf('campus'), {}],
        area_area_both_query_dir('campus', 'wireless'),
        area_area_both_query_dir('wireless', 'campus'),
        area_area_both_query_dir('internet', 'wireless'),
        area_area_both_query_dir('wireless', 'internet'),
        # Below are effectively unit-test queries; all should be equal to
        # 3 in length, because of our vantage point.
        #['path_len >= 1', '.{%s}.*' % 1, {}],
        #['path_len >= 2', '.{%s}.*' % 2, {}],
        #['path_len >= 3', '.{%s}.*' % 3, {}],
    ]
    query_fcns = [
        area_area_header_query_dir,
        area_area_loc_query_dir,
        area_area_both_query_dir
    ]
    for a in CLEMSON_AREAS:
        for b in CLEMSON_AREAS:
            if a != b:
                for query_fcn in query_fcns:
                    queries.append(query_fcn(a, b))

    process_queries(queries)
    for q in queries:
        name, phf, expected = q
        #print "name: %s, phf: %s" % (name, phf)
    return total, queries


def clemson_queries_small(t, path_lengths, sw_to_dpid, host_to_ip):
    """
    NOTE: nothing beside sw_to_dpid is actually used here.
    """
    host1 = '172.19.3.83' # 1071 pkts in .1M trace
    # 4 pkts w/the host, all to 85.x.x.x (net).  Hence, all pathlens = 3.
    host2 = '130.127.120.221'
    host2_opt_pcards = 12  # 4 * 3
    # Wireless:
    # Should be on the order of 5262
    # tcpdump -r core-capture.cap.1M.pcap net 172.19.3.0/24 | wc -l
    # Wireshark filter:
    # ip.addr==172.19.3.0/24: 5262 pkts displayed.
    # 

    # Total hack!!!!  Worst-case for everything.
    # TODO: fix this
    total = NUM_PKTS_IN_TRACE * 3

    queries = [
        ['none', '$', {}],
        ['any', '.*', {}],
        ['hostip-%s' % host1, '{{--bpf "host %s"}}.*' % host1, {}],
#        ['hostip-%s' % host2, '{{--bpf "host %s"}}.*' % host2, {
#            'opt': host2_opt_pcards,
#            'ipred_notifies': host2_opt_pcards,
#            'ipred_pcards': host2_opt_pcards,
#         }],
#        ['wireless', '{{--bpf "%s"}}.*' % bpf('wireless'), {}],
#        ['dc', '{{--bpf "%s"}}.*' % bpf('dc'), {}],
#        ['campus', '{{--bpf "%s"}}.*' % bpf('campus'), {}],
#        area_area_both_query_dir('campus', 'wireless'),
#        area_area_both_query_dir('wireless', 'campus'),
#        area_area_both_query_dir('internet', 'wireless'),
#        area_area_both_query_dir('wireless', 'internet'),
        # Below are effectively unit-test queries; all should be equal to
        # 3 in length, because of our vantage point.
        #['path_len >= 1', '.{%s}.*' % 1, {}],
        #['path_len >= 2', '.{%s}.*' % 2, {}],
        #['path_len >= 3', '.{%s}.*' % 3, {}],
    ]

    process_queries(queries)
    for q in queries:
        name, phf, expected = q
        #print "name: %s, phf: %s" % (name, phf)
    return total, queries


def clemson_queries_names_only():
    total, queries = clemson_queries(None, None, CLEMSON_SW_TO_DPID, None)
    return [name for (name, phf, expected) in queries]


CLEMSON_QUERY_ORDER = clemson_queries_names_only()


def ClemsonTracePostcardAnalysis(t, host_to_ip, sw_to_dpid, path_lengths,
                                 traffic, query_fcns, filename = DEF_TRACE):
    """Trace-driven tm.
    
    t: Topo object
    host_to_ip: map of host names to IP 4B string
    sw_to_dpid: map of sw names to dpid uints
    path_lengths: string of paths lengths from one node
    traffic: unitless postcard rate
    fcns: postcard analysis functions, w/signature:
        fcn(t, path_lengths, leftmost, rightmost)
    """

    tm = clemson_tm(filename, 1)
    tm_len = 0
    for src in sorted(tm):
        for dst in sorted(tm[src]):
            flows = tm[src][dst]
            tm_len += len(flows)

    print "tm # flows: %i" % tm_len
    queries = []
    total = None
    for fcn in query_fcns:
        total, queries_to_add = fcn(t, path_lengths, sw_to_dpid, host_to_ip)
        queries += queries_to_add

    return PostcardAnalysis(t, [tm], [queries], 'shortest-path', FILTERS, total, sw_to_dpid)


def ClemsonNikhilAnalysis():
    """Specific queries for Nikhil's defense."""
    t = ClemsonTopo()
    filename = DEF_TRACE

    host_to_ip = {}
    traffic = 1
    query_fcns = [clemson_queries_nikhil]
    path_lengths = []
    return ClemsonTracePostcardAnalysis(t, host_to_ip, CLEMSON_SW_TO_DPID, path_lengths, traffic, query_fcns, filename)


def ClemsonSmallAnalysis():
    """Clemson topology with traffic from a trace file."""
    t = ClemsonTopo()
    filename = SMALL_TRACE

    host_to_ip = {}
    traffic = 1
    query_fcns = [clemson_queries_small]
    path_lengths = []
    return ClemsonTracePostcardAnalysis(t, host_to_ip, CLEMSON_SW_TO_DPID, path_lengths, traffic, query_fcns, filename)



def ClemsonAnalysis():
    """Clemson topology with traffic from a trace file."""
    t = ClemsonTopo()

    host_to_ip = {}
    traffic = 1
    query_fcns = [clemson_queries]
    path_lengths = []
    return ClemsonTracePostcardAnalysis(t, host_to_ip, CLEMSON_SW_TO_DPID, path_lengths, traffic, query_fcns)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        filename = DEF_TRACE
    elif len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        raise Exception("usage: %s [filename]" % sys.argv[0])
    #parse_clemson_trace(filename, None)
    clemson_tm(filename, 1)

