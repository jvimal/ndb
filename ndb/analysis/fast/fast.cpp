#include <map>
#include <unordered_map>
#include <functional>
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <queue>
using namespace std;
#include <pcap.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>

/* Simple json: just a header file
 * Github: https://github.com/kazuho/picojson
 * Examples: http://mbed.org/users/mimil/code/PicoJSONSample/docs/81c978de0e2b/main_8cpp_source.html
 */
#include "picojson.h"
#define V(x) picojson::value(x)

#include "types.h"
#define USEC_PER_SEC (1e6)

#define TCPHDR_SIZE_NOOPTS 5

const int LOOP_STATS_INTERVAL = 1;
const int FLOW_EXP_SEC = 10;
int SKIP_ETHERNET = 0;
int MAX_PERCENT = 100;
int NR_THREADS = 4;
u64 MAX_PACKETS = ~0;
int OUT_SNAPLEN = 64;
int COUNT_FLOW_KEYS = 0;
int COMPRESSION_LEVEL = 1;
int FILTER_ODD_LENS = 1;

/*
 * Open files completely in main memory without writing to disk.
 * Update: OK I tried open_writemem() and it failed miserably when I
 * needed it the most -- to transparently compress in memory.  So I
 * instead use this flag to open a file in tmpfs which is usually *
 * mounted in memory.  It probably is not as good as mmap()ing a file.
 */
bool MEMORY_OPEN = false;

/*
 * If your avg flow size is small, no point in keeping flow hash table
 * for too long.  If your avg flow size is too large, then the extra
 * cost of storing the first packet of the flow is amortised over the
 * flow duration.  Repeating it a few times over the time of a flow
 * will be a small blip in the grand scheme.  Therefore you're better
 * off clearing the flow hash table every few 100k packets say.
 * Assuming 64B per pcap entry, this is set accordingly.
 */
u64 TABLE_CLEAR_INTERVAL_BYTES = 100000 * 64;
char *JSON_OUTPUT = NULL;
bool WRITE_TO_DISK = false;
bool DUMP_READ_PCAP = false;
bool VERBOSE = false;
bool STRIP_PCAP = false;
bool STRIP_PCAP_TCPOPTS = false;
u64 MIN_PACKET_NUMBER = 0, MAX_PACKET_NUMBER = ~0;
u64 MIN_PACKET_USEC = 0, MAX_PACKET_USEC = ~0;

#define SINGLE_THREADED 1
#define LET(var,x) auto var(x)
#define EACH(it, cont) for (LET(it, (cont).begin()); it != (cont).end(); ++it)
#define REP(i, n) for (int i = 0; i < (n); ++i)

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

void pcap_reader(const u8 *, struct pcap_pkthdr *, const u8 *pkt, Compressor &c);

struct proto_stats proto_count[1 << 16];
struct proto_stats ip_proto_count[1 << 8];

/*
map<u16, struct proto_stats> ip_proto_count;
map<u16, struct proto_stats> proto_count;
*/
map<u16, u32> packet_sizes;
unordered_map<u32, u32> ip_count;

/* The final stats; use in slowpath only. */
typedef picojson::object JSON;
JSON json;

time_t loop_start_time;
time_t loop_finish_time;
u64 pcap_filesize;
u64 pcap_read_bytes;
u64 pcap_bytes_on_wire;
u64 pcap_num_packets;
u64 num_flows;

char *pcap_filename;
FILE *pcap_file_fp;

#include "packet.cpp"
#include "flow.cpp"
#include "thread.cpp"
#include "helper.cpp"
#include "compress.cpp"

void parse_args(int argc, char *argv[]) {
	int i = 0;
	while (i < argc) {
		if (string(argv[i]) == "-p" or string(argv[i]) == "--max-percent") {
			MAX_PERCENT = atoi(argv[i+1]);
			i += 2;
			continue;
		}

		if (string(argv[i]) == "-s" or string(argv[i]) == "--skip-ethernet") {
			SKIP_ETHERNET = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "-t" or string(argv[i]) == "--threads") {
			NR_THREADS = atoi(argv[i+1]);
			i += 2;
			continue;
		}

		if (string(argv[i]) == "-P" or string(argv[i]) == "--packets") {
			MAX_PACKETS = atoi(argv[i+1]);
			i += 2;
			continue;
		}

		if (string(argv[i]) == "-d" or string(argv[i]) == "--dump") {
			WRITE_TO_DISK = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "-D") {
			DUMP_READ_PCAP = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "--verbose") {
			VERBOSE = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "-i" or string(argv[i]) == "--interval-bytes") {
			TABLE_CLEAR_INTERVAL_BYTES = atoll(argv[i+1]);
			i += 2;
			continue;
		}

		if (string(argv[i]) == "-j" or string(argv[i]) == "--json") {
			JSON_OUTPUT = argv[i+1];
			i += 2;
			continue;
		}

		if (string(argv[i]) == "--count-flows") {
			COUNT_FLOW_KEYS = 1;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "--level") {
			COMPRESSION_LEVEL = atoi(argv[i+1]);
			i += 2;
			continue;
		}

		if (string(argv[i]) == "--mem") {
			MEMORY_OPEN = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "--strip") {
			STRIP_PCAP = true;
			i += 1;
			continue;
		}

		if (string(argv[i]) == "--range") {
			MIN_PACKET_NUMBER = atoll(argv[i+1]);
			MAX_PACKET_NUMBER = atoll(argv[i+2]);
			i += 3;
			continue;
		}

		if (string(argv[i]) == "--range-usec") {
			MIN_PACKET_USEC = atoll(argv[i+1]);
			MAX_PACKET_USEC = atoll(argv[i+2]);
			i += 3;
			continue;
		}

		// Remove packets with TCP options, not just the options themselves.
        if (string(argv[i]) == "--strip-tcpopts") {
            STRIP_PCAP_TCPOPTS = true;
            i += 1;
            continue;
        }

        if (string(argv[i]) == "--filter-odd-len") {
            FILTER_ODD_LENS = 1;
            i += 1;
            continue;
        }

        i++;
	}
}

inline void update_stats(Packet &pkt, const struct pcap_pkthdr *hdr) {
	struct proto_stats &stat = proto_count[pkt.eth.proto];
	if (pkt.eth.proto == ETHERTYPE_IP) {
		struct proto_stats &ipstat = ip_proto_count[pkt.ip.proto];
		ipstat.count++;
		ipstat.bytes += hdr->len;
	}

	stat.count++;
	stat.bytes += hdr->len;
	pcap_bytes_on_wire += hdr->len;

	packet_sizes[hdr->len] += 1;
	pcap_num_packets += 1;

	ip_count[pkt.nw_src()]++;
	ip_count[pkt.nw_dst()]++;
}

void loop_start() {
	loop_start_time = time(NULL);
}

float loop_stats(FILE *pcap_file, struct pcap_pkthdr *hdr, Compressor &c) {
	static time_t prev_time = 0;
	static u64 prev_read = 0, prev_bytes_on_wire = 0;
	static u64 prev_bpp_read = 1 << 20; /* 1 MB */
	static bool first = true;
	static struct timeval pcap_begin;
	static struct timeval start, curr;

	u64 pcap_sec;
	time_t now = time(NULL);
	u32 delta = now - prev_time;
	float ret = 0;

	if (prev_bpp_read < pcap_bytes_on_wire) {
		if (prev_bpp_read == (1 << 20)) {
			json["bpp-intervals-pow2"] = V(picojson::array());
			json["gzbpp-intervals-pow2"] = V(picojson::array());
		}

		{
			picojson::array &arr = json["bpp-intervals-pow2"].get<picojson::array>();
			arr.push_back(picojson::value(c.bpp_normal()));
		}

		{
			picojson::array &arr = json["gzbpp-intervals-pow2"].get<picojson::array>();
			arr.push_back(picojson::value(c.bpp_compress()));
		}

		/* Wait a bit more until you record stats */
		prev_bpp_read *= 2;
	}

	if (delta > LOOP_STATS_INTERVAL) {
		if (first) {
			pcap_begin = hdr->ts;
			gettimeofday(&start, NULL);
			first = false;
		}
		JSON jstat;

		gettimeofday(&curr, NULL);
		double dt_usec = (curr.tv_sec - start.tv_sec) * USEC_PER_SEC;
		dt_usec += (curr.tv_usec - start.tv_usec);

		pcap_sec = hdr->ts.tv_sec - pcap_begin.tv_sec;
		pcap_read_bytes = ftell(pcap_file);
		float speed = (pcap_read_bytes - prev_read) * 8.0 / delta / 1e6;
		float wire_speed = (pcap_bytes_on_wire - prev_bytes_on_wire) * 8.0 / delta / 1e6;
		prev_bytes_on_wire = pcap_bytes_on_wire;
		if (speed < 1e-3) speed = 1;

		float percent = pcap_read_bytes * 100.0 / pcap_filesize;
		float est = (pcap_filesize - pcap_read_bytes) * 8.0 / 1e6 / speed;
		u64 mem_kb = memory_usage_kb();

		printf("read %llu/%llu bytes, num-flows %llu, keys %llu "
		       "percent %.2f%%, pcap-speed %.3f Mb/s, est-rem %.1fs, "
		       "wire-speed %.3f Mb/s, mem %.3fGB\n ",
		       pcap_read_bytes, pcap_filesize, num_flows, (u64)threads[0].flows.size(),
		       percent, speed, est,
		       wire_speed, mem_kb/1e6);

		jstat["read"] = V(pcap_read_bytes);
		jstat["total"] = V(pcap_filesize);
		jstat["num_flows"] = V(num_flows);
		jstat["keys"] = V((u64)(threads[0].flows.size()));
		jstat["percent"] = V(percent);
		jstat["pcap-speed"] = V(speed);
		jstat["wire-speed"] = V(wire_speed);
		jstat["memgb"] = V(mem_kb/1e6);

		printf("\tbpp %.3f, gz-bpp %.3f, ", c.bpp_normal(), c.bpp_compress());
		printf("uspp %.3lf, fpp %.3lf, mem-bpp %.3lf ",
		       dt_usec / pcap_num_packets,
		       num_flows * 1.0 / pcap_num_packets,
		       mem_kb * 1e3 / pcap_num_packets);

		jstat["bpp"] = V(c.bpp_normal());
		jstat["gz-bpp"] = V(c.bpp_compress());
		jstat["uspp"] = V(dt_usec / pcap_num_packets);
		jstat["fpp"] = V(num_flows * 1.0 / pcap_num_packets);
		jstat["mem-bpp"] = V(mem_kb * 1e3 / pcap_num_packets);

		picojson::array &arr = json["running-stats"].get<picojson::array>();
		arr.push_back(picojson::value(jstat));

#if not defined(SINGLE_THREADED)
		printf(" qsizes:");
		for (int i = 0; i < NR_THREADS; i++) {
			printf("%llu,", (u64) threads[i].q.size());
		}
#endif

		printf("\n");

		prev_time = now;
		prev_read = pcap_read_bytes;
		ret = percent;
	}

	return ret;
}

void loop_finish() {
	loop_finish_time = time(NULL);
	u32 delta_sec = loop_finish_time - loop_start_time;
	if (delta_sec == 0)
		delta_sec = 1;

	pcap_read_bytes = ftell(pcap_file_fp);
	float speed = pcap_read_bytes * 8.0 / delta_sec / 1e6;
	printf("Parsed %llu bytes in %d seconds, speed %.3f Mb/s\n",
	       pcap_read_bytes, delta_sec, speed);

	json["exec_time_sec"] = picojson::value((u64)delta_sec);
}

void stats_finish(JSON &json) {
	printf("Found %llu flow keys in %llu packets\n", num_flows, pcap_num_packets);
	printf("Unique IPs (source, dest): %llu\n", (u64)ip_count.size());
	printf("    Unique IPs per packet: %.3f\n", ip_count.size() * 1.0 / pcap_num_packets);

	print_proto_stats("Ethernet", proto_count, 1 << 16, ETHERTYPE_TO_STRING, json);
	print_proto_stats("IP", ip_proto_count, 1 << 8, IPPROTO_TO_STRING, json);

	json["pcap_bytes"] = picojson::value(pcap_read_bytes);
	json["pcap_file"] = picojson::value(pcap_filename);
	json["num_flows"] = picojson::value(num_flows);
	json["num_packets"] = picojson::value(pcap_num_packets);
	json["unique_ips"] = picojson::value((u64)ip_count.size());
}

void scan_stats() {
	num_flows = 0;
#if defined SINGLE_THREADED
	num_flows = threads[0].num_flows;
#endif

	REP(i, NR_THREADS) {
		ThreadData *td = &threads[i];
		num_flows += td->num_flows;
	}
}

void dump_json() {
	string filename;

	if (STRIP_PCAP)
		return;

	if (JSON_OUTPUT == NULL) {
		filename = pcap_filename;
		filename += "---stats.json";
	} else {
		filename = JSON_OUTPUT;
	}

	FILE *fp = fopen(filename.c_str(), "w");
	if (!fp) {
		printf("Counldn't open file %s\n", filename.c_str());
		return;
	}

	printf("Dumping stats to %s\n", filename.c_str());
	fprintf(fp, "%s", picojson::value(json).serialize().c_str());
	fclose(fp);
}

static inline u64 ts_to_usec(struct timeval &ts) {
	return ts.tv_sec * USEC_PER_SEC + ts.tv_usec;
}

static inline u64 ts_diff_usec(struct timeval &end, struct timeval &start) {
	u64 us = 0;
	us += (end.tv_sec - start.tv_sec) * USEC_PER_SEC;
	us += end.tv_usec - start.tv_usec;
	return us;
}

int main(int argc, char *argv[]) {
	pcap_t *file, *temp_pcap;
	pcap_dumper_t *temp_dumper;
	FILE *pcap_temp_file;
	string pcap_temp_filename;
	u64 last_pcap_read = 0;
	struct timeval ts_first;

	char errbuf[PCAP_ERRBUF_SIZE];
	if (argc < 2)
		return fprintf(stderr, "Usage (minimal): %s pcap-file\n", argv[0]);

	pcap_filename = argv[1];

#if defined SINGLE_THREADED
	NR_THREADS = 0;
#endif

	pcap_file_fp = fopen(pcap_filename, "r");
	if (pcap_file_fp == NULL) {
		printf("Error in opening %s\n", argv[1]);
		return 1;
	}

	file = pcap_fopen_offline(pcap_file_fp, errbuf);
	if (file == NULL) {
		printf("Error: %s in opening %s\n", errbuf, argv[1]);
		return 1;
	}

	if (pcap_datalink(file) == DLT_RAW) {
		printf("Skipping ethernet headers.\n");
		SKIP_ETHERNET = 1;
	}

	temp_pcap = pcap_open_dead(DLT_EN10MB, OUT_SNAPLEN);
	if (temp_pcap == NULL) {
		printf("Couldn't pcap open for temp output\n");
		return 1;
	}

	pcap_temp_filename = string(pcap_filename) + ".read.pcap";
	temp_dumper = pcap_dump_open(temp_pcap, pcap_temp_filename.c_str());
	if (temp_dumper == NULL) {
		printf("Error: %s in opening %s\n", errbuf, pcap_temp_filename.c_str());
		return 1;
	}

	pcap_filesize = get_file_size(pcap_filename);

	parse_args(argc, argv);
	thread_init();
	packet_init();
	flow_init();
	loop_start();

	json["running-stats"] = picojson::value(picojson::array());
	Compressor c = Compressor(pcap_filename, WRITE_TO_DISK);

	u64 index = 0;
	bool first = true;
	u64 usec = 0;

	while (1) {
	    index++;
	    float percent;
		struct pcap_pkthdr hdr;
		const u8 *pkt = pcap_next(file, &hdr);
		if (pkt == NULL)
			break;
		assert(hdr.caplen <= PACKET_BUFF_SIZE);

		if (first) {
			ts_first = hdr.ts;
			first = false;
		}

		/* Only parse partial trace */
		if (MAX_PACKET_NUMBER != ~0) {
			if (index < MIN_PACKET_NUMBER)
				continue;

			if (index > MAX_PACKET_NUMBER)
				break;
		}

		/* Partial trace expressed in time quanta */
		if (MAX_PACKET_USEC != ~0) {
			usec = ts_diff_usec(hdr.ts, ts_first);
			printf("%llu\n", usec);
			if (usec < MIN_PACKET_USEC)
				continue;
			if (usec > MAX_PACKET_USEC)
				break;
		}

		if (likely(!STRIP_PCAP)) {
			pcap_reader(pkt, &hdr, pkt, c);

	        if (DUMP_READ_PCAP) {
	            pcap_dump((u_char *)temp_dumper, &hdr, pkt);
	        }
		} else {
			Packet packet(pkt, hdr.len, SKIP_ETHERNET, 0, hdr.caplen);
			int offset = 0;

			if (packet.eth.proto != ETHERTYPE_IP)
				continue;

			offset = packet.ip.hl * 4;
			if (!SKIP_ETHERNET)
				offset += 14;

			if (packet.ip.proto == IPPROTO_TCP) {
                offset += packet.tcp.off * 4;
			    if (STRIP_PCAP_TCPOPTS && (packet.tcp.off > TCPHDR_SIZE_NOOPTS)) {
			        continue;
			    }
			} else if (packet.ip.proto == IPPROTO_UDP) {
				offset += sizeof(struct udphdr);
			} else {
				continue;
			}

			// If any packet has an odd length, that is, one where the IP-
			// computed length differs from the report length from the pcap
			// entry, ditch the packet.
			// For more examples of the wild and wonderful packets that
			// trigger this condition, see:
			// ndb/ndb/analysis/packetlength.py:packet_len()
			int inferred_len = packet.infer_len();
			if (FILTER_ODD_LENS and (inferred_len != hdr.len)) {
			    if (VERBOSE)
			        printf("WARN: WS pkt num %u: %i != inferred len %i\n",
			               (uint) index, hdr.len, inferred_len);
			    continue;
			}

			// Uncomment below to zero out everything past the header.
			// Just setting hdr.caplen is sufficient to truncate the PCAP
			// entry written to disk.
			//bzero(packet.buff + offset, (PACKET_BUFF_SIZE - offset));
			hdr.caplen = offset;
			pcap_dump((u_char *)temp_dumper, &hdr, packet.buff);
			pcap_num_packets++;
		}

		percent = loop_stats(pcap_file_fp, &hdr, c);
		if (pcap_read_bytes - last_pcap_read >= TABLE_CLEAR_INTERVAL_BYTES) {
			threads[0].flows.clear();
			last_pcap_read = pcap_read_bytes;
		}

		if (percent > MAX_PERCENT || pcap_num_packets == MAX_PACKETS)
			break;
	}

	pcap_dump_close(temp_dumper);
	scan_stats();
	loop_finish();
	stats_finish(json);
	thread_finish(json);
	c.stats(json);
	c.close();

	dump_json();
	return 0;
}

void pcap_reader(const u8 *data, struct pcap_pkthdr *hdr, const u8 *_pkt, Compressor &c) {
	static u32 packet_number = 0;
	Packet pkt(_pkt, hdr->len, SKIP_ETHERNET, packet_number++, hdr->caplen);
	FlowKey key(pkt);
	pkt.ts = hdr->ts;
	update_stats(pkt, hdr);

#if not defined(SINGLE_THREADED)
	ThreadData *td = &threads[key.hsh % NR_THREADS];
	volatile u32 *qsize = &td->qsize;
	while (*qsize > 1000000) {
		scan_stats();
	}

	pthread_spin_lock(&td->qmutex);
	td->q.push(make_pair(key, pkt));
	td->qsize++;
	pthread_spin_unlock(&td->qmutex);
#endif

#if defined (SINGLE_THREADED)
	/* Single threaded version */
	Flow &flow = threads[0].flows[key];
	int first = flow.add_packet(pkt, global_flow_stats[0]);
	int first_packet_id = -1;

	if (first) {
		if (likely(COUNT_FLOW_KEYS == 0))
			first_packet_id = c.write_first_header(pkt);
		threads[0].num_flows++;
		scan_stats();
	}

	if (likely(COUNT_FLOW_KEYS == 0))
		c.write(flow, pkt, first_packet_id);
#endif
}
