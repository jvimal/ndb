
template<class T>
void print_table(map<T, u64> &dict, map<T, string> &names, string msg);
template<class T>
void print_table_int(map<T, u64> &dict, const char *msg, double, JSON&);

Header diff_headers[] = {
	IP_TOS_F,
	IP_LEN,
	IP_ID,
	IP_OFF,
	IP_TTL_F,
	IP_PROTO,
	TCP_OFF,
	TCP_FLAGS,
	TCP_WIN,
	TCP_URP,
	UDP_LEN,
};

Header delta_headers[] = {
	TS_SEC,
	TCP_SEQ,
	TCP_ACK,
};

template <class T>
void add_maps(map<T, u64> &a, map<T, u64> &b) {
	EACH(it, b) {
		a[it->first] += it->second;
	}
}

/* These std::maps might be costly to update per-packet */
struct FlowStats {
	map<Header, u64> FieldsChanged;
	map<u16, u64> NumCompressedFields;
	map<u32, u64> TCPSeqDeltas;
	map<u32, u64> TCPAckDeltas;
	map<u32, u64> PacketsPerFlow;

	u64 total_compressed_bits;
	u64 total_compressed_bytes;
	u64 total_bytes;
	u64 num_packets;
	u64 max_duration_sec;

	FlowStats() {
		total_compressed_bits = 0;
		total_compressed_bytes = 0;
		total_bytes = 0;
		num_packets = 0;
		max_duration_sec = 0;
	}

	void update(FlowStats *other) {
		total_compressed_bits += other->total_compressed_bits;
		total_compressed_bytes += other->total_compressed_bytes;
		total_bytes += other->total_bytes;
		num_packets += other->num_packets;
		max_duration_sec = max(max_duration_sec, other->max_duration_sec);

		add_maps(FieldsChanged, other->FieldsChanged);
		add_maps(NumCompressedFields, other->NumCompressedFields);
		add_maps(TCPSeqDeltas, other->TCPSeqDeltas);
		add_maps(TCPAckDeltas, other->TCPAckDeltas);
		add_maps(PacketsPerFlow, other->PacketsPerFlow);
	}
};

#define MAX_STATS 8
FlowStats *global_flow_stats[MAX_STATS];

struct Flow {
	u32 first_packet_size;
	u32 packets;
	u64 bytes;
	u64 other_packets_size;
	u64 compressed_size_bits;

	Packet first;
	Packet prev, curr;
	HeaderValues hprev, hcurr;
	u32 haprev[NUM_FIELDS], hacurr[NUM_FIELDS];

	FlowStats *stats;

	Flow() {
		first_packet_size = 0;
		compressed_size_bits = 0;
		other_packets_size = 0;
		packets = 0;
		bytes = 0;
		stats = NULL;
		memset(haprev, -1, sizeof(haprev));
		memset(hacurr, -1, sizeof(hacurr));
	}

	int add_packet(Packet &pkt, FlowStats *s = NULL);
	bool expired() {
		return (u64)(prev.ts.tv_sec - first.ts.tv_sec) > FLOW_EXP_SEC;
	}

	u32 *get_prev_harray() {
		return haprev;
	}
};

void flow_init() {
	for (int i = 0; i < MAX_STATS; i++)
		global_flow_stats[i] = new FlowStats;
}

int Flow::add_packet(Packet &pkt, FlowStats *s) {
	int ret = 0;
	packets += 1;
	bytes += pkt.size;

	if (packets == 1) {
		stats = s;
		first_packet_size = pkt.size;
		first = pkt;
		prev = pkt;
		curr = pkt;
		pkt.get_headers_opt(haprev);
		ret = 1;
	} else {
		other_packets_size += pkt.size;
		prev = curr;
		memcpy(haprev, hacurr, sizeof(haprev));
		curr = pkt;
	}

	pkt.get_headers_opt(hacurr);
	return ret;
}
