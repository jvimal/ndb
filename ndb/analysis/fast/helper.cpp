

u64 get_file_size(const char *filename) {
	FILE *fp = fopen(filename, "r");
	u64 size = 0;

	if (fp == NULL)
		return -1;

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fclose(fp);
	return size;
}

string ntos(u64 num) {
	char buff[32];
	sprintf(buff, "%llu", num);
	return string(buff);
}

template<class T>
float entropy(map<T, u64> &dict) {
	vector< pair<u64, T> > items;
	u64 total = 0;
	double entropy = 0.0;

	EACH(it, dict) {
		LET(key, it->first);
		LET(value, it->second);
		items.push_back(make_pair(value, key));
		total += it->second;
	}

	sort(items.begin(), items.end());
	EACH(it, items) {
		LET(key, it->second);
		LET(value, it->first);
		double pi = value * 1.0 / total;
		if (pi > 0)
			entropy += pi * log2(1.0 / pi);
		// printf("%d: %d\n", key, value);
	}

	return entropy;
}

template<class T>
void print_table(map<T, u64> &dict, map<T, string> &names, string msg) {
	vector< pair<u64, T> > items;
	u64 total = 0;

	printf("%s\n", msg.c_str());

	EACH(it, dict) {
		LET(key, it->first);
		LET(value, it->second);
		items.push_back(make_pair(value, key));
		total += it->second;
	}

	sort(items.begin(), items.end());
	EACH(it, items) {
		LET(key, it->second);
		LET(value, it->first);
		float fraction = value * 1.0 / total;
		printf("%s: %llu, fraction: %.3f\n", names[key].c_str(), value, fraction);
	}

	puts("");
}

void print_proto_stats(string parent,
		       struct proto_stats *dict,
		       int num_elem,
		       map<u16, string> &id_to_name,
		       JSON &json)
{
	u32 num_found = 0;
	REP(i, num_elem) {
		if (dict[i].count)
			num_found++;
	}

	JSON jstat;

	printf("%s protocol statistics %d found:\n", parent.c_str(), num_found);
	REP(i, num_elem) {
		if (!dict[i].count)
			continue;
		u16 proto = i;
		struct proto_stats stat = dict[i];
		float fraction = stat.bytes * 1.0 / pcap_bytes_on_wire;
		printf("proto: %d, name: %s, count: %u, bytes: %llu, fraction: %.3f\n",
		       proto,
		       id_to_name[proto].c_str(),
		       stat.count,
		       stat.bytes,
		       fraction);


		JSON protostat;
		string protoname = id_to_name[proto];
		if (protoname == "")
			protoname = ntos(proto);

		protostat["count"] = picojson::value((u64)stat.count);
		protostat["bytes"] = picojson::value((u64)stat.bytes);
		jstat[protoname] = picojson::value(protostat);
	}

	json[parent.c_str()] = picojson::value(jstat);
	puts("");
}

template<class T>
void print_table_int(map<T, u64> &dict, const char *msg, double fr_greater, JSON &json) {
	vector< pair<u64, T> > items;
	u64 total = 0;
	u64 kmax = 0;
	u64 kmin = ~0;
	double kavg = 0;
	JSON ele;

	printf("%s\n", msg);

	EACH(it, dict) {
		LET(key, it->first);
		LET(value, it->second);
		kmax = max(kmax, (u64)key);
		kmin = min(kmin, (u64)key);

		items.push_back(make_pair(value, key));
		total += it->second;
	}

	sort(items.begin(), items.end());

	EACH(it, items) {
		LET(key, it->second);
		LET(value, it->first);
		float fraction = value * 1.0 / total;
		kavg += fraction * key;

		if (fraction > fr_greater)
			printf("%llu: %llu, frac: %.3f\n", (u64)key, value, fraction);

		ele[ntos(key)] = picojson::value(value);
	}

	printf("MaxKey: %llu, MinKey: %llu, AvgKey: %.3lf\n",
	       kmax, kmin, kavg);

	json[msg] = picojson::value(ele);

	puts("");
}

u64 memory_usage_kb() {
	FILE* file = fopen("/proc/self/status", "r");
	u64 mem = 0;
	char line[128];

	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "VmRSS:", 6) == 0) {
			sscanf(line, "VmRSS: %llu kB", &mem);
			break;
		}
	}

	return mem;
}
