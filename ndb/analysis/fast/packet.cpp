#include <string>
using namespace std;
#include <string.h>
#include "types.h"
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#define __FAVOR_BSD 1
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <net/if_arp.h>
#include <netinet/ip_icmp.h>

// When printing a packet, check this to not print an overly large one.
#define MAX_PKT_SIZE (1514)

//#define USE_PACKET_COPY_CONSTRUCTOR

struct arp_eth_header {
	u16      ar_hrd;     /* format of hardware address   */
	u16      ar_pro;     /* format of protocol address   */
	unsigned char   ar_hln; /* length of hardware address   */
	unsigned char   ar_pln; /* length of protocol address   */
	u16      ar_op;      /* ARP opcode (command)     */

	/* Ethernet+IPv4 specific members. */
	unsigned char       ar_sha[ETHER_ADDR_LEN];   /* sender hardware address  */
	u32 sip;
	unsigned char       ar_tha[ETHER_ADDR_LEN];   /* target hardware address  */
	u32 tip;
} __attribute__((packed));

enum Header {
	TS_SEC,
	PCAP_SEQ,
	IP_PROTO,
	IP_SRC,
	IP_DST,

	TCP_SRC,
	TCP_DST,
	UDP_SRC,
	UDP_DST,

	IP_HL,

	IP_TOS_F,
	IP_LEN,
	IP_ID,
	IP_OFF,
	IP_TTL_F,
	IP_CSUM,
	TCP_SEQ,
	TCP_ACK,
	TCP_OFF,
	TCP_FLAGS,
	TCP_WIN,
	TCP_CSUM,
	TCP_URP,

	UDP_CSUM,
	UDP_LEN,

/* This should always be at the end */
	NUM_FIELDS,
};

map<u16, string> ETHERTYPE_TO_STRING;
map<u16, string> IPPROTO_TO_STRING;
map<Header, u16> HEADER_SIZE_BITS;
map<Header, string> HEADER_NAMES;
u8 HEADER_WRITE_BITS[NUM_FIELDS];

typedef map<Header, u64> HeaderValues;

void packet_init() {
#define a ETHERTYPE_TO_STRING
	a[ETHERTYPE_IP] = "IP";
	a[ETHERTYPE_ARP] = "ARP";
	a[ETHERTYPE_VLAN] = "VLAN";
	a[ETHERTYPE_IPV6] = "IPv6";
#undef a

#define a IPPROTO_TO_STRING
	a[IPPROTO_TCP] = "TCP";
	a[IPPROTO_UDP] = "UDP";
	a[IPPROTO_ICMP] = "ICMP";
#undef a

#define a HEADER_SIZE_BITS
	a[TS_SEC] = 64;
	a[PCAP_SEQ] = 64;
	a[IP_HL] = 8; // 4??
	a[IP_TOS_F] = 8;
	a[IP_LEN] = 16;
	a[IP_ID] = 16;
	a[IP_OFF] = 16;
	a[IP_TTL_F] = 8;
	a[IP_PROTO] = 8;
	a[IP_CSUM] = 16;
	a[IP_SRC] = 32;
	a[IP_DST] = 32;

	a[TCP_SRC] = 16;
	a[TCP_DST] = 16;
	a[TCP_SEQ] = 32;
	a[TCP_ACK] = 32;
	a[TCP_OFF] = 8; // 4??
	a[TCP_FLAGS] = 8; // 6??
	a[TCP_WIN] = 16;
	a[TCP_CSUM] = 16;
	a[TCP_URP] = 16;

	a[UDP_SRC] = 16;
	a[UDP_DST] = 16;
	a[UDP_CSUM] = 16;
	a[UDP_LEN] = 16;
#undef a

#define a HEADER_WRITE_BITS
	//a[TS_SEC] = 64;
	//a[PCAP_SEQ] = 64;
	a[IP_HL] = 8; // 4??
	a[IP_TOS_F] = 8;
	a[IP_LEN] = 16;
	a[IP_ID] = 16;
	a[IP_OFF] = 16;
	a[IP_TTL_F] = 8;
	a[IP_PROTO] = 8;
	a[IP_CSUM] = 16;
	a[IP_SRC] = 32;
	a[IP_DST] = 32;

	a[TCP_SRC] = 16;
	a[TCP_DST] = 16;
	a[TCP_SEQ] = 32;
	a[TCP_ACK] = 32;
	a[TCP_OFF] = 8; // 4??
	a[TCP_FLAGS] = 8; // 6??
	a[TCP_WIN] = 16;
	a[TCP_CSUM] = 16;
	a[TCP_URP] = 16;

	a[UDP_SRC] = 16;
	a[UDP_DST] = 16;
	a[UDP_CSUM] = 16;
	a[UDP_LEN] = 16;
#undef a

#define a HEADER_NAMES
	a[TS_SEC] = "TS_SEC";
	a[PCAP_SEQ] = "PCAP_SEQ";
	a[IP_HL] = "IP_HL";
	a[IP_TOS_F] = "IP_TOS";
	a[IP_LEN] = "IP_LEN";
	a[IP_ID] = "IP_ID";
	a[IP_OFF] = "IP_OFF";
	a[IP_TTL_F] = "IP_TTL";
	a[IP_PROTO] = "IP_PROTO";
	a[IP_CSUM] = "IP_CSUM";
	a[IP_SRC] = "IP_SRC";
	a[IP_DST] = "IP_DST";

	a[TCP_SRC] = "TCP_SRC";
	a[TCP_DST] = "TCP_DST";
	a[TCP_SEQ] = "TCP_SEQ";
	a[TCP_ACK] = "TCP_ACK";
	a[TCP_OFF] = "TCP_OFF";
	a[TCP_FLAGS] = "TCP_FLAGS";
	a[TCP_WIN] = "TCP_WIN";
	a[TCP_CSUM] = "TCP_CSUM";
	a[TCP_URP] = "TCP_URP";

	a[UDP_SRC] = "UDP_SRC";
	a[UDP_DST] = "UDP_DST";
	a[UDP_CSUM] = "UDP_CSUM";
	a[UDP_LEN] = "UDP_LEN";
#undef a
}

void update_headers(HeaderValues &a, HeaderValues &b) {
	EACH(it, b) {
		a[it->first] = it->second;
	}
}

void print_headers(HeaderValues &a) {
	EACH(it, a) {
		printf("%s: %llu (0x%llx)\n",
		       HEADER_NAMES[it->first].c_str(),
		       it->second, it->second);
	}
}

// Print as colon-hex string
void print_eth(u8* e) {
    int i;
    for (i = 0; i < 6; i++) {
        if (e[i] < 16)
            printf("0%0x:", (u8) e[i]);
        else
            printf("%0x:", (u8) e[i]);
    }
}

struct Ethernet {
	u8 *dst;
	u8 *src;
	u16 vlan;
	u8 pcp;
	u16 tpid;

	u16 proto;
	const u8 *payload;

	Ethernet() {
		dst = (u8 *)"\x00\x00\x00\x00\x00\x00";
		src = dst;
		vlan = 0;
		pcp = 0;
		tpid = 0;
	}

	Ethernet(const u8 *pkt) {
		update(pkt);
		vlan = pcp = 0;
		u32 offset = sizeof(ether_header);

		if (proto == ETHERTYPE_VLAN) {
			tpid = proto;
			vlan = ntohs(*(const u16 *) (pkt + offset));
			pcp = (vlan & 0xf000) >> 12;
			vlan = (vlan & 0x0fff);
			offset += 2;
			proto = ntohs(*(const u16 *) (pkt + offset));
			offset += 2;
		}

		payload = pkt + offset;
		//print();
	}

	void update(const u8 *pkt) {
	    struct ether_header *eth = (struct ether_header *)pkt;
        dst = eth->ether_dhost;
        src = eth->ether_shost;
        proto = ntohs(eth->ether_type);
	}

	void print() {
        printf("Eth: ");
        print_eth(dst);
        printf(" ");
        print_eth(src);
        printf(" ");
        printf("0x%x\n", proto);
	}

	std::vector<u8> pack() {
        ether_header eh;
	    memcpy(&eh.ether_dhost, dst, sizeof eh.ether_dhost);
        memcpy(&eh.ether_shost, src, sizeof eh.ether_shost);
	    eh.ether_type = htons(proto);

	    std::vector<u8> packed;
	    for (int i = 0; i < sizeof(ether_header); i++) {
	        packed.push_back(*((const u8*) &eh + i));
	    }

	    assert(packed.size() == sizeof(ether_header));
	    return packed;
	}

	u8 pack_buf(u8* buf) {
        ether_header eh;
	    memcpy(&eh.ether_dhost, dst, sizeof eh.ether_dhost);
        memcpy(&eh.ether_shost, src, sizeof eh.ether_shost);
        eh.ether_type = htons(proto);
        memcpy(buf, &eh, sizeof(ether_header));
        return sizeof(eh);
	}
};

struct ARP {
	u16 op;
	u32 nw_src, nw_dst;

	ARP() {}
	ARP(const u8 *pkt) {
		struct arp_eth_header *arp = (struct arp_eth_header *)pkt;
		op = ntohs(arp->ar_op);

		if (ntohs(arp->ar_pro) == ETHERTYPE_IP
		    && arp->ar_pln == 4) /* ipv4 */
		{
			nw_src = ntohl(arp->sip);
			nw_dst = ntohl(arp->tip);
		}
	}
};

void bytes_to_vector(std::vector<u8>* packed, u8* header, uint size) {
    for (int i = 0; i < size; i++) {
        packed->push_back(*((const u8*) header + i));
    }
    assert(packed->size() == size);
}


struct IP {
	u8 v, hl, tos;
	u16 len, id, off;
	u8 ttl, proto;
	u16 csum;
	u32 src, dst;
	const u8 *payload;

	IP() {}
	IP(const u8 *pkt) {
		const struct ip *ip = (const struct ip *)pkt;
		v = ip->ip_v;
		hl = ip->ip_hl;
		tos = ip->ip_tos;
		len = ntohs(ip->ip_len);
		id = ntohs(ip->ip_id);
		off = ntohs(ip->ip_off);
		ttl = ip->ip_ttl;
		proto = ip->ip_p;
		csum = ntohs(ip->ip_sum);
		src = ntohl(ip->ip_src.s_addr);
		dst = ntohl(ip->ip_dst.s_addr);
		payload = pkt + (ip->ip_hl * 4);
	}

    std::vector<u8> pack() {
        struct ip ih;
        ih.ip_v = v;
        ih.ip_hl = hl;
        ih.ip_tos = tos;
        ih.ip_len = htons(len);
        ih.ip_id = htons(id);
        ih.ip_off = htons(off);
        ih.ip_ttl = ttl;
        ih.ip_p = proto;
        ih.ip_sum = htons(csum);
        ih.ip_src.s_addr = htonl(src);
        ih.ip_dst.s_addr = htonl(dst);

        std::vector<u8> packed;
        bytes_to_vector(&packed, (u8*) &ih, sizeof(ih));
        return packed;
    }

    u8 pack_buf(u8* buf) {
        struct ip ih;
        ih.ip_v = v;
        ih.ip_hl = hl;
        ih.ip_tos = tos;
        ih.ip_len = htons(len);
        ih.ip_id = htons(id);
        ih.ip_off = htons(off);
        ih.ip_ttl = ttl;
        ih.ip_p = proto;
        ih.ip_sum = htons(csum);
        ih.ip_src.s_addr = htonl(src);
        ih.ip_dst.s_addr = htonl(dst);

        memcpy(buf, &ih, sizeof(ip));
        return sizeof(ip);
    }

	bool is_fragment() {
#define MORE_FRAGMENTS 0x2000
#define FRAG_OFF_MASK 0x1fff
		return off & htons(MORE_FRAGMENTS | FRAG_OFF_MASK);
	}

	HeaderValues get_headers() {
		HeaderValues ret;
                ret[IP_HL] = hl;
                ret[IP_TOS_F] = tos;
                ret[IP_LEN] = len;
                ret[IP_ID] = id;
                ret[IP_OFF] = off;
                ret[IP_TTL_F] = ttl;
                ret[IP_PROTO] = proto;
                ret[IP_CSUM] = csum;
                ret[IP_SRC] = src;
                ret[IP_DST] = dst;
		return ret;
	}

	void get_headers_opt(HVArray ret) {
                ret[IP_HL] = hl;
                ret[IP_TOS_F] = tos;
                ret[IP_LEN] = len;
                ret[IP_ID] = id;
                ret[IP_OFF] = off;
                ret[IP_TTL_F] = ttl;
                ret[IP_PROTO] = proto;
                ret[IP_CSUM] = csum;
                ret[IP_SRC] = src;
                ret[IP_DST] = dst;
	}
};

struct ICMP {
	u8 type, code;
	ICMP(){}
	ICMP(const u8* pkt) {
		struct icmphdr *icmp = (struct icmphdr *)pkt;
		type = icmp->type;
		code = icmp->code;
	}
};

struct TCP {
	u16 src, dst;
	u32 seq, ack;
	u8 off;
	u8 x2; // 4 extra reserved bits next to the offset
	u8 flags;
	u16 win;
	u16 csum;
	u16 urp;

	TCP(){}
	TCP(const u8 *pkt) {
		const struct tcphdr *tcp = (const struct tcphdr *)pkt;
		src = ntohs(tcp->th_sport);
		dst = ntohs(tcp->th_dport);
		seq = ntohl(tcp->th_seq);
		ack = ntohl(tcp->th_ack);
		off = tcp->th_off;
		x2 = tcp->th_x2;
		flags = tcp->th_flags;
		win = ntohs(tcp->th_win);
		csum = ntohs(tcp->th_sum);
		urp = ntohs(tcp->th_urp);
	}

    std::vector<u8> pack() {
        struct tcphdr th;
        th.th_sport = htons(src);
        th.th_dport = htons(dst);
        th.th_seq = htonl(seq);
        th.th_ack = htonl(ack);
        th.th_off = off;
        th.th_x2 = x2;
        th.th_flags = flags;
        th.th_win = htons(win);
        th.th_sum = htons(csum);
        th.th_urp = htons(urp);

        std::vector<u8> packed;
        bytes_to_vector(&packed, (u8*) &th, sizeof(th));
        return packed;
    }

    u8 pack_buf(u8* buf) {
        struct tcphdr th;
        th.th_sport = htons(src);
        th.th_dport = htons(dst);
        th.th_seq = htonl(seq);
        th.th_ack = htonl(ack);
        th.th_off = off;
        th.th_x2 = x2;
        th.th_flags = flags;
        th.th_win = htons(win);
        th.th_sum = htons(csum);
        th.th_urp = htons(urp);

        memcpy(buf, &th, sizeof(tcphdr));
        return sizeof(tcphdr);
    }

	HeaderValues get_headers() {
		HeaderValues ret;
		ret[TCP_SRC] = src;
		ret[TCP_DST] = dst;
		ret[TCP_SEQ] = seq;
		ret[TCP_ACK] = ack;
		ret[TCP_OFF] = off;
		ret[TCP_FLAGS] = flags;
		ret[TCP_WIN] = win;
		ret[TCP_CSUM] = csum;
		ret[TCP_URP] = urp;
		return ret;
	}

	void get_headers_opt(HVArray ret) {
		ret[TCP_SRC] = src;
		ret[TCP_DST] = dst;
		ret[TCP_SEQ] = seq;
		ret[TCP_ACK] = ack;
		ret[TCP_OFF] = off;
		ret[TCP_FLAGS] = flags;
		ret[TCP_WIN] = win;
		ret[TCP_CSUM] = csum;
		ret[TCP_URP] = urp;
	}
};

struct UDP {
	u16 src, dst;
	u16 len, csum;

	UDP(){}
	UDP(const u8 *pkt) {
		const struct udphdr *udp = (const struct udphdr *)pkt;
		src = ntohs(udp->uh_sport);
		dst = ntohs(udp->uh_dport);
		len = ntohs(udp->uh_ulen);
		csum = ntohs(udp->uh_sum);
	}

    std::vector<u8> pack() {
        struct udphdr uh;
        uh.uh_sport = htons(src);
        uh.uh_dport = htons(dst);
        uh.uh_ulen = htons(len);
        uh.uh_sum = htons(csum);

        std::vector<u8> packed;
        bytes_to_vector(&packed, (u8*) &uh, sizeof(uh));
        return packed;
    }

    u8 pack_buf(u8* buf) {
        struct udphdr uh;
        uh.uh_sport = htons(src);
        uh.uh_dport = htons(dst);
        uh.uh_ulen = htons(len);
        uh.uh_sum = htons(csum);
        memcpy(buf, &uh, sizeof(uh));
        return sizeof(udphdr);
    }

	HeaderValues get_headers() {
		HeaderValues ret;
		ret[UDP_SRC] = src;
		ret[UDP_DST] = dst;
		ret[UDP_CSUM] = csum;
		ret[UDP_LEN] = len;
		return ret;
	}

	void get_headers_opt(HVArray ret) {
		ret[UDP_SRC] = src;
		ret[UDP_DST] = dst;
		ret[UDP_CSUM] = csum;
		ret[UDP_LEN] = len;
	}
};

#define PACKET_BUFF_SIZE 128

struct FastPacket {
    u8 buff[PACKET_BUFF_SIZE];
    FastPacket() {
    }
    FastPacket(const u8 *pkt, u32 len) {
        memcpy(buff, pkt, len);
    }
    u16 infer_len() {
        int inferred_len = 0;
        uint skip_ethernet = 0;
        if (!skip_ethernet) {
            inferred_len += sizeof(ether_header);
            const ether_header* eh = (const ether_header*) buff;
            u16 proto = ntohs(eh->ether_type);
            assert (proto == ETHERTYPE_IP);
        }
        ip* ip_hdr = (ip*)(buff + sizeof(ether_header));
        u16 ip_len = ntohs(ip_hdr->ip_len);
        inferred_len += ip_len;
        return inferred_len;
    }
    u16 hdr_size() {
        int size = 0;
        uint skip_ethernet = 0;
        if (!skip_ethernet) {
            size += sizeof(ether_header);
            const ether_header* eh = (const ether_header*) buff;
            u16 proto = ntohs(eh->ether_type);
            assert (proto == ETHERTYPE_IP);
            size += sizeof(iphdr);
            iphdr* ip_hdr = (iphdr*)(buff + sizeof(ether_header));
            switch (ip_hdr->protocol) {
            case IPPROTO_TCP:
                size += sizeof(struct tcphdr);
                break;

            case IPPROTO_UDP:
                size += sizeof(struct udphdr);
                break;
            }
        }
        return size;
    }
    /* NOTE:
     * Most diffs are full replacement, but a few actually represent
     * deltas: [TCP_SEQ, TCP_ACK, IP_ID] */
    void apply_diff(Header h, u64 v) {
        const ether_header* eh = (const ether_header*) buff;
        iphdr* ip_hdr = (iphdr*)(buff + sizeof(ether_header));
        tcphdr* tcp_hdr = (tcphdr*)(buff + sizeof(ether_header) + sizeof(struct iphdr));
        udphdr* udp_hdr = (udphdr*)(buff + sizeof(ether_header) + sizeof(struct iphdr));
        switch (h) {
        //TS_SEC
        //PCAP_SEQ
        case IP_HL:
            ip_hdr->ihl = v;
            break;
        case IP_TOS_F:
            ip_hdr->tos = v;
            break;
        case IP_LEN:
            ip_hdr->tot_len = htons(v);
            break;
        case IP_ID: // DELTA
            //printf("before: 0x%x\n", ntohs(ip_hdr->id) + v);
            ip_hdr->id = htons(ntohs(ip_hdr->id) + v);
            //printf("after: 0x%x\n", ntohs(ip_hdr->id) + v);
            break;
        case IP_OFF:
            ip_hdr->frag_off = htons(v);
            break;
        case IP_TTL_F:
            ip_hdr->ttl = v;
            break;
        case IP_PROTO:
            ip_hdr->protocol = v;
            break;
        case IP_CSUM:
            ip_hdr->check = htons(v);
            break;
        case IP_SRC:
            ip_hdr->saddr = htonl(v);
            break;
        case IP_DST:
            ip_hdr->daddr = htonl(v);
            break;

        case TCP_SRC:
            tcp_hdr->th_sport = htons(v);
            break;
        case TCP_DST:
            tcp_hdr->th_dport = htons(v);
            break;
        case TCP_SEQ: // DELTA
            tcp_hdr->th_seq = htonl(ntohl(tcp_hdr->th_seq) + v);
            break;
        case TCP_ACK: // DELTA
            tcp_hdr->th_ack = htonl(ntohl(tcp_hdr->th_ack) + v);
            break;
        case TCP_OFF:
            tcp_hdr->th_off = v;
            break;
        case TCP_FLAGS:
            tcp_hdr->th_flags = v;
            break;
        case TCP_WIN:
            tcp_hdr->th_win = htons(v);
            break;
        case TCP_CSUM:
            tcp_hdr->th_sum = htons(v);
            break;
        case TCP_URP:
            tcp_hdr->th_urp = htons(v);
            break;

        case UDP_SRC:
            udp_hdr->uh_sport = htons(v);
            break;
        case UDP_DST:
            udp_hdr->uh_dport = htons(v);
            break;
        case UDP_CSUM:
            udp_hdr->uh_sum = htons(v);
            break;
        case UDP_LEN:
            udp_hdr->uh_ulen = htons(v);
            break;
        }

    }
    void inc_ip_id() {
        iphdr* ip_hdr = (iphdr*)(buff + sizeof(ether_header));
        ip_hdr->id = htons(ntohs(ip_hdr->id) + 1);
    }
};

struct Packet {
	const u8 *payload;
	u8 buff[PACKET_BUFF_SIZE];
	struct timeval ts;
	Ethernet eth;
	ARP arp;
	IP ip;
	TCP tcp;
	UDP udp;
	ICMP icmp;
	u32 size;
	int seq;
	int caplen;
	int skip_ethernet;

	Packet() {
	    payload = buff;
	}
    Packet(const u8 *pkt, u32 sz, int skip_ethernet = 0, u32 packet_number = 0, int caplen = 0) {
        size = sz;
        this->caplen = caplen;
        payload = buff;
        memcpy(buff, pkt, caplen);
        seq = packet_number;
        this->skip_ethernet = skip_ethernet;

        if (likely(!skip_ethernet)) {
            eth = Ethernet(buff); // now (buff) was: pkt
        } else {
            eth.proto = ETHERTYPE_IP;
            eth.payload = payload;
        }

        switch (eth.proto) {
        case ETHERTYPE_IP:
            parse_ip(eth.payload);
            break;

        case ETHERTYPE_ARP:
            parse_arp(eth.payload);
            break;
        }
        //print();
    }

//    Packet& operator= (const Packet &p) {
//        // do the copy
//        payload = buff;
//        return *this;
//    }

    // _something_ is necessary to work around the issue where the src and dst
    // pointers in the Ethernet object, which point to inside a packet, do
    // not get updated from a basic default shallow-copy copy constructor.
    // For whatever reasons, the copy constructor below fails.  Hence, you
    // MUST call this immediately following any operation that duplicates
    // a Packet objects, such as an STL map or vector insertion, on the ref
    // to the newly created object in the container.
    void update() {
        eth.update(buff);
    }

#if defined(USE_PACKET_COPY_CONSTRUCTOR)
    Packet(const Packet &p) {
        //memcpy(buff, p.buff, PACKET_BUFF_SIZE);
        /* Since fields like eth src and dst are pointers to inside the buff,
           simplying copying them would have them point to stack memory,
           not heap.  Hence, we build the packet again.*/
        // Ideally this would work:
        //Packet(p.buff, p.size, p.skip_ethernet, p.seq, p.caplen);
        // but it doesn't so this code is duplicated for now
        // TODO: remove constructor code duplication
        size = p.size;
        caplen = p.caplen;
        payload = buff;
        //printf("p.buff: 0x%x [0]: 0x%x [1]: 0x%x\n", (u64) p.buff);
        //printf("buff: 0x%x\n", (u64) buff);
        // Why does this segfault in when an STL container duplicates a Packet
        // object?
        memcpy(buff, p.buff, p.caplen);
        seq = p.seq;
        skip_ethernet = p.skip_ethernet;

        if (likely(!skip_ethernet)) {
            eth = Ethernet(buff);
        } else {
            eth.proto = ETHERTYPE_IP;
            eth.payload = payload;
        }

        switch (eth.proto) {
        case ETHERTYPE_IP:
            parse_ip(eth.payload);
            break;

        case ETHERTYPE_ARP:
            parse_arp(eth.payload);
            break;
        }
        //print();
    }
#endif

	void print() {
	    //eth.print();
	}

	void print_hex() {
        int i;
        assert(size <= MAX_PKT_SIZE);
        for (i = 0; i < size; i++) {
            if (i % 16 == 0)
                printf("%04x ", i);
            if (buff[i] < 16)
                printf("0");
            printf("%x ", (u8) buff[i]);
            if ((i + 1) % 8 == 0)
                printf(" ");
            if ((i + 1) % 16 == 0)
                printf("\n");
        }
        if (size % 16 != 0)
            printf("\n");
	}

	/* NOTE:
	 * Most diffs are full replacement, but a few actually represent
	 * deltas: [TCP_SEQ, TCP_ACK, IP_ID] */
	void apply_diff(Header h, u64 v) {
        switch (h) {
        //TS_SEC
        //PCAP_SEQ
        case IP_HL:
            ip.hl = v;
            break;
        case IP_TOS_F:
            ip.tos = v;
            break;
        case IP_LEN:
            ip.len = v;
            break;
        case IP_ID: // DELTA
            ip.id += v;
            break;
        case IP_OFF:
            ip.off = v;
            break;
        case IP_TTL_F:
            ip.ttl = v;
            break;
        case IP_PROTO:
            ip.proto = v;
            break;
        case IP_CSUM:
            ip.csum = v;
            break;
        case IP_SRC:
            ip.src = v;
            break;
        case IP_DST:
            ip.dst = v;
            break;

        case TCP_SRC:
            tcp.src = v;
            break;
        case TCP_DST:
            tcp.dst = v;
            break;
        case TCP_SEQ: // DELTA
             tcp.seq += v;
            break;
        case TCP_ACK: // DELTA
            tcp.ack += v;
            break;
        case TCP_OFF:
            tcp.off = v;
            break;
        case TCP_FLAGS:
            tcp.flags = v;
            break;
        case TCP_WIN:
            tcp.win = v;
            break;
        case TCP_CSUM:
            tcp.csum = v;
            break;
        case TCP_URP:
            tcp.urp = v;
            break;

        case UDP_SRC:
            udp.src = v;
            break;
        case UDP_DST:
            udp.dst = v;
            break;
	    case UDP_CSUM:
	        udp.csum = v;
            break;
	    case UDP_LEN:
	        udp.len = v;
            break;
        }

	}

	HeaderValues get_headers() {
		u64 tstamp = ts.tv_sec;
		HeaderValues ret;
		//ret[TS_SEC] = tstamp;
		//ret[PCAP_SEQ] = seq;

		switch (eth.proto) {
		case ETHERTYPE_IP:
			HeaderValues retip = ip.get_headers();
			HeaderValues retp;

			switch (ip.proto) {
			case IPPROTO_TCP:
				retp = tcp.get_headers();
				break;

			case IPPROTO_UDP:
				retp = udp.get_headers();
				break;
			}

			update_headers(retip, retp);
			update_headers(ret, retip);
			break;
		}

		return ret;
	}

	// Writes from an STL vector into the provided buffer.
    // returns number of bytes written to buf
	uint pack(u8* buf) {
	    std::vector<u8> p = pack_eth();
	    int i;
	    for (i = 0; i < p.size(); i++) {
	        buf[i] = p[i];
	    }
	    return p.size();
	}

	uint pack_buf(u8* buf) {
	    return pack_eth_buf(buf);
	}

	std::vector<u8> pack_eth() {
	    std::vector<u8> rest;
        switch (eth.proto) {
            case ETHERTYPE_IP:
                rest = pack_ip();
                break;

            case ETHERTYPE_ARP:
                rest = pack_arp();
                break;
        }
        std::vector<u8> eth_packed = eth.pack();
        eth_packed.insert(eth_packed.end(), rest.begin(), rest.end());
        return eth_packed;
	}

	u8 pack_eth_buf(u8* buf) {
	    u8 rest;
        switch (eth.proto) {
            case ETHERTYPE_IP:
                rest = pack_ip_buf(buf + sizeof(ether_header));
                break;

            case ETHERTYPE_ARP:
                rest = pack_arp_buf(buf + sizeof(ether_header));
                break;
        }
        return this->eth.pack_buf(buf) + rest;
	}

	std::vector<u8> pack_ip() {
	    std::vector<u8> rest;
        switch (ip.proto) {
            case IPPROTO_TCP:
                rest = pack_tcp();
                break;

            case IPPROTO_UDP:
                rest = pack_udp();
                break;
        }
        std::vector<u8> ip_packed = ip.pack();
        ip_packed.insert(ip_packed.end(), rest.begin(), rest.end());
        return ip_packed;
	}

	u8 pack_ip_buf(u8* buf) {
        u8 rest;
        switch (ip.proto) {
            case IPPROTO_TCP:
                rest = pack_tcp_buf(buf + sizeof(struct ip));
                break;

            case IPPROTO_UDP:
                rest = pack_udp_buf(buf + sizeof(struct ip));
                break;
        }
        return this->ip.pack_buf(buf) + rest;
    }

    void unimplemented(const char* s) {
        printf("Exiting; %s unimplemented", s);
        exit(EXIT_FAILURE);
    }

	std::vector<u8> pack_arp() {
	    std::vector<u8> packed;
	    unimplemented("arp");
	    return packed;
	}

	u8 pack_arp_buf(u8* buf) {
        unimplemented("arp");
    }

	std::vector<u8> pack_udp() {
        return udp.pack();
    }

	u8 pack_udp_buf(u8* buf) {
	    return udp.pack_buf(buf);
    }

	std::vector<u8> pack_tcp() {
        return tcp.pack();
    }

	u8 pack_tcp_buf(u8* buf) {
        return tcp.pack_buf(buf);
    }

	void get_headers_opt(HVArray ret) {
		if (unlikely(eth.proto != ETHERTYPE_IP))
			return;

		ip.get_headers_opt(ret);
		switch (ip.proto) {
		case IPPROTO_TCP:
			tcp.get_headers_opt(ret);
			break;

		case IPPROTO_UDP:
			udp.get_headers_opt(ret);
			break;
		}
	}

	void parse_arp(const u8 *pkt) {
		arp = ARP(pkt);
	}

	void parse_ip(const u8 *pkt) {
		ip = IP(pkt);
		switch (ip.proto) {
		case IPPROTO_TCP:
			parse_tcp(ip.payload);
			break;

		case IPPROTO_UDP:
			parse_udp(ip.payload);
			break;
		}
	}

	void parse_tcp(const u8 *pkt) {
		tcp = TCP(pkt);
	}

	void parse_udp(const u8 *pkt) {
		udp = UDP(pkt);
	}

	u8 nw_proto() {
		switch(eth.proto) {
		case ETHERTYPE_ARP:
			return arp.op & 0xff;

		case ETHERTYPE_IP:
			return ip.proto;
		}
	}

	u32 nw_src() {
		switch(eth.proto) {
		case ETHERTYPE_ARP:
			return arp.nw_src;

		case ETHERTYPE_IP:
			return ip.src;
		}
	}

	u32 nw_dst() {
		switch(eth.proto) {
		case ETHERTYPE_ARP:
			return arp.nw_dst;

		case ETHERTYPE_IP:
			return ip.dst;
		}
	}

	u16 tp_src() {
		switch(ip.proto) {
		case IPPROTO_TCP:
			return tcp.src;

		case IPPROTO_UDP:
			return udp.src;

		case IPPROTO_ICMP:
			return icmp.type;
		}
	}

	u16 tp_dst() {
		switch(ip.proto) {
		case IPPROTO_TCP:
			return tcp.dst;

		case IPPROTO_UDP:
			return udp.dst;

		case IPPROTO_ICMP:
			return icmp.code;
		}
	}

	u16 infer_len() {
        int inferred_len = 0;
        if (!skip_ethernet) {
            assert (eth.proto == ETHERTYPE_IP);
            inferred_len += 14;
        }
        inferred_len += ip.len;
        return inferred_len;
	}
};

struct ofp_match {
#if defined(FULL_OF_MATCH)
	uint32_t wildcards;        /* Wildcard fields. */
	uint16_t in_port;          /* Input switch port. */
	uint8_t dl_src[ETHER_ADDR_LEN]; /* Ethernet source address. */
	uint8_t dl_dst[ETHER_ADDR_LEN]; /* Ethernet destination address. */
	uint16_t dl_vlan;          /* Input VLAN id. */
	uint8_t dl_vlan_pcp;       /* Input VLAN priority. */
	uint8_t pad1[1];           /* Align to 64-bits */

	uint16_t dl_type;          /* Ethernet frame type. */
	uint8_t nw_tos;            /* IP ToS (actually DSCP field, 6 bits). */
#endif
	uint32_t nw_src;           /* IP source address. */
	uint32_t nw_dst;           /* IP destination address. */

	uint16_t tp_src;           /* TCP/UDP source port. */
	uint16_t tp_dst;           /* TCP/UDP destination port. */
	uint32_t nw_proto;          /* IP protocol or lower 8 bits of
				    * ARP opcode. */
} __attribute__((packed));

/*
 * Ref: http://www.noxrepo.org/_/nox-doxygen/openflow-inl-1_80_8hh_source.html#l01099
 */
struct FlowKey {
	u8 key[sizeof(struct ofp_match)];
	struct ofp_match *match;
	u64 hsh;

	FlowKey() {}

	FlowKey(Packet &pkt) {
		bzero(key, sizeof(key));
		match = (struct ofp_match *)key;
#if defined(FULL_OF_MATCH)
		memcpy(match->dl_src, pkt.eth.src, ETHER_ADDR_LEN);
		memcpy(match->dl_dst, pkt.eth.dst, ETHER_ADDR_LEN);
		match->dl_vlan = pkt.eth.vlan;
		match->dl_vlan_pcp = pkt.eth.pcp;
		match->dl_type = pkt.eth.proto;
#endif
		match->nw_proto = pkt.nw_proto();

		match->nw_src = pkt.nw_src();
		match->nw_dst = pkt.nw_dst();

		match->tp_src = pkt.tp_src();
		match->tp_dst = pkt.tp_dst();

		hash();
	};

	void hash() {
		u64 *data = (u64 *)&key[0];
		hsh = data[0] ^ data[1];
	}

	bool operator<(const FlowKey &other) const {
		u64 *data0 = (u64 *)&key[0];
		u64 *data1 = (u64 *)&other.key[0];
		return data0[0] < data1[0] or (data0[0] == data1[0] and data0[1] < data1[1]);
	}

	bool operator==(const FlowKey &other) const {
		u64 *data0 = (u64 *)&key[0];
		u64 *data1 = (u64 *)&other.key[0];
		return data0[0] == data1[0] and data0[1] == data1[1];
	}

	void print() const {
		for(int i = 0; i < sizeof(key); i++) {
			printf("%02x.", key[i]);
			if ((i + 1) % 8 == 0)
				printf("  ");
		}
		printf("\n");
	}
};

struct HashFlowKey {
	size_t operator()(const FlowKey &fkey) const {
		return fkey.hsh;
	}
};
