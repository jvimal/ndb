#!/usr/bin/env python

import os
import sys
import argparse
import math
import glob
import json
import pprint
import re
from collections import OrderedDict

parser = argparse.ArgumentParser()
parser.add_argument("--dir",
                    nargs="+",
                    help="The list of directories to scan json summaries",
                    required=True)
parser.add_argument("--tries",
                    default=3)

"""
Point to the directories in --dir and this script will output
the data required for scatterplotting.
"""

args = parser.parse_args()
SEP = '*' * 100

def load_json(filename):
    ret = json.load(open(filename))
    ret["json_file"] = filename
    return ret

ttime = re.compile(r'(\d+)m([\d\.]+)s')
spaces = re.compile(r'\s+')
def parse_time(line):
    realtime = spaces.split(line.strip())[1]
    m = ttime.match(realtime)
    elapsed_sec = 0.0
    if m:
        minutes = float(m.group(1))
        seconds = float(m.group(2))
        elapsed_sec = minutes * 60 + seconds
    return elapsed_sec

class Results:
    def __init__(self, dir):
        self.rootdir = dir
        self.dir = dir
        self.readme = os.path.join(dir, "README")
        self.gz = dict()
        self.pcap = dict()
        for tries in [1]:
            self.dir = os.path.join(self.rootdir, str(tries))
        self.vj = self.parse_json("vj.json")
        self.vjgz = self.parse_json("vjgz.json")
        for ext in ["gz"]:#, "bz2", "lzma"]:
            self.parse_comp(ext)
        self.parse_pcap()

    def parse_pcap(self):
        self.pcap["type"] = "pcap"
        self.pcap["bpp"] = self.vj["pcap_bytes"] * 1.0 / self.vj["num_packets"]
        self.pcap["uspp"] = 0

    def parse_bz2(self):
        self.parse_comp("bz2")
    def parse_lzma(self):
        self.parse_comp("lzma")
    def parse_gzip(self):
        self.parse_comp("gz")

    def parse_comp(self, ext):
        filename = os.path.join(self.dir, "%s.log" % ext)
        setattr(self, ext, {})
        dict = getattr(self, ext)
        dict["time"] = 0
        dict["uspp"] = 0
        dict["bpp"] = 0
        dict["type"] = ext
        if not os.path.exists(filename):
            print filename, "does not exist"
            return
        lines = open(filename).readlines()
        for line in lines:
            if line.startswith("real"):
                dict["time"] = parse_time(line)
                dict["uspp"] = dict["time"] * 1e6 / self.vj["num_packets"]
            if line.startswith("-r"):
                size = int(line.split(' ')[4])
                dict["bpp"] = size * 1.0 / self.vj["num_packets"]
                dict["compressed"] = size
        dict["type"] = ext
        return

    def parse_json(self, filename):
        filename = os.path.join(self.dir, filename)
        if not os.path.exists(filename):
            print filename, "doesnt exist"
            return
        ret = load_json(filename)
        bpp = ret["running-stats"][-1]["bpp"]
        gzbpp = ret["running-stats"][-1]["gz-bpp"]

        ret["uspp"] = ret["exec_time_sec"] * 1e6 / ret["num_packets"]
        ret["bpp"] = bpp
        if gzbpp > 0:
            ret["bpp"] = gzbpp
        return ret

    def summary(self, json):
        keys = ["json_file", "pcap_file", "pcap_bytes", "num_packets", "num_flows", "exec_time_sec"]
        ppmetrics = ["bpp", "gz-bpp", "uspp", "fpp", "mem-bpp"]
        stats = dict()
        for k in keys:
            stats[k] = json[k]
        for ppm in ppmetrics:
            stats[ppm] = json["running-stats"][-1][ppm]
        pprint.pprint(stats)
        print SEP
        return

    def gzsummary(self):
        pprint.pprint(self.gz)
        print SEP

    def pcapsummary(self):
        pprint.pprint(self.pcap)
        print SEP

    def progress(self, param, json):
        ret = []
        for row in json["running-stats"]:
            read = row["read"]
            total = row["total"]
            percent = read * 100.0/total
            ret.append(('%.2f' % percent, row[param]))
        return ret

    def scatterplot_output(self):
        #methods = ["pcap", "gz", "bz2", "lzma", "vj", "vjgz"]
        methods = ["pcap", "gz", "vj", "vjgz"]
        FMT = "# trace, method, bpp, uspp"
        print FMT
        for method in methods:
            bpp = getattr(self, method)["bpp"]
            uspp = getattr(self, method)["uspp"]
            trace = os.path.basename(self.vj["pcap_file"])
            trace = trace.replace(".pcap", "")
            print "%s,%s,%.3f,%.3f" % (trace, method, bpp, uspp)
        return

for dir in args.dir:
    r = Results(dir)
    #r.summary(r.vj)
    #print r.progress("bpp", r.vj)
    #print SEP

    #r.summary(r.vjgz)
    #print r.progress("bpp", r.vjgz)
    #print SEP

    #r.gzsummary()
    #r.pcapsummary()

    r.scatterplot_output()
