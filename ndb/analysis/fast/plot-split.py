
import os
import sys
import argparse
import matplotlib
matplotlib.use("Agg")
import matplotlib.cm
import matplotlib as mp
import matplotlib.pyplot as plt
#from colour import Color # sigh...
from ndb.plot import plot_defaults_bpp_uspp
import json
import numpy as np
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("--files",
                    nargs="+",
                    required=True)

parser.add_argument('--out', '-o')
parser.add_argument('-n', type=int)

args = parser.parse_args()

mp.rc("figure", figsize=(8, 5.5))

LOCATIONS = ["dc", "campus", "wan"]

SHAPES = {
    'pcap': 's',  # square
    'gz': '^',  # triangle
    'ns': 'o', # circle,
    'ns+gz': '*', # star
    }

COLOURS = 'bgrcmyk'
COLOUR_BY_TRACE = {
    'wan': 'blue',
    'dc': 'orange',
    'campus': 'green'
    }

def get_trace_data(trace):
    type = 'wan'
    if "equinix" in trace:
        type = 'wan'
    elif "univ" in trace:
        type = 'dc'
    elif "clemson" in trace:
        type = 'campus'

    js = json.load(open(trace))
    return type, js

def transpose(lst):
    return zip(*lst)

data_type = defaultdict(str)
data = defaultdict(list)

for fname in args.files:
    type, ret = get_trace_data(fname)
    fname = fname.split('--')[0]
    data_type[fname] = type
    data[fname] += ret

for fname in data.keys():
    type = data_type[fname]
    D = sorted(data[fname])
    xs, bpps, gzbpps = transpose(D)
    col = COLOUR_BY_TRACE[type]
    plt.plot(xs, gzbpps, label=type, lw=2, color=col)
    #plt.plot(xs, bpps, lw=2, color=col, ls='--')

plt.legend(loc="lower right")
plt.grid(True)
plt.xscale("log")
plt.xlabel("Number of packets")
plt.ylabel("Bytes/packet")
plt.ylim((0, None))
if args.out:
    plt.savefig(args.out)
    print 'saved to', args.out
else:
    plt.show()
