#!/bin/bash


#time (for i in {0,1,2,3,4,5}; do
#    ./run_compression_analysis.sh /disk/hs03/scratch/jvimal/caida$i/*.pcap;
#done)

dir=/home/u1/jvimal/traces/caida/pcaps
pcaps=`echo /home/u1/jvimal/traces/caida/pcaps/*.anon.pcap`

for pcap in $pcaps; do
    ./run_compression_analysis.sh $pcap;

    # Delete the compression file so we don't consume space...
    bdir=$(basename $pcap);
    rm -f $bdir/*.read.pcap;
done

