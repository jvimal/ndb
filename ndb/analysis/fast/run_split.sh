#!/bin/bash

traces="/home/jvimal/traces/clemson/clemson-all.pcap "
traces+="/home/jvimal/traces/univ2/univ2.pcap "
traces+="/home/jvimal/traces/caida/equinix-sanjose.dirA.20121220-130000.UTC.anon.pcap "

max=10000000
#max=10000

nsplit=100000
jsons=""

for trace in $traces; do
    echo $trace
    python run_split_test.py -f $trace -n $max -s $nsplit --sweep -j $trace--sweep-$max.json
    jsons+="$trace--sweep-$max.json "
done

echo $jsons
python plot-split.py --files $jsons -n $max --out plot-split.pdf
