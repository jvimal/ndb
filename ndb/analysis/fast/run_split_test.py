#!/usr/bin/env python

import os
import sys
import argparse
import math
import glob
import json
import pprint
import re
from subprocess import Popen
import time

parser = argparse.ArgumentParser()
parser.add_argument("--file", '-f',
                    help="The pcap file to run the test on.",
                    required=True)

parser.add_argument("--nsplit", '-s',
                    help="The number of splits.",
                    type=int,
                    default=10)

parser.add_argument("--num-packets", '-n',
                    help="The number of packets in pcap to process.",
                    type=float,
                    default=1e6)

parser.add_argument("--dry-run",
                    help="Don't actually run, but just print commands.",
                    action="store_true",
                    default=False)

parser.add_argument("--all",
                    help="Run the compress-all case as well.",
                    action="store_true",
                    default=False)

parser.add_argument("--verbose", '-v',
                    help="Print verbose progress....",
                    action="store_true",
                    default=False)

parser.add_argument("--sweep",
                    help="Sweep till nsplits instead of running for one split",
                    action="store_true",
                    default=False)

parser.add_argument("--usec",
                    help="Use time slices instead of packet ranges.",
                    action="store_true",
                    default=False)

parser.add_argument("--json", '-j',
                    help="The json stats output file.",
                    default="/dev/null")

"""
Examples:

- Run for one instance:
python run_split_test.py -f ~/traces/caida/equinix-sanjose.dirA.20121220-130000.UTC.anon.pcap
       -n 1e5 -s 100

- Run for one instance but also show without splits:
python run_split_test.py -f ~/traces/caida/equinix-sanjose.dirA.20121220-130000.UTC.anon.pcap
       -n 1e5 -s 100 --all

- Run a sweep with splits 1, 10 and 100
python run_split_test.py -f ~/traces/caida/equinix-sanjose.dirA.20121220-130000.UTC.anon.pcap
       -n 1e5 -s 100 --sweep
"""

args = parser.parse_args()
if args.usec:
    args.usec = "-usec"
else:
    args.usec = ""

def fast_cmd(file, opts):
    FAST="./fast"
    if type(opts) == type([]):
        opts = ' '.join(opts)
    return "%s %s %s" % (FAST, file, opts)

def run_one_range(start, end):
    opts = [
        "--range%s %s %s" % (args.usec, start, end),
        "-d --mem",
        ]
    cmd = fast_cmd(args.file, opts)
    if args.dry_run:
        print cmd
        return
    start = time.time()
    Popen(cmd + " > /dev/null 2>&1;", shell=True).wait()
    end = time.time()
    return end - start

def stats():
    out = args.file + "---stats.json"
    js = json.load(open(out))
    bpp = js["bpp"]
    gzbpp = js["gzbpp"]
    num_bytes = js["total_size"]
    num_cbytes = js["total_csize"]
    num_packes = js["num_packets"]
    return bpp, gzbpp, num_bytes, num_cbytes, num_packets

def run_split(num_packets, nsplit):
    num_one_range = num_packets / nsplit
    if num_packets % nsplit != 0:
        print "WARNING: better to make splits divide total packets"

    if args.all:
        print "overall (no splits): "
        t = run_one_range(1, args.num_packets)
        bpp, gzbpp = stats()
        print "bpp: %.3f, gzbpp: %.3f [%d,%d] (%.3fs)" % (bpp, gzbpp, 1, args.num_packets, t)
        print "\nindividual:"

    bpp_total = 0
    gzbpp_total = 0
    num_bytes_total = 0
    num_cbytes_total = 0
    num_packets_total = 0
    count = 0
    for i in xrange(0, num_packets, num_one_range):
        start = i + 1
        end = start + num_one_range - 1
        t = run_one_range(start, end)
        bpp, gzbpp, num_bytes, num_cbytes, _ = stats()

        bpp_total += bpp
        gzbpp_total += gzbpp
        count += 1
        if args.verbose:
            print "bpp: %6.3f, gzbpp: %6.3f  running-avg:(%6.3f,%6.3f) (%d of %d) [%d,%d] (%.3fs)" \
                % (bpp, gzbpp, bpp_total/count, gzbpp_total/count, count, args.nsplit, start, end, t)

    if args.verbose:
        print ""
        print "average bpp: %6.3f, gzbpp: %6.3f" % (bpp_total/count, gzbpp_total/count)
    return (bpp_total/count, gzbpp_total/count)

if __name__ == "__main__":
    num_packets = int(args.num_packets)
    data = []
    if args.sweep:
        nsplit = 1
        while nsplit <= args.nsplit:
            bpp, gzbpp = run_split(num_packets, nsplit)
            print "split %6d, bpp %6.3f, gzbpp %6.3f" % (nsplit, bpp, gzbpp)
            data.append((num_packets/nsplit, bpp, gzbpp))
            nsplit *= 10
        f = open(args.json, 'w')
        json.dump(data, f)
        f.close()
    else:
        nsplit = args.nsplit
        bpp, gzbpp = run_split(num_packets, nsplit)
        print "split %6d, bpp %6.3f, gzbpp %6.3f" % (nsplit, bpp, gzbpp)
