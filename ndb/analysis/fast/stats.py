#!/usr/bin/env python

import json
import sys
import os

def print_stats(j):
    def pretty(x):
        FMT = '%.2f~%s'
        if x < 1e6 * 100:
            return FMT % (x/1e6, 'MB')
        if x < 1e9 * 100000:
            return FMT % (x/1e9, 'GB')
        return x
    pcap = os.path.basename(j['pcap_file'])
    wire_size = j['Ethernet']['IP']['bytes']
    wire_size_s = pretty(wire_size)

    pcap_size = j['pcap_bytes']
    pcap_size_s = pretty(pcap_size)

    pcap_bpp = pcap_size * 1.0 / j['num_packets']
    ppflow = j['num_packets'] * 1.0 / j['num_flows']
    ipppkt = j['unique_ips'] * 1.0 / j['num_packets']
    print "%s  &  %s  &   %s   &  %.3f  & %.2f  & %.g \\\\ " % (pcap, wire_size_s, pcap_size_s, pcap_bpp, ppflow, ipppkt)

print sys.argv[1]
print_stats(json.load(open(sys.argv[1])))
