#/bin/sh
# Read a file, strip it, compress it, and then decompress the stripped output.

# Uncomment below to leave in packets w/TCP options
# Out for now, until added to the decompressor.
STRIP_OPTS="--strip-tcpopts"
#STRIP_OPTS=

E_BADARGS=65
if [ $# -eq 0 ]
then
	echo "Usage: `basename $0` <filename> [opt_num_pkts] "
	exit $E_BADARGS
fi
input=$1

pkts=
if [ $# -eq 2 ]
then
	pkts="-P $2"
fi

echo ">>> Removing previous files"
if [[ "$input" != *.pcap ]]
then
	echo "input ${input} does not contain .pcap; exiting"
	exit
fi

rm ${input}.*
rm ${input}-*
set -e
echo ">>> Generating stripped input"
./fast ${input} --strip ${pkts} ${STRIP_OPTS}
stripped=${input}.read.pcap
echo ">>> Compressing stripped input"
./fast ${stripped} -d
echo ">>> Decompressing stripped input"
./dec ${stripped} -D
decompressed=${stripped}.out.pcap
in_size=`wc -c < ${stripped}`
out_size=`wc -c < ${decompressed}`
echo ">>> Comparing sizes"
if [ ${in_size} -ne ${out_size} ]
then
	echo "SIZES DON'T MATCH:"
	echo "input size: ${in_size}"
	echo "output size: ${out_size}"
else
	echo "Passed size check; both are: ${in_size}."
	echo ${stripped}
	in_md5=`md5sum ${stripped} | awk '{print $1}'`
	out_md5=`md5sum ${decompressed} | awk '{print $1}'`
	echo $in_md5 $out_md5
	if [[ $in_md5 = $out_md5 ]]
	then
		echo "Passed MD5 check; rock on."
	else
		echo "FAILED MD5 CHECK"
	fi
fi
