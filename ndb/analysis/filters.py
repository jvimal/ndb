"""
Filtering algorithms for postcard reduction.

For two-stage filtering:
TODO: add support for ports to path to cover inport/outport filtering
TODO: add support for actual node dpids in filtering fcns, not string names
"""
from collections import OrderedDict

from ndb.filter import PacketFilter, find_ipredicates

# Print out details for each filter w/each path?
DEBUG_FILTERS = False


def filter_fcn_null(ppfs, query_strings, path):
    return len(path)


def filter_fcn_opt(ppfs, query_strings, path):
    matches = [ppf.match(path) for ppf in ppfs]
    retval = len(path) if True in matches else 0
    return retval


def list_and(l):
    """Return the boolean AND of each element in a list."""
    if False in l:
        return False
    else:
        return True

def list_or(l):
    """Return the boolean OR of each element in a list."""
    if True in l:
        return True
    else:
        return False


def filter_fcn_ipred_both(ppfs, query_strings, path, stage):
    pcards = 0
    ipred_group_pfs = []  # Each elt is a list: ipred pf objects for that group.
    ipred_groups = [find_ipredicates(qs) for qs in query_strings]
    for ipred_group in ipred_groups:
        ipred_pfs = []
        for ipred in ipred_group:
            ipred_pfs.append(PacketFilter(ipred))
        ipred_group_pfs.append(ipred_pfs)

    # TODO: add optimization where ppf's that match nothing (e.g. '$')
    # generate no postcards, ever.  Seems like an obvious one.

    # If there are no imperative predicates, then every postcard is headed
    # to the collector.  Done.
    ipred_group_totals = [len(ipreds) for ipreds in ipred_groups]
    if sum(ipred_group_totals) == 0:
        return len(path)

    # TODO: matching process could be further optimized by considering the
    # position of each node.  As implemented below, ipredicates are all
    # considered for every node.
    # Must consider the OR of each ppf in the query.
    if DEBUG_FILTERS:
        print "\t\t ipreds: %s" % " | ".join([" / ".join(ipreds) for ipreds in ipred_groups])
    # Create scoreboard: one boolean value per ipred.
    ipred_group_scoreboard = []
    for ipred_group in ipred_groups:
        ipred_group_scoreboard.append([False] * len(ipred_group))
    for pcard in path:
        # Only one ipred in any string needs to match to send this out.
        ipred_matched = False
        for i, ipred_pfs in enumerate(ipred_group_pfs):
            for j, ipred_pf in enumerate(ipred_pfs):
                if ipred_pf.match(pcard):
                    ipred_matched = True
                    ipred_group_scoreboard[i][j] = True
        if ipred_matched:
            # Send a notify message
            pcards += 1

    if stage == 'total':
        # If every ipred matched some postcard in at least one group,
        # then get all postcards
        matched = list_or([list_and(l) for l in ipred_group_scoreboard])
        if matched:
            # Whether it matched or not, gotta check it.
            return len(path)
        # Otherwise, we don't ask for _any_ postcards; consider
        # notifies a different species.
        else:
            return 0

    elif stage == 'initial':
        # Initial only; these "pcards" are really notify messages.
        return pcards

    else:
        raise Exception("invalid stage: %s" % stage)


def filter_fcn_ipred_notifies(ppfs, query_strings, path):
    return filter_fcn_ipred_both(ppfs, query_strings, path, 'initial')


def filter_fcn_ipred_pcards(ppfs, query_strings, path):
    retval = filter_fcn_ipred_both(ppfs, query_strings, path, 'total')
    return retval
#    if ppf.match(path):
#        return len(path)
#    else:
#        return filter_fcn_ipred_initial(ppfs, query_strings, path)

# Dict mapping short filter names to more descriptive plot names.
FILTER_PLOT_NAMES = {
    'null': 'none\n(postcards)',
    'opt': 'optimal\n(postcards)',
    'ipred_pcards': 'two-stage\n(postcards)',
    'ipred_notifies': 'two-stage\n(notifies)'
}

# A filter refers to a filtering algorithm.
FILTERS = OrderedDict([
    ('null', filter_fcn_null),
    ('opt', filter_fcn_opt),
    ('ipred_pcards', filter_fcn_ipred_pcards),
    ('ipred_notifies', filter_fcn_ipred_notifies),
])

FILTER_ORDER = FILTERS.keys()

