import os
import sys
#import pickle
import cPickle as pickle

import dpkt
import dpkt.ethernet

from flowify_ofp_match import *
from packetlength import packet_len

from cStringIO import StringIO
from collections import defaultdict, Counter, OrderedDict

import argparse
import math
import struct
import pprint
import time
import compress
import itertools
from dpkt_readerpluslen import ReaderPlusLen
import pdb

# When diffcoding, we need to store the position (index into the
# packet) that has changed.  If we do diffcoding on a byte-by-byte
# basis, and a maximum header size of 64 bytes, we need a 6-bit
# descriptor for every byte change we record.  So every byte change (8
# bits) is noted as a 14 bit quantity (offset + new byte).  If we
# record n-byte diffs there are 64/n possible slots which require
# ilog2(64/n) bits.  So every n-byte word change becomes (n +
# ilog2(64/n)/8) bytes.  Of course the actual compression ratio will
# depend on the number of changes.  Sensible values of n: 2, 4 (which
# correspond to field lengths) and perhaps 8.  Or we can directly look
# at the field boundaries.
DIFF_BYTES = 1

# The Clemson trace showed us that ~1.5 bits enough to describe the
# number of contiguous bytes that changed for every such change.
DIFF_LEN_BYTES = 0.5

OFFSET_DESC_BYTES = math.log(64 / DIFF_BYTES, 2) / 8
# If we index the field that was changed.  I found just 40 header
# fields in the 100K pcap trace.  TODO: make this value dynamic.  This
# is what you would set if you assumed every field is equally likely
# to change.
FIELD_DESC_BYTES = math.log(64, 2) / 8

# From a 10MB Clemson run, I find that the entropy required to
# describe the field that changed within a packet is only about 3 bits
# per field.  So I am having 0.5 bytes (4 bits) per packet just in
# case...
FIELD_DESC_BYTES = 4 / 8.0

# From the same 10MB Clemson run, I find that the number of fields
# that changed per packet is typically small and can be described by 2
# bits on average.  Again, I set this cautiously to 4 bits.  Doesn't
# change by very much if I ignore IP.id, IP.csum and TCP.sum...
NUM_FIELDS_DESC_BYTES = 4 / 8.0

IGNORE_FIELDS_EXAMPLE = ["IP.sum", "IP.id", "TCP.sum", "UDP.sum"]
CHECK_FIELDS_EXAMPLE = ['IP.len', 'UDP.ulen', "TCP.flags", "TCP.win"]
TCP_DELTA_FIELDS = ["TCP.seq", "TCP.ack"]

parser = argparse.ArgumentParser()
parser.add_argument("--from-pcap", "-p", default=True,
                    action="store_true",
                    help="Read directly from pcap file.")
parser.add_argument("--file", "-f", required=True,
                    help="Pickled lookup_table_flow/pcap file to parse.")
parser.add_argument("--verbose", "-v", default=False,
                    action="store_true")
parser.add_argument("--ip", default=False,
                    help="pcap file contains IP packets without Ethernet headers.",
                    action="store_true")
parser.add_argument('--diffbytes', '-d', default=1, type=int,
                    help="Diff @diff bytes bytes at a time.")
parser.add_argument('--ignore-fields', '-i',
                    nargs="+",
                    default=IGNORE_FIELDS_EXAMPLE)
parser.add_argument('--check-fields', '-c',
                    nargs="+",
                    default=CHECK_FIELDS_EXAMPLE)
args = parser.parse_args()

DIFF_BYTES = args.diffbytes
OFFSET_DESC_BYTES = math.log(64 / DIFF_BYTES, 2) / 8

def get_headers(pkt):
    ret = dict()
    while True:
        if type(pkt) == str or type(pkt) == type(None):
            break
        hdr = pkt.__hdr__
        for name, fmt, _ in hdr:
            hdrname = "%s.%s" % (pkt.__class__.__name__, name)
            ret[hdrname] = (getattr(pkt, name), struct.calcsize(fmt))
        pkt = pkt.data
    return ret

lookup_table = dict()
class Flow:
    flows = []
    changed_headers = defaultdict(lambda: defaultdict(int))
    total_packets = 0
    fields_seen = set([])
    firstpacket_pcap = None
    num_fields_changed_packet = defaultdict(Counter)
    num_pkts_flow = Counter()
    field_value_freq = defaultdict(lambda: defaultdict(Counter))
    tcp_delta = defaultdict(lambda: defaultdict(Counter))
    csize = defaultdict(int)

    def __init__(self, flowkey, packets, packetlengths):
        self.flowkey = flowkey
        self.packets = packets
        self.packetlengths = packetlengths
        self._size = None
        self.len = len(packets)
        if self.len > 0:
            Flow.num_pkts_flow[self.len] += 1
        Flow.total_packets += self.len
        Flow.flows.append(self)

        self.firstpacket = None
        self.lastpacket = None

    def add_packet(self, pkt, len):
        if self.len > 0:
            Flow.num_pkts_flow[self.len] -= 1
        Flow.total_packets += 1
        #self.packets.append(pkt)
        #self.packetlengths.append(len)
        self.len += 1
        if self._size is None:
            self._size = 0
        self._size += len
        Flow.num_pkts_flow[self.len] += 1

        if self.firstpacket is None:
            self.firstpacket = pkt
            self.packets.append(str(pkt))
            self.write_packet()
            self.firstheader = get_headers(pkt)
            for h, _ in self.firstheader.iteritems():
                Flow.fields_seen.add(h)
            self.prevheader = self.firstheader
        else:
            newheaders = get_headers(pkt)
            #self.incremental_compress(newheaders, self.firstheader, "diff_field_with_first")
            self.incremental_compress(newheaders, self.prevheader, "diff_field_with_prev")
            del self.prevheader
            del pkt
            self.prevheader = newheaders
        return

    def incremental_compress(self, newheaders, compare_with, alg):
        csize = NUM_FIELDS_DESC_BYTES
        num_changed = 0
        # TCP specific changes
        self.record_tcp_deltas(newheaders, compare_with, alg)
        for h, (val, sz) in newheaders.iteritems():
            if h in args.ignore_fields:
                continue
            try:
                prev = compare_with[h][0]
            except Exception, e:
                if args.verbose:
                    print "New field %s in packet of same flow" % h
                # so we always record it as a different value...
                prev = None
            if val != prev:
                Flow.changed_headers[alg][h] += 1
                # For now we note the size of the entire value....
                csize += sz + FIELD_DESC_BYTES
                num_changed += 1
                # Note that this field takes on a particular
                # value.  Useful to compute empirical entropy for
                # this field.
                if h in args.check_fields:
                    Flow.field_value_freq[alg][h][val] += 1
        # To compute the avg number of fields that change per
        # packet...
        Flow.num_fields_changed_packet[alg][num_changed] += 1
        Flow.csize[alg] += csize

    def size(self):
        if self._size is not None:
            return self._size
        self._size = sum(self.packetlengths)
        return self._size

    def record_tcp_deltas(self, curr, prev, calg):
        fields = ["TCP.seq", "TCP.ack"]
        Flow.warnings = 0
        for field in fields:
            currval = curr.get(field, None)
            if currval is None:
                continue
            prevval = prev.get(field, None)
            if prevval is None:
                Flow.warnings += 1
                if args.verbose:
                    print "Warning.  Field %s dissappeared in the packet..." % field
                continue
            if currval[0] - prevval[0] != 0:
                Flow.tcp_delta[calg][field][currval[0] - prevval[0]] += 1
        return

    def write_packet(self):
        firstpkt = self.packets[0]
        Flow.firstpacket_pcap.writepkt(firstpkt)
        return

def read_flows(fname):
    f = open(fname)
    ret = pickle.load(f)
    f.close()
    return ret

def headers(pkt):
    """
    Clears the first unparseable 'data' field in the dpkt structure.
    Assume we want to record everything else.
    """
    newpkt = pkt
    tmp = newpkt
    while True:
        if type(tmp.data) == str:
            tmp.data = ''
            break
        tmp = tmp.data
    return newpkt

def entropy(freqs):
    total = sum(freqs)
    probabilities = map(lambda f: float(f)/total, freqs)
    bits = 0
    for prob in probabilities:
        assert prob <= 1.0
        if prob == 0:
            continue
        bits += prob * math.log(1.0 / prob, 2)
    return bits

def best_compress_algo(filepath):
    # Runs tgz, bz2 and lzma and returns the best compression
    # algorithm out of the lot and the filesize.  It is always(?)
    # lzma.
    alg_name = ''
    alg_size = None
    for fcn in [compress.lzma]:
        output, output_filepath = fcn(filepath)
        out_size = -1
        if os.path.exists(filepath):
            out_size = os.path.getsize(output_filepath)
            if alg_size is None or out_size < alg_size:
                alg_size = out_size
                alg_name = fcn.__name__
    return alg_name, alg_size


DIFF_FIELD_ALGS = ["diff_field_with_prev"]
FIELD_SIZE_BYTES = {"IP.len": 2,
                    "UDP.ulen": 2,
                    "TCP.seq": 4,
                    "TCP.ack": 4,
                    "TCP.flags": 1,
                    "TCP.win": 2}

# Our "domain-specific" compression algorithms
calg_name_fns = [
    #("diff_field_with_first", "compress_diff_field_with_first"),
    ("diff_field_with_prev", "compress_diff_field_with_prev")]

calgs = OrderedDict()
for k, v in calg_name_fns:
    calgs[k] = v

PRINT_PROGRESS_INTERVAL = 5 # seconds
PRINT_STATS_INTERVAL_BYTES = 100 * 1e6 # bytes

def print_counter_table(counter, limit=30):
    items = [ (times, thing) for (thing, times) in counter.iteritems() ]
    freq = [times for (times, _) in items]
    entropy_thing = entropy(freq)
    total = sum(freq)
    items.sort()
    printed = 0
    for (times, thing) in reversed(items):
        printed += 1
        print "%10s: %6.3f%%" % (thing, times * 100.0 / total)
        if printed >= limit:
            break
    return entropy_thing

def stats(total_csize, num_flows, firstpkt_file_size, firstpkt_filename, total_size, out="/dev/null"):
    stdout = sys.stdout
    sys.stdout = open(out, 'w')
    SEP = "**" * 80
    print "\nTotal packets: %d" % Flow.total_packets
    print "\tTotal flows: %d" % num_flows
    print "Fields seen --"
    print "Len: %d" % len(Flow.fields_seen)
    pprint.pprint(list(Flow.fields_seen))

    print "\nFields that changed -- (% of all changes)"
    print "Ignoring fields: %s" % args.ignore_fields
    print "\nNumber of fields per packet that changed -- (% of all changes)"
    for calg in DIFF_FIELD_ALGS:
        print "Number of field changes per packet"
        entropy_num_changed = print_counter_table(Flow.num_fields_changed_packet[calg])
        print "Which fields changed?"
        entropy_fields = print_counter_table(Flow.changed_headers[calg])
        print "\tEntropy      : %9.3f (%2.2fB) for %s" % (entropy_num_changed, entropy_num_changed/8.0, calg)
        print "\tField pointer: %9.3f (%2.2fB) for %s" % (entropy_fields, entropy_fields/8.0, calg)
    print SEP

    print "\nPackets/flow -- (% of all lengths)"
    entropy_pkts_flow = print_counter_table(Flow.num_pkts_flow, limit=10)
    print SEP

    print "\nField entropy for %s" % args.check_fields
    entropy_field = defaultdict(dict)
    # This should be parameterised by field-compression algo
    for alg, field in itertools.product(DIFF_FIELD_ALGS, args.check_fields):
        print alg, field, "---"
        table = Flow.field_value_freq[alg][field]
        E = print_counter_table(table, limit=5)
        entropy_field[alg][field] = E
    print SEP

    print "\nTCP delta entropy for fields %s" % TCP_DELTA_FIELDS
    for field in TCP_DELTA_FIELDS:
        table = Flow.tcp_delta["diff_field_with_prev"][field]
        E = print_counter_table(table, limit=10)
        print "\t=> %s entropy: %.3f" % (field, E)
        entropy_field["diff_field_with_prev"][field] = E
        entropy_field["diff_field_with_first"][field] = E
    print SEP

    # TODO: for each entity that goes into the file (field descriptor,
    # flow descriptor, number of changes in the packet, etc.) we can
    # compute the fraction it contributes to the total file size.  It
    # gives us a sense of what can be compressed and what cannot be
    # and what to go after and how.
    print "\nEntropy (in bits):"
    entropy_num_flows = math.log(num_flows, 2)
    print "\tNum flows    : %9.3f (%2.2fB)" % (entropy_num_flows, entropy_num_flows/8.0)
    print "\tNum pkts/flow: %9.3f (%2.2fB)" % (entropy_pkts_flow, entropy_pkts_flow/8.0)
    print "\tFor fields --"
    for alg, field in itertools.product(DIFF_FIELD_ALGS, args.check_fields + TCP_DELTA_FIELDS):
        E = entropy_field[alg][field]
        count = sum(Flow.field_value_freq[alg][field].itervalues())
        reduction_bytes = count * (FIELD_SIZE_BYTES[field] - E/8.0)
        # New compression algorithm to compress values in a field...
        value_calg = alg+"-value+delta"
        newsize = total_csize[alg] - reduction_bytes
        total_csize[value_calg] = newsize
        print "\t%13s: %9.3f (%2.2fB), count %d, redn %.2fB, newsize %.2f, for alg: %s" % (field, E, E/8.0, count, reduction_bytes, newsize, alg)

    print "\nTotal size of all packets on wire: %s bytes" % (total_size)
    print "First packet of all flows size: %d bytes" % firstpkt_file_size
    alg_name, alg_size = best_compress_algo(firstpkt_filename)
    ratio = float(firstpkt_file_size) / alg_size
    print "\t%s compresses it best: %d bytes (ratio: %.3f)" % (alg_name, alg_size, ratio)

    print "\nFlow compression methods -- (ratio of total est. bytes on wire to compressed size)"
    FLOW_DESCRIPTOR_SIZE = (entropy_pkts_flow + entropy_num_flows) / 8.0
    newlist = total_csize.keys()
    for key in calgs:
        newlist.remove(key)
    for alg in calgs.keys() + newlist:
        csize = total_csize[alg]
        # The final compressed file includes the first packet
        # (compressed) and for each flow the (num packets per flow,
        # list of diff descriptors)
        dsize = csize
        csize += alg_size + num_flows * FLOW_DESCRIPTOR_SIZE
        if csize == 0:
            print "ERR: Compressed file size cannot be 0, so assuming -1."
            csize = -1
        ratio = float(total_size) / csize
        bytes_per_packet = float(csize) / Flow.total_packets
        print "alg: %35s,   dsize: %10d,   csize: %10d,   ratio: %9.3f,   bytes-per-packet: %9.3f" % (alg, dsize, csize, ratio, bytes_per_packet)
    sys.stdout.close()
    sys.stdout = stdout
    if out != "/dev/null":
        os.system("grep '^alg:' %s" % (out))

def from_pcap(args):
    """Directly read from pcap file without having to generate the
    flowkeys pickle file."""
    pcap_filename = args.file
    pcap_filesize = os.path.getsize(pcap_filename)

    firstpkt_filename = args.file + ".firstpacket.pcap"
    firstpkt_file = open(firstpkt_filename, "w")
    Flow.firstpacket_pcap = dpkt.pcap.Writer(firstpkt_file)

    num_processed_flows = 0
    # For progress stats
    total_pkts = 0
    total_size = 0
    target_stat_bytes = PRINT_STATS_INTERVAL_BYTES
    prev_read = 0
    prev_timestamp = time.time()
    ignored_packets = 0
    ignored_bytes = 0
    def update_status(f, prev_read, prev_timestamp, total_pkts, total_flows):
        # IN case the pcap file keeps changing...
        pcap_filesize = os.path.getsize(pcap_filename)
        curr_timestamp = time.time()
        if curr_timestamp - prev_timestamp > PRINT_PROGRESS_INTERVAL:
            curr_ptr = f.tell()
            percent = curr_ptr * 100.0 / pcap_filesize
            bytes_read = curr_ptr - prev_read
            prev_read = curr_ptr
            dt = curr_timestamp - prev_timestamp
            prev_timestamp = curr_timestamp
            read_rate_mbps = bytes_read * 8.0 / dt / 1e6
            estimated_time_sec = (pcap_filesize - curr_ptr) * 8.0 / 1e6 / read_rate_mbps
            print "%d packets read, %d flows found, %dB/%dB: %.3f%% complete, rate: %.3f Mb/s, est-remaining: %ds" % (total_pkts, total_flows, f.tell(), pcap_filesize, percent, read_rate_mbps, estimated_time_sec)
        return prev_read, prev_timestamp

    def parse_pkt(buf):
        if args.ip:
            return dpkt.ip.IP(buf)
        return dpkt.ethernet.Ethernet(buf)

    def safe_dpkt_reader(pcap):
        try:
            for ts, buf, orig_len in pcap:
                try:
                    pkt = parse_pkt(buf)
                except IndexError as e:
                    continue
                yield ts, pkt, buf, orig_len
        except dpkt.dpkt.NeedData as e:
            raise Exception("check validity of pcap input")
        raise StopIteration

    f = open(args.file, 'r')
    pcap = ReaderPlusLen(f)
    for ts, pkt, buf, orig_len in safe_dpkt_reader(pcap):
        total_pkts += 1
        total_size += orig_len
        prev_read, prev_timestamp = update_status(f, prev_read, prev_timestamp, total_pkts, num_processed_flows)
        newpkt = parse_pkt(buf)
        if args.ip:
            flow_dpkt, klen = flowify_ip(newpkt)
        else:
            flow_dpkt, klen = flowify_ofp_match(newpkt)
        key = str(flow_dpkt)[:klen]
        if key not in lookup_table:
            flow = Flow(key, [], [])
            lookup_table[key] = flow
            num_processed_flows += 1
        else:
            flow = lookup_table[key]

        try:
            flow.add_packet(pkt, orig_len)
        except:
            ignored_packets += 1
            ignored_bytes += orig_len
            continue

        if total_size > target_stat_bytes:
            firstpkt_file.flush()
            firstpkt_file_size = os.path.getsize(firstpkt_filename)
            percent = f.tell() * 100.0 / pcap_filesize
            outfile = "/tmp/%s-stats-%dpc" % (os.path.basename(args.file), percent)
            stats(Flow.csize, num_processed_flows, firstpkt_file_size, firstpkt_filename, total_size, outfile)
            target_stat_bytes += PRINT_STATS_INTERVAL_BYTES

    firstpkt_file.close()
    firstpkt_file_size = os.path.getsize(firstpkt_filename)
    prev_read, prev_timestamp = update_status(f, prev_read, prev_timestamp, total_pkts, total_size)
    outfile = "/tmp/%s-stats-100pc" % (os.path.basename(args.file))
    stats(Flow.csize, num_processed_flows, firstpkt_file_size, firstpkt_filename, total_size, outfile)
    print "ignored %d packets, %dB, %.3f%%" % (ignored_packets, ignored_bytes, float(ignored_bytes*100)/total_size)
    return

if __name__ == "__main__":
    start = time.time()
    try:
        from_pcap(args)
    except Exception, e:
        import traceback
        traceback.print_exc()
        print "stepping into debugger..."
        pdb.set_trace()
    end = time.time()
    print "finished in %.3f seconds" % (end - start)
