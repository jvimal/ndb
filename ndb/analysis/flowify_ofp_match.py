import dpkt

"""
From openflow/include/openflow/openflow.h:

/* Fields to match against flows */
struct ofp_match {
    uint32_t wildcards;        /* Wildcard fields. */
    uint16_t in_port;          /* Input switch port. */
    uint8_t dl_src[OFP_ETH_ALEN]; /* Ethernet source address. */
    uint8_t dl_dst[OFP_ETH_ALEN]; /* Ethernet destination address. */
    uint16_t dl_vlan;          /* Input VLAN id. */
    uint8_t dl_vlan_pcp;       /* Input VLAN priority. */
    uint8_t pad1[1];           /* Align to 64-bits */
    uint16_t dl_type;          /* Ethernet frame type. */
    uint8_t nw_tos;            /* IP ToS (actually DSCP field, 6 bits). */
    uint8_t nw_proto;          /* IP protocol or lower 8 bits of
                                * ARP opcode. */
    uint8_t pad2[2];           /* Align to 64-bits */
    uint32_t nw_src;           /* IP source address. */
    uint32_t nw_dst;           /* IP destination address. */
    uint16_t tp_src;           /* TCP/UDP source port. */
    uint16_t tp_dst;           /* TCP/UDP destination port. */
};
OFP_ASSERT(sizeof(struct ofp_match) == 40);
"""
OFP_MATCH_LEN = 40

OFP_MATCH_PAD1_B = 1
OFP_MATCH_PAD2_B = 2
OFP_MATCH_WILDCARDS_B = 4
OFP_MATCH_INPORT_B = 2
OFP_MATCH_DL_VLAN_B = 2
OFP_MATCH_DL_VLAN_PCP_B = 1
OFP_MATCH_UNUSED_B = (
    OFP_MATCH_PAD1_B + OFP_MATCH_PAD2_B + OFP_MATCH_WILDCARDS_B +
    OFP_MATCH_INPORT_B + OFP_MATCH_DL_VLAN_B + OFP_MATCH_DL_VLAN_PCP_B
)
OFP_MATCH_USED_B = OFP_MATCH_LEN - OFP_MATCH_UNUSED_B
VERBOSE = False

def zero_all(pkt):
    zero_others(pkt, [])

def zero_others(pkt, fields_to_keep):
    """Zero all fields whose names aren't provided.

    pkt: dpkt Packet object
    """
    # Verify that fields_to_keep are actually field in the packet!
    assert hasattr(pkt, '__hdr__')
    pkt_hdr_fields = [name for name, structfmt, default in pkt.__hdr__]
    for field in fields_to_keep:
        if field not in pkt_hdr_fields:
            raise Exception("unknown header field to zero: %s; pkt %s; valid values: %s" %
                            (field, pkt, pkt_hdr_fields))
    # Can all fields actually be zeroed?
    for name, structfmt, default in pkt.__hdr__:
        if name not in fields_to_keep:
            setattr(pkt, name, default)

def zero_fields(pkt, fields):
    """Zero fields whose names are provided...

    pkt: dpkt Packet object
    """
    # Verify that fields_to_keep are actually field in the packet!
    assert hasattr(pkt, '__hdr__')
    pkt_hdr_fields = ["%s.%s" % (pkt.__class__.__name__, fieldname) \
                          for fieldname, structfmt, default in pkt.__hdr__]
    # Can all fields actually be zeroed?
    for fieldname, structfmt, default in pkt.__hdr__:
        name = "%s.%s" % (pkt.__class__.__name__, fieldname)
        if name in fields:
            setattr(pkt, fieldname, default)

def zero_all_fields(_pkt, fields):
    pkt = _pkt
    while True:
        if type(pkt) == str or type(pkt) == type(None):
            break
        zero_fields(pkt, fields)
        pkt = pkt.data
    return _pkt

def wipe_data(pkt):
    """Clear the following data field on a dpkt."""
    pkt.data = ''

def flowify_ofp_match(pkt):
    """Zero fields that are not part of a 'struct ofp_match' flow key

    Input:
    - pkt: dpkt Ethernet Pkt object

    Returns:
    - pkt: flowified dpkt Pkt object
    - klen: num bytes that comprise the flow key's "used" headers.  E.g.:
        For an L2 flow, num bytes up to the end of the Eth header.
        For an L3 flow, num bytes to the end of the IP header.

    The advantage of zeroing out everything unknown is that it maximally
    compresses the packet trace into flows, in the way a simple,
    OpenFlow-field-based collector would do.

    The disadvantage is that some of the output "flows" are sufficiently
    different to be differentiated, but we haven't encoded that logic here.

    The alternative approach would be the opposite: use every field, with
    no zeros, which would potentially result in a much higher number of
    "flows", but with things like different IP IDs or data headers.

    Another design choice: rather than smush only the known fields together,
    simply zero out unknown or unused ones.  Leads to slightly longer input
    keys but simpler code.

    Known fields based on the OpenFlow 1.0 spec, pg 8, Fig 2:
    Packet flow in an OpenFlow switch.
    http://www.openflow.org/documents/openflow-spec-v1.0.0.pdf
    """
    return flowify_eth(pkt)


ETH_TYPES_CHOP = [
    267, # No idea about this one!
    273, # Maybe IEEE802.3 frame length value?
    137, 554, 552, # STP?
    35020,  # 0x88cc LLDP
    34525,  # 0x86dd IPv6
    8196, # 0x2004 DTP?  Dynamic Trunking Protocol
    8193, # 0x2001 ?
]

def flowify_eth(pkt):
    # Assume the dst, src, and type fields are present, and identifying.
    # No zeroing for Ethernet packets.
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    if pkt.type == dpkt.ethernet.ETH_TYPE_8021Q:
        # Demux
        #pkt, klen = flowify_vlan(pkt.data)
        #return pkt, klen + this_hdr_klen
        print "saw VLAN-tagged packet: %s" % pkt
        print dir(pkt)
        exit()
    elif pkt.type == dpkt.ethernet.ETH_TYPE_ARP:
        ignore, klen = flowify_arp(pkt.data)
        return pkt, klen + this_hdr_klen
    elif pkt.type == dpkt.ethernet.ETH_TYPE_IP:
        ignore, klen = flowify_ip(pkt.data)
        return pkt, klen + this_hdr_klen
    elif pkt.type in ETH_TYPES_CHOP:
        wipe_data(pkt)
        return pkt, this_hdr_klen
    else:
        if VERBOSE:
            print "unknown eth type:%s for pkt %s" % (pkt.type, repr(pkt))
        wipe_data(pkt)
        return pkt, this_hdr_klen


def flowify_arp(pkt):
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    zero_others(pkt, ['spa', 'tpa'])
    wipe_data(pkt)
    return pkt, this_hdr_klen



IP_PROTOS_CHOP = [
    dpkt.ip.IP_PROTO_VRRP,
    dpkt.ip.IP_PROTO_IGMP,
    dpkt.ip.IP_PROTO_PIM,
    dpkt.ip.IP_PROTO_ESP,
    dpkt.ip.IP_PROTO_EIGRP
]

def flowify_ip(pkt):
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    # Declared fragmented if more fragment bit set of fragment offset nonzero.
    fragment = ((pkt.off & (dpkt.ip.IP_MF | dpkt.ip.IP_OFFMASK)) != 0)
    zero_others(pkt, ['src', 'dst', 'p'])
    if fragment:
        # Spec says to zero the transport ports that follow.
        return pkt, 0
    elif pkt.p == dpkt.ip.IP_PROTO_UDP:
        ignore, klen = flowify_udp(pkt.data)
        return pkt, klen + this_hdr_klen
    elif pkt.p == dpkt.ip.IP_PROTO_TCP:
        ignore, klen = flowify_tcp(pkt.data)
        return pkt, klen + this_hdr_klen
    elif pkt.p == dpkt.ip.IP_PROTO_ICMP:
        ignore, klen = flowify_icmp(pkt.data)
        return pkt, klen + this_hdr_klen
    elif pkt.p in IP_PROTOS_CHOP:
        wipe_data(pkt)
        return pkt, this_hdr_klen
    else:
        if VERBOSE:
            print "unknown ip protocol field:%s for pkt %s" % (pkt.p, repr(pkt))
        wipe_data(pkt)
        return pkt, this_hdr_klen


def flowify_udp(pkt):
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    zero_others(pkt, ['sport', 'dport'])
    wipe_data(pkt)
    return pkt, this_hdr_klen


def flowify_tcp(pkt):
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    zero_others(pkt, ['sport', 'dport'])
    wipe_data(pkt)
    return pkt, this_hdr_klen


def flowify_icmp(pkt):
    this_hdr_klen = pkt.__hdr_len__ #len(pkt.pack_hdr())
    #assert this_hdr_klen == len(pkt.pack_hdr())
    zero_others(pkt, ['type', 'code'])
    wipe_data(pkt)
    return pkt, this_hdr_klen
