import struct

import dpkt

HANDLE_IPV6_LENGTHS = True
ARP_LEN = 28

IPV4_MIN_LEN = 20
IPV4_MAX_LEN = 1500

def verify_ipv4_len(ip_len):
    if (ip_len < IPV4_MIN_LEN):
        print "WARN: ip_len < %i???: %i" % (ip_len, IPV4_MIN_LEN)
        return False
    elif (ip_len > IPV4_MAX_LEN):
        print "WARN: ip_len > %i???: %i" % (ip_len, IPV4_MAX_LEN)
        return False
    return True


IPV6_MIN_LEN = 40
IPV6_MAX_LEN = 1500
IPV6_MIN_HDR_LEN = IPV6_MIN_LEN

def get_ipv6_len(ipv6):
    plen = ipv6.plen
    hlen = IPV6_MIN_HDR_LEN
    for ext_hdr, buf in ipv6.extension_hdrs.iteritems():
        if buf is not None:
            hlen += len(buf)
            print "WARN: added ext hdr with len: %i type: %i" % (len(buf), ext_hdr)
    return plen + hlen


def verify_ipv6_len(ip_len):
    if (ip_len < 40):
        print "WARN: ip_len < %i???: %i" % (ip_len, IPV6_MIN_LEN)
        return False
    elif (ip_len > 1500):
        print "WARN: ip_len > %i???: %i" % (ip_len, IPV6_MAX_LEN)
        return False
    return True


ETH_TYPE_LLDP = 0x88cc

def is_8023(buf):
    eth_type = struct.unpack('>H', buf[12:14])[0]
    return eth_type > 0 and eth_type <= dpkt.ethernet.ETH_LEN_MAX

def is_ISL(buf):
    # From http://www.cisco.com/en/US/tech/tk389/tk689/technologies_tech_note09186a0080094665.shtml
    # ISL (Inter-Switch Link) is an encap format.
    return buf.startswith('\x01\x00\x0c\x00\x00')

def eth_type_is_known(eth_type):
    if (eth_type == dpkt.ethernet.ETH_TYPE_IP or
        eth_type == dpkt.ethernet.ETH_TYPE_ARP or
        HANDLE_IPV6_LENGTHS and (eth_type == dpkt.ethernet.ETH_TYPE_IP6) or
        eth_type == ETH_TYPE_LLDP):
        return True
    return False


def packet_len(curr_dpkt, buf = None, packet_index = 0):
    """Extract the packet length if possible.

    return the full length if known, 0 if indeterminate, otherwise None.

    if buf is provided, handle a special-case that confuses dpkt.
    if packet_index is provided, print that out, too.
    """
    eth_type = curr_dpkt.type
    pkt_len = 0

    if (buf and (is_ISL(buf) or is_8023(buf))) or eth_type_is_known(eth_type):
        # Handle VLAN tagged packets; presumably you can't have an 802.3
        # packet with a VLAN tag, or Ethernet pkt with VLAN = 0.
        if (eth_type > dpkt.ethernet.ETH_LEN_MAX and
            hasattr(curr_dpkt, 'tag') and
            curr_dpkt.tag != 0):
            pkt_len = 4

        # ISL: Dpkt parses past this field and gives no length field.
        # Handle 802.3 the same way.
        if buf and (is_ISL(buf) or is_8023(buf)):
            len_8023 = struct.unpack('>H', buf[12:14])[0]
            pkt_len = dpkt.ethernet.ETH_HDR_LEN + len_8023
        elif eth_type == dpkt.ethernet.ETH_TYPE_IP:
            ip = curr_dpkt.data
            if not verify_ipv4_len(ip.len):
                print "WARN: invalid ipv4 len at pkt %i: %i" % (packet_index, ip.len)
            pkt_len += ip.len + dpkt.ethernet.ETH_HDR_LEN
        elif eth_type == dpkt.ethernet.ETH_TYPE_ARP:
            arp = curr_dpkt.data
            arp_max_len = len(arp)
            if arp_max_len < ARP_LEN:
                print "WARN: invalid arp len at pkt %i: %i" % (packet_index, arp_max_len)
            pkt_len += ARP_LEN + dpkt.ethernet.ETH_HDR_LEN
        elif HANDLE_IPV6_LENGTHS and (eth_type == dpkt.ethernet.ETH_TYPE_IP6):
            ipv6 = curr_dpkt.data
            ipv6_len = get_ipv6_len(ipv6)
            if not verify_ipv6_len(ipv6_len):
                print "WARN: invalid ipv6 len at pkt %i: %i" % (packet_index, ipv6_len)
            pkt_len = ipv6_len + dpkt.ethernet.ETH_HDR_LEN
        elif eth_type == ETH_TYPE_LLDP:
            #print "WARN: indeterminate LLDP pkt length at pkt %i" % (packet_index)
            pkt_len = 0
    else:
        pkt_len = None
    return pkt_len

