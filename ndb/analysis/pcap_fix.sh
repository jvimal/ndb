#!/bin/sh
# Program to fix pcap files with a last packet truncated, such as 
# by forcibly killing the writing process w/Ctrl-C.
EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` {filename}"
  exit $E_BADARGS
fi

echo "processing file..."
ORIGINAL=$1
BACKUP="${1}.bak"
tcpdump -n -r $ORIGINAL -w $BACKUP
cp $BACKUP $ORIGINAL
rm $BACKUP
echo "done."
