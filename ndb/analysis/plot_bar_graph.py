import os
import matplotlib.pyplot as plt
from ndb.analysis import plot_defaults
import numpy as np

# Plot-specific options
COLORS = ["r", "g", "b", "c", "m", "y", "k", "purple"]
DPI = 300

def plot_data_bar(plot_dir, data, show_plot, save_plot, group_order,
                  subgroup_order, analysis_name,
                  height_fcn = None,  # e.g., np.mean; given value(s)
                  err_height_fcn = None,  # e.g., np.std; given value(s)
                  normalize = False,
                  total_bar_width = 0.5,
                  yscale = 'linear',
                  legend_title = None,
                  no_legend = False,
                  xlabel = None,
                  ylabel_fcn = None,
                  axis_left = 0.1,
                  axis_right = 0.8,
                  axis_bottom = 0.35,
                  axis_top = 0.97,
                  group_label_fcn = None,  # given group data key
                  subgroup_label_fcn = None,  # given subgroup data key
                  size_x = 15,  # inches
                  size_y = 5,  # inches
                  plot_ext = 'pdf',
                  group_name_map = None,  # opt dict to replace group names
                  subgroup_name_map = None,  # opt dict to replace subgroup names
                  ymin = None,
                  ymax = None,
                  grid = None
                  ):
    """Plot a bar graph with sub-bars

    data: two-dimensional array of anything, generally values or arrays.

    group_order: group names
    subgroup_order: bar names within a group
    """
    if height_fcn == None:
        height_fcn = (lambda x: x)
    if err_height_fcn == None:
        err_height_fcn = (lambda x: 0)
    if ylabel_fcn == None:
        ylabel_fcn = (lambda y_unit: str(y_unit))
    if group_label_fcn == None:
        group_label_fcn = (lambda k: k)
    if group_label_fcn == None:
        group_label_fcn = (lambda k: k)

    #  Applies a wide bar graph.
    plot_defaults.apply(axis_left = axis_left, axis_right = axis_right,
                        axis_bottom = axis_bottom, axis_top = axis_top)

    fig = plt.figure()
    fig.set_size_inches(size_x, size_y)
    ax = fig.add_subplot(111)

    N = len(group_order)
    ind = np.arange(N)  # x locations for the groups
    each_bar_width = total_bar_width / len(subgroup_order) # width of the bars
    group_x_shift = (1.0 - total_bar_width) / 2
    # With rotated text, helps to slightly shift the names over so they
    # apear to align better with the bars above.
    manual_x_shift = min(each_bar_width, total_bar_width/2)
    rects = []
    highest_total = 0
    ax.set_yscale(yscale)

    for i, subgroup in enumerate(subgroup_order):
        # Generate all the bars for one particular subgroup, for each group.
        # We use this order because it corresponds to the legend order
        heights = [height_fcn(data[group][subgroup]) for group in group_order]
        err_heights = [err_height_fcn(data[group][subgroup]) for group in group_order]
        height_plus_err = []
        for group in group_order:
            height_plus_err.append(height_fcn(data[group][subgroup]) + err_height_fcn(data[group][subgroup]))
        total = max(height_plus_err)
        # Enable per-subgroup normalization 
        if normalize:
            heights = [float(height) / total for height in heights]
            err_heights = [float(err_height) / total for err_height in err_heights]
            total = 1.0
        if total > highest_total:
            highest_total = total

        # list of err vals:
        # http://matplotlib.org/api/axes_api.html#matplotlib.axes.Axes.errorbar
        bar = ax.bar(ind + (each_bar_width * i + group_x_shift), heights, 
                     each_bar_width,
                     yerr = err_heights, 
                     ecolor='black', error_kw={'elinewidth':0.8},
                     color = COLORS[i])
        rects.append(bar)

    if normalize:
        y_unit = 'fraction'
    else:
        y_unit = 'number'

    if xlabel is not None:
        ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel_fcn(y_unit))

    ax.set_xticks(ind + total_bar_width * 0.5 + group_x_shift + manual_x_shift)
    group_display_names = []
    for g in group_order:
        if group_name_map is not None and g in group_name_map:
            group_display_names.append(group_name_map[g])
        else:
            group_display_names.append(g)
    ax.set_xticklabels(group_display_names, rotation = 45,
                       horizontalalignment = 'right' )

    subgroup_display_names = []
    for sg in subgroup_order:
        if subgroup_name_map is not None and sg in subgroup_name_map:
            subgroup_display_names.append(subgroup_name_map[sg])
        else:
            subgroup_display_names.append(sg)

    max_y = highest_total

    if not no_legend:
        # MPL: push legend off to the side.
        # from http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
        # Shrink current axis by 10%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.89, box.height])

        # Put a legend to the right of the current axis
        ax.legend([rect[0] for rect in rects], subgroup_display_names,
                  loc = 'center left',
                  title = legend_title, bbox_to_anchor = (1, 0.25))

    if ymin is not None and ymax is not None:
        ax.set_ylim([ymin, ymax])
    else:
        if ymax is not None:
            ax.set_ylim([0, ymax])
        
        if yscale == 'linear':
            temp_ymin = 0
        elif yscale == 'log':
            temp_ymin = 1
        if ymin is None:
            ymin = temp_ymin
        if ymax is None:
            ymax = max_y
        ax.set_ylim([ymin, ymax])

    # grid can be 'x', 'y', or 'both' depending on which grids we want
    if grid:
        ax.grid(True, axis=grid)

    if save_plot:
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
        filepath = os.path.join(plot_dir, analysis_name + '.' + plot_ext)
        print "Writing file to %s" % filepath
        fig.savefig(filepath, dpi = DPI)
    if show_plot:
        plt.show()

    plt.close(fig)

