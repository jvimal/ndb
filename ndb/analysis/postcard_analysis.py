"""
Inputs (eventually, partially implemented):
- Topology, covering hosts and nodes.
- Traffic matrix: src/dst host pairs with:
    - Flow size distribution: packet headers
    - Packet size distribution
    - Rates
- Routing: shortest path only
- queries: PPFs defined for the topology
- Filtering methods: what we're trying to compare

Outputs:
- Postcard totals: per query and per filtering method
"""

import networkx as nx

from analysis_lib import autodict
from ndb.filter import PacketPathFilter
from filters import DEBUG_FILTERS
from ndb.collector import get_tag
from ndb.analysis.postcards import postcards_on_path, NO_INGRESS_INPORT

def print_postcard_totals(all_postcard_totals, queries):

    # Check that every known total matched.
    # Print out postcard totals.
    print "%%%%%%%%%%%%%%%%%%%%%%%"
    print "--> Postcard totals:"
    for query_name, query_strings, expected in queries:
        print "%s: (%s)" % (query_name, " | ".join(query_strings))
        for filter in all_postcard_totals[query_name]:
            print "\t%s: " % filter,
            totals = all_postcard_totals[query_name][filter]
            # If there is only one total in the data structure, assume
            # that we may want to check the one value there.  Otherwise,
            # assume that the test was meant to return a distribution.
            if filter in expected and len(totals) == 1:
                actual = totals[0]
                if expected[filter] == actual:
                    print "%i OK" % actual
                else:
                    print "ERROR: saw %i, expected %s" % (actual, expected[filter])
            else:
                # Print 'em all
                print "%s" % all_postcard_totals[query_name][filter]


def run_scenario(t, tm, queries, routing, filters, total, sw_to_dpid):
    """Function to run a complete scenario and return the postcard_totals data.

    Use this as in inner-loop call to wrap around different tms & query types.
    """
    if routing != 'shortest-path':
        raise NotImplementedException('Shortest paths only.')
    shortest_paths = nx.all_pairs_shortest_path(t.g)
    
    # Generate list of PPF objects
    # TODO: optimize this where these don't change (e.g. one kind of queries)
    ppf_lists = {}
    for query_name, query_strings, expected in queries:
        ppf_lists[query_name] = [PacketPathFilter(qs) for qs in query_strings]

    # Set maximum possible pcards for each.
    # TODO: optimize this where these don't change (e.g. one kind of queries)
    for query_name, query_strings, expected in queries:
        expected['null'] = total

    # Postcards tally sheet, per query and filter algorithm:
    # postcard_totals[query][filter] = int (counter for # postcards)
    postcard_totals = init_postcard_totals(int, queries, filters)

    # Run the main "simulation".
    # For each flow:
    # - Figure out how that flow would be routed.
    # - Generate the postcard sequence to see if it actually matches each query.
    # - Tally the postcards from each method.
    # TODO: reconsider whether this is a reasonable or efficient way to order
    # the computation.
    for src in sorted(tm):
        for dst in sorted(tm[src]):
            for flow in tm[src][dst]:
                #print "Considering path from %s to %s" % (src, dst)

                # flow looks like: IPFlow(traffic_fraction, src_ip, dst_ip)
                rate = flow.rate
                pkt = flow.pkt
                # TODO: generalize to hosts with multiple neighbors
                node_path = shortest_paths[src][dst]

                path = postcards_on_path(t, node_path, NO_INGRESS_INPORT,
                                         pkt, sw_to_dpid)
                
                #print "Generated postcard path of len: %s" % len(path)
                #for postcard in path:
                #    print "%s" % postcard

                for query_name, query_strings, expected in queries:
                    ppfs = ppf_lists[query_name]
                    # TODO: cache objects used by multiple queries
                    #print "%s: %s" % (query_name, query_string)
                    if DEBUG_FILTERS:
                        print "ppf_strs: %s" % " | ".join(query_strings)
                        for pcard in path:
                            print "\t%s" % pcard
                    for filter_name, filter_fcn in filters.iteritems():
                        added = filter_fcn(ppfs, query_strings, path)
                        if DEBUG_FILTERS:
                            print "\t%s : %s" % (filter_name, added)
                        postcard_totals[query_name][filter_name] += added * rate

    return postcard_totals


def append_postcard_totals(all_postcard_totals, postcard_totals, queries, filters):
    # Append each total to the list of all postcard totals:
    for query_name, query_strings, expected in queries:
        for filter in filters:
            this_postcard_total = postcard_totals[query_name][filter]
            all_postcard_totals[query_name][filter].append(this_postcard_total)


def init_postcard_totals(constructor, queries, filters):
    # Postcards tally sheet, per query and filter algorithm:
    # postcard_totals[query][filter] = [list of totals]
    all_postcard_totals = autodict()
    for query_name, query_strings, expected in queries:
        for filter in filters:
            all_postcard_totals[query_name][filter] = constructor()
    return all_postcard_totals


def PostcardAnalysis(t, tms, queries_lists, routing, filters, total, sw_to_dpid):
    """Run a 'simulation' to measure postcards for different algorithms.

    Inputs (eventually, partially implemented):
    - Topology, covering hosts and nodes.
    - Traffic matrices: each has src/dst host pairs with:
        - Flows: a list of Flow objects, each of which as:
            - Traffic amount, unitless
            - Packet examples, as dpkt.Packet
        - TODO: Flow size distribution: packet headers
        - TODO: Packet size distribution
    - Queries: each has a list of PPFs w/expected postcards to be generated with:
        - string name
        - string to generate PacketPathFilter
        - dict of expected postcards per filtering method:
            - key: filter name as string
            - value: expected postcards as int
    - Routing: string, shortest path only
    - Filtering methods: what we're trying to compare; same values as used
        in Queries
    - Total: 
        - max number of possible postcards from this traffic matrix.

    - (temp) sw_to_dpid: map from switch string IDs to dpid uints.
    TODO: put this in the topology directly.

    ASSUMPTION: all queries objects in queries_lists have the same filter
    and query fields.

    Outputs:
    - All postcard totals: autodict w/two dimensions:
       all_postcard_totals[query_name][filter_name] = [list of integer postcard total, one per tm]
    """
    # all_postcard_totals[query][filter] = list (counters for #'s of postcards)
    all_postcard_totals = init_postcard_totals(list, queries_lists[0], filters)

    # If a list of queries was passed in:
    if len(queries_lists) > 1 and len(tms) == 1:
        for queries in queries_lists:
            tm = tms[0]
            postcard_totals = run_scenario(t, tm, queries, routing, filters, total, sw_to_dpid)
            append_postcard_totals(all_postcard_totals, postcard_totals, queries, filters)

    # If a list of traffic matrices was passed in:
    elif len(tms) > 1:
        for tm in tms:
            queries = queries_lists[0]
            postcard_totals = run_scenario(t, tm, queries, routing, filters, total, sw_to_dpid)
            append_postcard_totals(all_postcard_totals, postcard_totals, queries, filters)

    # Original case: run once.
    else:
        tm = tms[0]
        queries = queries_lists[0]
        postcard_totals = run_scenario(t, tm, queries, routing, filters, total, sw_to_dpid)
        append_postcard_totals(all_postcard_totals, postcard_totals, queries, filters)

    print_postcard_totals(all_postcard_totals, queries)

    return all_postcard_totals

