#!/usr/bin/env python
'''Simple test to print out a .flows pickle file.'''
import pickle
import sys
if len(sys.argv) == 1:
    filename = 'http.cap.flows'
elif len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    raise Exception("too many args")

print "filename:", filename
f = open(filename, 'r')
contents = pickle.load(f)
print contents
