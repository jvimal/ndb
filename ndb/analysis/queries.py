"""
Defined queries and functions to process them.
"""
from analysis_lib import ip_dd

# Global ordering across all query types for use in plot output.
DEF_QUERY_ORDER = [
    'any', 'any-2', 'srcip', 'dstip', 'srcip_dstip',
    'srcsw', 'dstsw', 'srcsw_dstsw',
    'srcip+srcsw', 'dstip+dstsw', 'srcip+srcsw_dstip+dstsw',
    'thru_core', 'core_link_dir', 'core_link_undir',
    'core_link_ports_dir', 'core_link_ports_undir',
    'loop',
    'edge_not_otheredge', 'edge_not_otheredge-$',
    'path_len >= 1', 'path_len >= max', 'pathlen >= max+1',
    'none'
]

TREE_PAPER_QUERY_ORDER = [
    #'any',
    'loop',
    'path_len >= 1',
    'path_len >= max',
    'srcip', 'dstip', 'srcip_dstip',
    'srcsw', 'dstsw', 'srcsw_dstsw',
    'srcip+srcsw', 'dstip+dstsw', 'srcip+srcsw_dstip+dstsw',
    'thru_core', 'core_link_dir', 'core_link_ports_dir',
    'core_link_undir',  'core_link_ports_undir',
    #'edge_not_otheredge', 
    'edge_not_otheredge-$',
    #'none'
]

QUERY_DISPLAY_NAMES = {
    'srcip': 'src ip',
    'dstip': 'dst ip',
    'srcsw': 'src location',
    'dstsw': 'dst location',
    'srcip_srcsw': 'src ip+location',
    'dstip_dstsw': 'dst ip+location',
    'srcip_dstip': 'src to dst ip',
    'srcip+srcsw': 'src ip+location',
    'dstip+dstsw': 'dst ip+location',
    'srcip+srcsw_dstip+dstsw': 'src to dst\nip+location',
    'srcsw_dstsw': 'src to dst\nlocation',
    'thru_core': 'through core',
    'core_link_dir': 'core link\none way',
    'core_link_undir': 'core link\nboth ways',
    'core_link_ports_dir': 'core link ports\none way',
    'core_link_ports_undir': 'core link ports\nboth ways',
    'edge_not_otheredge-$': 'edge then\nnot otheredge',
}


def process_queries(queries):
    """Convenience function: enables single-string declarations for PPFs.

    If not a string (like a set or list), leave alone.

    Modifies query string in-place.
    """
    for i, query_element in enumerate(queries):
        query_name, query_string, expected = query_element
        if type(query_string) is str:
            queries[i] = [query_name, [query_string], expected]


def all_to_all_queries(t, path_lengths, sw_to_dpid, host_to_ip):
    """Return totals + queries list for use in PostcardAnalysis.

    t: Topo object
    path_lengths: path lengths for all flows leaving one host
    sw_to_dpid: mapping from switch names to dpid uints
    host_to_ip: mapping from host names to ip uints
    
    returns:
    total: total number of postcards expected to be generated
    queries: list, query data
    """
    leftmost_host = t.hosts()[0]
    rightmost_host = t.hosts()[-1]
    leftmost_node_ip = ip_dd(host_to_ip[leftmost_host])
    rightmost_node_ip = ip_dd(host_to_ip[rightmost_host])
    leftmost_dpid = sw_to_dpid[t.g.neighbors(leftmost_host)[0]]
    rightmost_dpid = sw_to_dpid[t.g.neighbors(rightmost_host)[0]]
    #print "leftmost dpid: %s" % leftmost_dpid
    #print "rightmost dpid: %s" % rightmost_dpid

    total = len(t.hosts()) * sum(path_lengths)
    hosts_per_edge_sw = 2

    # Each host sends to all hosts but itself
    ingress_leftmost_sw = (len(t.hosts()) - 1) * hosts_per_edge_sw
    # Hosts outside this switch send one flow through to each host on this
    # edge switch.
    egress_leftmost_sw = (len(t.hosts()) - hosts_per_edge_sw) * hosts_per_edge_sw
    # Traffic through leftmost switch generates postcards;
    # Add ingress postcards only; ignore egress.
    opt_single_total = hosts_per_edge_sw * sum(path_lengths)
    # Add postcards from the leftmost switch +
    #  to the leftmost switch - 
    #  intra-leftmost-switch flows.
    # The egress notifies all generate postcards, but then these don't match
    # the RE check, hence the diversion from optimal.
    ipred_single_total = (hosts_per_edge_sw * sum(path_lengths) + 
                         sum(path_lengths) * hosts_per_edge_sw - 
                         hosts_per_edge_sw * (hosts_per_edge_sw - 1))

    # Each leftmost hosts send to {hosts_per_edge_sw} hosts on rightmost sw.
    opt_both_total = hosts_per_edge_sw * hosts_per_edge_sw * max(path_lengths)
    # Everything except the middle root switch should match
    # For four-node case:
    # Of 12 flows:
    # 4 are local, and generate single notifies w/no follow-up.
    #    Do not count these; consider notifies and postcards separately.
    # 8 are remote, and each generates at least two notifies (missing 4 at core).  
    #    Of these, 4 match the PPF, and each generate 3 postcards.
    #ipred_both_total = int(len(t.hosts()) * (hosts_per_edge_sw - 1) + # local
    #                  (len(t.hosts()) * (len(t.hosts()) - hosts_per_edge_sw) * 0.5 * 2 +
    #                   len(t.hosts()) * (len(t.hosts()) - hosts_per_edge_sw) * 0.5 * 3))
    # Actually, the above would be true, IF the ipred algorithm was smart
    # enough to notice that the first match. which specified the dpid,
    # had the wrong dpid.  This optimization requires using knowledge of the
    # topology to distinguish between the edge and core.
    # Right now: either a path is through the core, in which case both ipreds
    # will match and the full path will get built before filtering it out, OR
    # the path is length-one and since the pcard matches a pred, no difference
    # from null filtering.
    # That's another optimization too...
    # TODO: implement these optimizations and make the above calculations
    # useful!
    ipred_both_total = opt_both_total * 2  # Matching, both directions

    queries = [
        ['none', '$', {
            'opt': 0,
            'ipred_notifies': total,
            'ipred_pcards': total
            }
         ],
        ['any', '.*', {
            'opt': total,
            'ipred_notifies': total,
            'ipred_pcards': total
            }
         ],
        ['any-2', '', {
            'opt': total,
            'ipred_notifies': total,
            'ipred_pcards': total
            }
         ],
        ['srcip', '{{--bpf "src %s"}}.*' % leftmost_node_ip, {
            'opt': sum(path_lengths),
            'ipred_notifies': sum(path_lengths),
            'ipred_pcards': sum(path_lengths)
            }
         ],
        ['dstip', '.*{{--bpf "dst %s"}}$' % rightmost_node_ip, {
            'opt': sum(path_lengths),
            'ipred_notifies': sum(path_lengths),
            'ipred_pcards': sum(path_lengths)
            }
         ],
        ['srcip_dstip', '{{--bpf "src %s and dst %s"}}' % (leftmost_node_ip, rightmost_node_ip), {
            'opt': path_lengths[-1],
            'ipred_notifies': path_lengths[-1],
            'ipred_pcards': path_lengths[-1]
            }
         ],
        ['srcsw', '{{--dpid %s}}.*' % leftmost_dpid, {
            'opt': opt_single_total,
            # Traffic through leftmost switch generates notifies:
            'ipred_notifies': ingress_leftmost_sw + egress_leftmost_sw,
            'ipred_pcards': ipred_single_total,
            }
         ],
        # All the same totals as above (symmetric).
        ['dstsw', '.*{{--dpid %s}}$' % rightmost_dpid, {
            'opt': opt_single_total,
            'ipred_notifies': ingress_leftmost_sw + egress_leftmost_sw,
            'ipred_pcards': ipred_single_total,
            }
         ],
        ['srcsw_dstsw', '{{--dpid %s}}.*{{--dpid %s}}$' % (leftmost_dpid, rightmost_dpid), {
            'opt': opt_both_total,
            # Traffic through leftmost switch generates notifies;
            # total here is twice that for dstsw (additive combo to central
            # collector.
            'ipred_notifies': (ingress_leftmost_sw + egress_leftmost_sw) * 2,
            'ipred_pcards': ipred_both_total,
            }
         ],
        # This query is meant to test that the combination of two dash
        # qualifiers on the same imperative predicate actually works.
        ['srcip+srcsw', '{{--bpf "src %s" --dpid %s}}.*' % (leftmost_node_ip, leftmost_dpid), {
            'opt': sum(path_lengths),
            # Simply the number of outgoing flows from the leftmost IP.
            'ipred_notifies': (len(t.hosts()) - 1),
            'ipred_pcards': min(sum(path_lengths), ipred_single_total),
            }
         ],
        ['dstip+dstsw', '.*{{--bpf "dst %s" --dpid %s}}$' % (rightmost_node_ip, rightmost_dpid), {
            'opt': sum(path_lengths),
            'ipred_notifies': (len(t.hosts()) - 1),
            'ipred_pcards': min(sum(path_lengths), ipred_single_total),
            }
         ],
        ['srcip+srcsw_dstip+dstsw',
         '{{--bpf "src %s" --dpid %s}}.*{{--bpf "dst %s" --dpid %s}}$' %
         (leftmost_node_ip, leftmost_dpid, rightmost_node_ip, rightmost_dpid), {
            # Only one flow should match this highly specific query.
            'opt': max(path_lengths),
            # Notifies: num of flows per host, twice over.
            'ipred_notifies':  (len(t.hosts()) - 1) * 2,
            # One flow matches, given the ip-level specificity.
            'ipred_pcards': path_lengths[-1],
            }
         ],
    ]
    process_queries(queries)
    return total, queries


def all_to_all_tree_queries(t, path_lengths, sw_to_dpid, host_to_ip):
    """Return totals + queries list for use in PostcardAnalysis.

    Specific to trees.

    t: Topo object
    path_lengths: path lengths for all flows leaving one host
    sw_to_dpid: mapping from switch names to dpid uints
    host_to_ip: mapping from host names to ip uints
    
    returns:
    total: total number of postcards expected to be generated
    queries: list, query data
    """
    # TODO: pass this as a param
    root = 's1'
    root_dpid = 1
    # Any root-neighboring sw will work with symmetric TM.
    next = t.g.neighbors(root)[0]
    next_dpid = sw_to_dpid[next]
    root_outport, next_inport = t.port(root, next)
    next_outport, root_inport = t.port(next, root)

    hosts_per_edge_sw = 2
    hosts = len(t.hosts())
    flows = hosts * (hosts - 1)
    # 2x for bidirectional
    flows_thru_root = hosts_per_edge_sw * hosts_per_edge_sw * 2

    remote_flows = (hosts_per_edge_sw * hosts_per_edge_sw) * 2
    local_flows_one_sw = hosts_per_edge_sw * (hosts_per_edge_sw - 1)
    remote_flows_out_one_sw = hosts_per_edge_sw * (hosts - hosts_per_edge_sw)

    total = len(t.hosts()) * sum(path_lengths)

    leftmost_host = t.hosts()[0]
    rightmost_host = t.hosts()[-1]
    leftmost_dpid = sw_to_dpid[t.g.neighbors(leftmost_host)[0]]
    rightmost_dpid = sw_to_dpid[t.g.neighbors(rightmost_host)[0]]

    _opt = local_flows_one_sw + (remote_flows * max(path_lengths) / 2)
    flows_out_per_host = hosts - 1
    flows_out_per_edge_sw = flows_out_per_host * hosts_per_edge_sw

    flows_per_host = hosts - 1

    queries = [
        ['thru_core', '.*{{--dpid %s}}.*' % root_dpid, {
            'opt': flows_thru_root * max(path_lengths),
            # One notify per flow.
            'ipred_notifies': flows_thru_root,
            # No wasted postcards.
            'ipred_pcards': flows_thru_root * max(path_lengths),
            }
         ],
        ['core_link_dir', '.*{{--dpid %s}}{{--dpid %s}}.*' % (root_dpid, next_dpid), {
            # Only flows in one direction will match.
            'opt': flows_thru_root * max(path_lengths) / 2.0,
            # TODO: make this more generic to handle arbitary-depth trees.
            # Two notifies per remote flow.
            'ipred_notifies': remote_flows * 2 + local_flows_one_sw,
            # Add pcards for all remote flows, only half of which will pass.
            'ipred_pcards': remote_flows * max(path_lengths),
            }
         ],
        ['core_link_undir',
         '.*{{--dpid %s}}{{--dpid %s}}.*|.*{{--dpid %s}}{{--dpid %s}}.*.*' %
         (root_dpid, next_dpid, next_dpid, root_dpid), {
            # Flows in both directions will match.
            'opt': flows_thru_root * max(path_lengths),
            # TODO: make this more generic to handle arbitary-depth trees.
            'ipred_notifies': remote_flows * 2 + local_flows_one_sw,
            # Add pcards for all remote flows.
            'ipred_pcards': remote_flows * max(path_lengths),
            }
         ],
        ['core_link_ports_dir',
         '.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' %
         (root_dpid, root_outport, next_dpid, next_inport), {
            # Only flows in one direction will match.
            'opt': flows_thru_root * max(path_lengths) / 2.0,
            # TODO: make this more generic to handle arbitary-depth trees.
            # Half the remote flows, but 2 ipreds per remote flow.
            'ipred_notifies': (remote_flows / 2.0) * 2,
            # Should be optimal... collector will only ask for more for paths
            # that will match.
            'ipred_pcards': flows_thru_root * max(path_lengths) / 2.0,
            }
         ],
        ['core_link_ports_undir',
         ['.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' % (root_dpid, root_outport, next_dpid, next_inport),
          '.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' % (next_dpid, next_outport, root_dpid, root_inport)
          ],
         {
            # Flows in both directions will match.
            'opt': flows_thru_root * max(path_lengths),
            # TODO: make this more generic to handle arbitary-depth trees.
            # All the remote flows, 2 ipreds per remote flow.
            'ipred_notifies': remote_flows * 2,
            # Should be optimal... collector will only ask for more for paths
            # that will match.
            'ipred_pcards': remote_flows * max(path_lengths),
            }
         ],
        ['loop', '.*(.).*(\1).*',
         {
            'opt': 0,
            'ipred_notifies': total,
            'ipred_pcards': total,
          }
         ],
        # NOTE!!!
        # NOT THE SAME AS THE ONE BELOW
        # Remember, no dollar looks for a substring match.
        # The substring will match and return on paths of length two.
        # Need to add dollar to ensure that the _whole_ path is used.
        ['edge_not_otheredge',
         '{{--dpid %s}}[^{{--dpid %s}}]*' % (leftmost_dpid, rightmost_dpid),
         {
            'opt': _opt,
            # Flows out from this edge sw minus overlap (1), time 2 for bidir.
            'ipred_notifies': (flows_per_host * hosts_per_edge_sw - 1) * 2,
            # Only paths staying entirely within the ignored switch should
            # not generate postcards.
            'ipred_pcards': total - local_flows_one_sw,
          }
         ],
        ['edge_not_otheredge-$',
         '{{--dpid %s}}[^{{--dpid %s}}]*$' % (leftmost_dpid, rightmost_dpid),
         {
            'opt': local_flows_one_sw,
            # Flows out from this edge sw minus overlap (1), time 2 for bidir.
            'ipred_notifies': (flows_per_host * hosts_per_edge_sw - 1) * 2,
            # Only paths staying entirely within the ignored switch should
            # not generate postcards.
            'ipred_pcards': total - local_flows_one_sw,
          }
         ],
        ['path_len >= 1', '.{%s}.*' % 1,
         {
            # TODO: generalize this; assumes two edge switches only.
            'opt': total,
            'ipred_notifies': total,
            'ipred_pcards': total,
          }
         ],
        ['path_len >= max', '.{%s}.*' % max(path_lengths),
         {
            # TODO: generalize this; assumes two edge switches only.
            'opt': total - local_flows_one_sw * 2,
            'ipred_notifies': total,
            'ipred_pcards': total,
          }
         ],
        ['pathlen >= max+1', '.{%s}.*' % (max(path_lengths) + 1),
         {
            'opt': 0,
            'ipred_notifies': total,
            'ipred_pcards': total,
          }
         ],
    ]
    process_queries(queries)
    total = len(t.hosts()) * sum(path_lengths)

    return total, queries


def random_tree_queries(t, path_lengths, sw_to_dpid, host_to_ip):
    """Return totals + queries list for use in PostcardAnalysis.

    t: Topo object
    path_lengths: path lengths for all flows leaving one host
    sw_to_dpid: mapping from switch names to dpid uints
    host_to_ip: mapping from host names to ip uints
    
    returns:
    total: total number of postcards expected to be generated
    queries: list, query data
    """
    leftmost_host = t.hosts()[0]
    rightmost_host = t.hosts()[-1]
    leftmost_node_ip = ip_dd(host_to_ip[leftmost_host])
    rightmost_node_ip = ip_dd(host_to_ip[rightmost_host])
    leftmost_dpid = sw_to_dpid[t.g.neighbors(leftmost_host)[0]]
    rightmost_dpid = sw_to_dpid[t.g.neighbors(rightmost_host)[0]]

    # TODO: pass this as a param
    root = 's1'
    root_dpid = 1
    # Any root-neighboring sw will work with symmetric TM.
    next = t.g.neighbors(root)[0]
    next_dpid = sw_to_dpid[next]
    root_outport, next_inport = t.port(root, next)
    next_outport, root_inport = t.port(next, root)

    hosts_per_edge_sw = 2
    hosts = len(t.hosts())
    flows = hosts * (hosts - 1)
    # 2x for bidirectional
    flows_thru_root = hosts_per_edge_sw * hosts_per_edge_sw * 2

    remote_flows = (hosts_per_edge_sw * hosts_per_edge_sw) * 2
    local_flows_one_sw = hosts_per_edge_sw * (hosts_per_edge_sw - 1)
    remote_flows_out_one_sw = hosts_per_edge_sw * (hosts - hosts_per_edge_sw)

    total = len(t.hosts()) * sum(path_lengths)

    leftmost_host = t.hosts()[0]
    rightmost_host = t.hosts()[-1]
    leftmost_dpid = sw_to_dpid[t.g.neighbors(leftmost_host)[0]]
    rightmost_dpid = sw_to_dpid[t.g.neighbors(rightmost_host)[0]]

    _opt = local_flows_one_sw + (remote_flows * max(path_lengths) / 2)
    flows_out_per_host = hosts - 1
    flows_out_per_edge_sw = flows_out_per_host * hosts_per_edge_sw

    flows_per_host = hosts - 1

    # Assume worst-case path lengths, because without actually doing the
    # simulation, we won't know these.
    total = max(path_lengths) * len(t.hosts())

    queries = [
        ['none', '$', {}],
        ['any', '.*', {}],
        ['any-2', '', {}],
        ['srcip', '{{--bpf "src %s"}}.*' % leftmost_node_ip, {}],
        ['dstip', '.*{{--bpf "dst %s"}}$' % rightmost_node_ip, {}],
        ['srcip_dstip', '{{--bpf "src %s and dst %s"}}' % (leftmost_node_ip, rightmost_node_ip), {}],
        ['srcsw', '{{--dpid %s}}.*' % leftmost_dpid, {}],
        ['dstsw', '.*{{--dpid %s}}$' % rightmost_dpid, {}],
        ['srcsw_dstsw', '{{--dpid %s}}.*{{--dpid %s}}$' % (leftmost_dpid, rightmost_dpid), {}],
        ['srcip+srcsw', '{{--bpf "src %s" --dpid %s}}.*' % (leftmost_node_ip, leftmost_dpid), {}],
        ['dstip+dstsw', '.*{{--bpf "dst %s" --dpid %s}}$' % (rightmost_node_ip, rightmost_dpid), {}],
        ['srcip+srcsw_dstip+dstsw',
         '{{--bpf "src %s" --dpid %s}}.*{{--bpf "dst %s" --dpid %s}}$' %
         (leftmost_node_ip, leftmost_dpid, rightmost_node_ip, rightmost_dpid), {}],

        ['thru_core', '.*{{--dpid %s}}.*' % root_dpid, {}],
        ['core_link_dir', '.*{{--dpid %s}}{{--dpid %s}}.*' % (root_dpid, next_dpid), {}],
        ['core_link_undir',
         '.*{{--dpid %s}}{{--dpid %s}}.*|.*{{--dpid %s}}{{--dpid %s}}.*.*' %
         (root_dpid, next_dpid, next_dpid, root_dpid), {}],
        ['core_link_ports_dir',
         '.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' %
         (root_dpid, root_outport, next_dpid, next_inport), {}],
        ['core_link_ports_undir',
         ['.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' % (root_dpid, root_outport, next_dpid, next_inport),
          '.*{{--dpid %s --outport %s}}{{--dpid %s --inport %s}}.*' % (next_dpid, next_outport, root_dpid, root_inport)
          ],
         {}],
        ['loop', '.*(.).*(\1).*',{}],
        ['edge_not_otheredge',
         '{{--dpid %s}}[^{{--dpid %s}}]*' % (leftmost_dpid, rightmost_dpid),
         {}],
        ['edge_not_otheredge-$',
         '{{--dpid %s}}[^{{--dpid %s}}]*$' % (leftmost_dpid, rightmost_dpid),
         {}],
        ['path_len >= 1', '.{%s}.*' % 1, {}],
        ['path_len >= max', '.{%s}.*' % max(path_lengths), {}],
        ['pathlen >= max+1', '.{%s}.*' % (max(path_lengths) + 1), {}],
    ]
    process_queries(queries)
    return total, queries
