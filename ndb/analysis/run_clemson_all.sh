#!/bin/sh
/usr/bin/time ./pcap_flowsplit.py pcaps/clemson/core-capture.cap.100K.pcap
/usr/bin/time ./pcap_flowsplit.py pcaps/clemson/core-capture.cap.1M.pcap
/usr/bin/time ./pcap_flowsplit.py pcaps/clemson/core-capture.cap.10M.pcap
/usr/bin/time ./pcap_flowsplit.py pcaps/clemson/core-capture.cap.100M.pcap
