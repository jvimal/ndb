#!/usr/bin/python

import sys
sys.path.append('../')

import argparse
import os
from subprocess import Popen, PIPE
import tempfile
import time
import dpkt
import struct

from master import Master
from collector import pkt_hash

ETH_TYPE_PCARD = 0xABCD

args = None
path_db = {}

def parse_args():
    parser = argparse.ArgumentParser(description="netdump: ' \
            tcpdump anywhere in the network")

    parser.add_argument('--wireshark',
                        dest="wireshark",
                        help="Path to wireshark",
                        default=None)

    parser.add_argument('--test',
                        dest="test",
                        action="store_true",
                        help="Test netdump using synthetic data",
                        default=False)

    args = parser.parse_args()
    return args

class Postcard(dpkt.Packet):
    '''A custom shim layer for postcards'''
    __hdr__ = (
            ('pkt_id', '16s', '\x00'*16),
            ('path_id', 'Q', 0xffffffffffffffff),
            ('dpid', 'Q', 0xffffffffffffffff),
            ('inport', 'H', 0xffff),
            ('outport', 'H', 0xffff),
            ('version', 'H', 0xffff),
            ('length', 'H', 0x0)
            )

    def __len__(self):
        return self.__hdr_len__ + len(self.data)

def create_pcard_pkt(pcard, path_id=0xffffffffffffffff):
    '''Create a new ethernet packet of type ETH_TYPE_PCARD 
    by encapsulating the postcard'''
    pc = Postcard(
            pkt_id=pkt_hash(pcard['pkt']),
            path_id=path_id,
            dpid=pcard['dpid'], 
            inport=pcard['inport'],
            outport=pcard['outport'],
            version=pcard['version'],
            length=len(pcard['pkt'])
            )
    pc.data = pcard['pkt'].pack()

    pcard_pkt = dpkt.ethernet.Ethernet(type=ETH_TYPE_PCARD)
    pcard_pkt.data = pc.pack()

    return pcard_pkt

def get_path(pp):
    '''Get the path as a tuple of dpids'''
    return tuple(pcard['dpid'] for pcard in pp)

def handle_pp(pp, fifo_filename):
    global path_db

    path = get_path(pp)
    if path not in path_db:
        path_db[path] = len(path_db) + 1

    fifo = open(fifo_filename, 'w')
    pcapWriter = dpkt.pcap.Writer(fifo) 

    for pcard in pp:
        pcard_pkt = create_pcard_pkt(pcard, path_id=path_db[path])
        pcapWriter.writepkt(pcard_pkt.pack())

    pcapWriter.close()

def create_pp(pcap_fname):
    pcapReader = dpkt.pcap.Reader(file(pcap_fname))
    pp = []
    for ts, data in pcapReader:
        pkt = dpkt.ethernet.Ethernet(data)
        metadata = pkt.dst
        metadata = '\x00' + metadata
        version, port, dpid = struct.unpack('>IHB', metadata)
        pp.append({
            'pkt':pkt,
            'dpid': dpid,
            'inport': 0xffff,
            'outport': port,
            'version':version
            })
    return pp

def test(fifo_filename):
    pcard_pcap = '../test/icmp_request_chain.pcap'
    pp = create_pp(pcard_pcap)
    handle_pp(pp, fifo_filename)
    while True:
        time.sleep(1)

if __name__ == "__main__":
    global args

    args = parse_args()

    display = os.getenv('DISPLAY')
    if display is None:
        print 'Error! No display to show the wireshark GUI.'
        sys.exit()

    wireshark = args.wireshark
    if wireshark is None:
        wireshark = Popen('which wireshark', shell=True,
                stdout=PIPE).communicate()[0].strip()

    if not os.path.isfile(wireshark):
        print 'Error! No wireshark.'
        sys.exit()

    tmpdir = tempfile.mkdtemp()
    fifo_filename = os.path.join(tmpdir, 'myfifo')
    print 'Starting a temporary fifo:', fifo_filename
    try:
        os.mkfifo(fifo_filename)
    except OSError, e:
        print "Failed to create FIFO: %s" % e
        sys.exit()

    os.system('DISPLAY=%s %s -k -i %s &' % (os.getenv('DISPLAY'),
        wireshark, fifo_filename))

    raw_input('Hit ENTER when wireshark is up and running:')

    if args.test:
        try:
            test(fifo_filename)
        except KeyboardInterrupt:
            os.system('pkill wireshark')
            os.remove(fifo_filename)
            os.rmdir(tmpdir)
            print 'Exiting. Bye!'
            sys.exit()

    # start the master collector
    c = Master()
    c.start()

    print 'Started the Master collector.'
    try:
        raw_input('Start all the Slave collectors and hit Enter:')
        while True:
            bkpt = raw_input('Enter breakpoint in the form of a PPF\n>>> ')
            cb = lambda pp: handle_pp(pp, fifo_filename)
            c.add_filter(bkpt, cb)
            time.sleep(1) 
    except KeyboardInterrupt:
        c.stop()
        os.system('killall wireshark')
        os.remove(fifo_filename)
        os.rmdir(tmpdir)
        print 'Exiting. Bye!'

