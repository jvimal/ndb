#!/usr/bin/python

import sys
sys.path.append('../')

from threading import Timer, Thread
from master import Master, print_packetpath

refresh_interval = 1.0 # refresh stats every 1 sec

class StatTree(object):

    class StatNode(object):
        def __init__(self, dpid, val=0):
            self.dpid = dpid
            self.val = val
            self.children = []

        def __del__(self):
            del self.children

        def add_child(dpid, val=0):
            c = StatNode(dpid, val)
            self.children.append(c)
            return c

        def get_child(dpid):
            for c in self.children:
                if c.dpid == dpid:
                    return c
            return None

        def __repr__(self):
            s = '%s (%s)' % (self.dpid, self.val)
            for c in self.children:
                s += '\n\\____%s' % repr(c)
            return s

    def __init__(self, root):
        self.root = StatNode(root)

    def add_path(self, dpid_lst, val):
        assert len(dpid_lst) >= 1
        assert dpid_lst[0] == self.root.dpid

        parent = self.root
        for dpid in dpid_lst[1:]:
            c = parent.get_child(dpid)
            if c is not None:
                c.val += val
            else:
                c = parent.add_child(dpid, val)
            parent = c

    def __repr__(self):
        return repr(self.root)


root = None
stats_tree = None

def handle_packetpath(pp):
    dpid_lst = []
    for pcard in pp:
        dpid = pcard['dpid']
        dpid_lst.insert(0, dpid)
        if dpid == root:
            break
    return dpid_lst

def refresh_stats():
    '''Refresh statistics and print them on screen'''
    global t

    assert root != None
    print repr(stats_tree)
    print '------------------------------'
    stats_tree = StatTree(root)
    t = Timer(refresh_interval, refresh_stats)
    t.start()

def add_root(dpid, outport):
    global c
    global root

    pf = '{{--dpid %s --outport %s}}' % (dpid, outport)
    c.add_filter('.*%s.*' % pf, handle_packetpath)
    root = dpid

if __name__ == "__main__":
    global c
    global t

    c = Master()
    c.start()

    print 'Started the Master collector.'
    try:
        raw_input('Start all the Slave collectors and hit Enter:')
        dpid = raw_input('Enter root dpid: ')
        outport = raw_input('Enter root outport: ')
        add_root(int(dpid), int(outport))
        refresh_stats()
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        c.stop()
        if t is not None:
            t.cancel()
        print 'Exiting. Bye!'
        sys.exit()
