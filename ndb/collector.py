import logging as L
import dpkt
import hashlib
import struct
import pcapy
import json
import unittest
from scapy.all import *
from db_handler import MongoHandler
from topology import NDBTopo
from openfaucet import ofmatch, buffer

def pkt_equal(a, b):
    # Compares two packets and guesses if they are the same.  If we
    # have packet ids, this is a simple step.

    # Packets a, b are ethernet packets.  If they are not IP, then
    # we just compare ethernet data.

    # VLANs are stripped before type field is populated, so we're
    # good.  VLAN tag is accessible via .tag attribute
    return pkt_hash(a) == pkt_hash(b)

def pkt_hash(a):
    def hash(pkt):
        return hashlib.md5(str(pkt)).hexdigest()

    if a.type != dpkt.ethernet.ETH_TYPE_IP:
        return hash(a.data)

    ip = a.data
    if ip.p != dpkt.ip.IP_PROTO_TCP and ip.p != dpkt.ip.IP_PROTO_UDP:
        return hash(ip.data)

    proto = ip.data
    return hash(proto.data)

def get_tag(pkt):
    data = pkt.dst
    data = '\x00' + data
    version, port, dpid = struct.unpack('>IHB', data)
    return version, port, dpid

def describe_tag(t):
    return "ver=%s, oport=%s, sw=%s" % t

def sniff_pkts(intf, cb):

    if intf is None:
        return

    # pcap settings
    DEV          = intf  # interface to listen on
    MAX_LEN      = 1514    # max size of packet to capture
    PROMISCUOUS  = True    # promiscuous mode?
    READ_TIMEOUT = 100     # in milliseconds
    PCAP_FILTER  = ''      # empty => get everything (or we could use a BPF filter)
    MAX_PKTS     = -1      # number of packets to capture; -1 => no limit

    p = pcapy.open_live(DEV, MAX_LEN, PROMISCUOUS, READ_TIMEOUT)
    p.setfilter(PCAP_FILTER)
    try:
        p.loop(MAX_PKTS, cb)
    except KeyboardInterrupt:
        print '%d packets received, %d packets dropped, %d packets dropped by interface' % p.stats()


def extract_match(packet):
    '''Extract a match from packet'''
    packet = dpkt.ethernet.Ethernet(str(packet))
    try:
        tag = packet.tag
        vlan = tag & ((1 << 13) - 1)
        vlan_pcp = (tag & (0x7 << 13)) >> 13
    except:
        vlan = 0xffff
        vlan_pcp = None
    match = ofmatch.Match.create_wildcarded(
            dl_src=packet.src, dl_dst=packet.dst,
            dl_vlan=vlan, dl_vlan_pcp=vlan_pcp, dl_type=packet.type)
    if packet.type == dpkt.ethernet.ETH_TYPE_IP:
        ip = packet.data
        match = match._replace(nw_tos=ip.tos, nw_proto=ip.p,
                nw_src=(ip.src, 32), nw_dst=(ip.dst, 32))
        if ip.p == dpkt.ip.IP_PROTO_TCP:
            tcp = ip.data
            match = match._replace(tp_src=tcp.sport, tp_dst=tcp.dport)
    return match

def pkt_match(pkt, match):
    '''Check if @pkt matches Match @match'''
    a = extract_match(pkt)
    b = match
    wc = b.wildcards
    if not (a.nw_src is not None and b.nw_src is not None and \
            ((struct.unpack('!I', a.nw_src[0])[0] ^ struct.unpack('!I', b.nw_src[0])[0]) >> wc.nw_src)) \
        and not (a.nw_dst is not None and b.nw_dst is not None and \
        (struct.unpack('!I', a.nw_dst[0])[0] ^ struct.unpack('!I', b.nw_dst[0])[0]) >> wc.nw_dst) \
        and (wc.dl_src or a.dl_src == b.dl_src) \
        and (wc.dl_dst or a.dl_dst == b.dl_dst) \
        and (wc.dl_vlan or a.dl_vlan == b.dl_vlan) \
        and (wc.dl_vlan_pcp or a.dl_vlan_pcp == b.dl_vlan_pcp) \
        and (wc.dl_type or a.dl_type == b.dl_type) \
        and (wc.nw_tos or a.nw_tos == b.nw_tos) \
        and (wc.nw_proto or a.nw_proto == b.nw_proto) \
        and (wc.tp_src or a.tp_src == b.tp_src) \
        and (wc.tp_dst or a.tp_dst == b.tp_dst):
            return True
    return False

class Collector(object):
    def __init__(self, args):
        self.pkt_db = {}
        self.psid_to_dpid = {}
        self.args = args

        self.ft_db = MongoHandler(host=args.db_host, port=args.db_port, coll_name='flow-tables')
        self.psid_db = MongoHandler(host=args.db_host, port=args.db_port, coll_name='psid-to-dpid')
        self.topo_db = MongoHandler(host=args.db_host, port=args.db_port, coll_name='topo')

        self.topo = None  # NDBTopo object, set on start().

        self.logger = L.getLogger('Collector')
        self.logger.setLevel(L.INFO)
        if args.debug:
            print 'Setting log-level to debug'
            self.logger.setLevel(L.DEBUG)

    def start(self):
        '''start the collector'''

        topo_recs = self.topo_db.get_records()
        self.logger.debug("Getting topology info from DB")
        self.logger.debug("Got %d records" % topo_recs.count())
        topo_json = topo_recs[0] if topo_recs.count() > 0 else None
        self.logger.debug('%s' % repr(topo_json))
        self.topo = NDBTopo(topo_json=topo_json) if topo_recs.count() > 0 else None

        # Get psid-to-dpid mapping
        rec = self.psid_db.get_records()
        self.logger.debug("Getting psid-to-dpid info from DB")
        self.logger.debug("Got %d records" % rec.count())
        for r in rec:
            self.logger.debug('%s' % repr(r))
            self.psid_to_dpid[r['psid']] = r['dpid']

        if hasattr(self.args, 'psid_out') and self.args.psid_out is not None:
            f = open(self.args.psid_out, 'w')
            print >>f, json.dumps(self.psid_to_dpid)
            f.close()


    def stop(self):
        self.logger.info('Stopping collector.')

# Unit tests
class CollectorPPFTestCase(unittest.TestCase):

    def setUp(self):
	self.pcap_f = 'test/icmp_twoway_chain.pcap'

    def tearDown(self):
	pass

    def test_pcap_read(self):
	reader = pcapy.open_offline(self.pcap_f)
	while True:
	    try:
		(header, payload) = reader.next()
	    except pcapy.PcapError:
		break


if __name__ == "__main__":
    unittest.main()
