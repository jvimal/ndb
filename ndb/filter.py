import sys
import pcapy
import dpkt
import shlex
import re
import string

def id_generator(n):
    '''Return a list of n unique digits'''
    all_ids = string.ascii_uppercase + string.ascii_lowercase + string.digits
    assert n <= len(all_ids), 'Error! I can only generate %d ids. \
            You asked for %d.' % (len(all_ids), n)
    return all_ids[:n]

# XXX This is an incomplete implementation of imperative predicate extraction
# in that, it ignores more complicated constructs for PPF
def find_ipredicates(ppf_str):
    '''Find the imperative predicates in a PP. 
    A ipredicate is a state that the packet must assume along its path.
    '''
    ipredicates = []

    # replace individual PFs with ids
    mod_ppf_str = ''
    id_to_pf = {}
    ids = id_generator(None)
    ppf_re = re.compile('\{\{')

    i = 0
    curr_lookup_index = 0
    for m in ppf_re.finditer(ppf_str):
        mod_ppf_str += m.string[curr_lookup_index : m.start()]

        start_i = m.end()
        end_i = ppf_str.find('}}', start_i)
        id_to_pf[ids[i]] = ppf_str[start_i:end_i]
        mod_ppf_str += ids[i]

        curr_lookup_index = end_i + 2
        i += 1

    mod_ppf_str += ppf_str[curr_lookup_index:]

    ipredicate_str = mod_ppf_str
    ipredicate_str = re.sub('\(.*\)\*', '', ipredicate_str)
    ipredicate_str = re.sub('\[.*\]\*', '', ipredicate_str)
    ipredicate_str = re.sub('.\*', '', ipredicate_str)
    ipredicate_str = re.sub('\[\^.*\]', '', ipredicate_str)
    for c in ipredicate_str:
        if c in id_to_pf:
            ipredicates.append(id_to_pf[c])
    return ipredicates

class PacketFilter(object):
    '''
    A PacketFilter (PF) is a filter to match a pcard.
    A PF can have at most one of each of the following args:
        --bpf: A Berkeley Packet Filter to match a packet
        --dpid: datapath id of the switch generating the pcard
        --inport: input port at the switch
        --outport: output port at the switch
        --version : Flow entry version that the packet matched
    A pcard is a dictionary of the following form:
        {
            'pkt': <a pcapy Packet object>, 
            'dpid': <regex to match dpid>,
            'inport': <regex to match inport>,
            'outport': <regex to match outport>,
            'version': <regex on the matched flow entry version>
        }
    '''

    def __init__(self, pf_str=''):
        self.pf_str = pf_str
        self.pf_lst = shlex.split(self.pf_str)

        self.parse_pf_list()
        self.compile()

    def __repr__(self):
        return self.pf_str

    def get_filter(self, filter_type):
        '''get the filter string, or raise an exception if something fails'''
        try:
            i = self.pf_lst.index('%s' % filter_type)
            negate = False
            filter_str = self.pf_lst[i + 1]
            if filter_str == 'not':
                filter_str = self.pf_lst[i + 2]
                negate = True
            return (filter_str, negate)
        except Exception as e:
            raise e

    def parse_pf_list(self):
        # bpf
        try:
            (self.pkt_filter, self.bpf_negate) = self.get_filter('--bpf')
        except ValueError:
            self.pkt_filter = ''
            self.bpf_negate = False

        if 'ether dst' in self.pkt_filter:
            raise Exception('BPF in PacketFilter cannot contain \'ether dst\'')

        # dpid
        try:
            self.dpid_filter, self.dpid_negate = self.get_filter('--dpid')
        except ValueError:
            self.dpid_filter = '.*'
            self.dpid_negate = False

        # inport
        try:
            self.inport_filter, self.inport_negate = self.get_filter('--inport')
        except ValueError:
            self.inport_filter = '.*'
            self.inport_negate = False

        # outport
        try:
            self.outport_filter, self.outport_negate = self.get_filter('--outport')
        except ValueError:
            self.outport_filter = '.*'
            self.outport_negate = False

        # version
        try:
            self.version_filter, self.version_negate = self.get_filter('--version')
        except ValueError:
            self.version_filter = '.*'
            self.version_negate = False

    def compile(self):
        self.bpf = pcapy.compile(pcapy.DLT_EN10MB, 1500, self.pkt_filter, 0, 1)
        self.dpid_re = re.compile(self.dpid_filter)
        self.inport_re = re.compile(self.inport_filter)
        self.outport_re = re.compile(self.outport_filter)
        self.version_re = re.compile(self.version_filter)

    def match(self, pcard):
        '''Match PF on a pcard'''
        # Match bpf
        self.bpf_m = False
        if('pkt_str' in pcard):
            self.bpf_m = self.bpf.filter(pcard['pkt_str']) != 0
            if self.bpf_negate:
                self.bpf_m = not self.bpf_m
        elif('pkt' in pcard):
            pkt_str = pcard['pkt'].pack()
            self.bpf_m = self.bpf.filter(pkt_str) != 0
            if self.bpf_negate:
                self.bpf_m = not self.bpf_m

        # Match dpid
        self.dpid_m = False
        if('dpid' in pcard):
            dpid_match = self.dpid_re.match(repr(pcard['dpid']))
            self.dpid_m = (dpid_match is not None) and \
                    (dpid_match.group(0) == repr(pcard['dpid']))
            if self.dpid_negate:
                self.dpid_m = not self.dpid_m

        # Match inport
        self.inport_m = False
        if('inport' in pcard):
            inport_match = self.inport_re.match(repr(pcard['inport']))
            self.inport_m = (inport_match is not None) and \
                    (inport_match.group(0) == repr(pcard['inport']))
            if self.inport_negate:
                self.inport_m = not self.inport_m

        # Match outport
        self.outport_m = False
        if('outport' in pcard):
            outport_match = self.outport_re.match(repr(pcard['outport']))
            self.outport_m = (outport_match is not None) and \
                    (outport_match.group(0) == repr(pcard['outport']))
            if self.outport_negate:
                self.outport_m = not self.outport_m
                    
        # Match version
        self.version_m = False
        if('version' in pcard):
            version_match = self.version_re.match(repr(pcard['version']))
            self.version_m = (version_match is not None) and \
                    (version_match.group(0) == repr(pcard['version']))
            if self.version_negate:
                self.version_m = not self.version_m

        return (self.bpf_m and self.dpid_m and self.inport_m and self.outport_m and self.version_m)

class PacketPathFilter(object):
    '''
    A PacketPathFilter (PPF) is a filter on Packet Paths (PP) 
    A PPF is a regex string with PF as the base element
    Each PF must be enclosed between {{ and }}
    Therefore, a PPF has a nesting level of 1 (in terms of {{ and }})
    '''

    def __init__(self, ppf_str='', out_f=sys.stdout, syntax_check=True):
        self.ppf_str = ppf_str
        self.out_f = out_f
        # syntax check?
        if syntax_check:
            self.check()

    def __repr__(self):
        return self.ppf_str

    def check(self):
        mod_ppf_str = ''
        ppf_re = re.compile('\{\{')
        curr_lookup_index = 0
        for m in ppf_re.finditer(self.ppf_str):
            mod_ppf_str += m.string[curr_lookup_index : m.start()]

            start_i = m.end()
            end_i = self.ppf_str.find('}}', start_i)
            pf_str = self.ppf_str[start_i:end_i]
            pf = PacketFilter(pf_str)
            pcard_m = 'X'
            mod_ppf_str += pcard_m

            curr_lookup_index = end_i + 2
        
        mod_ppf_str += self.ppf_str[curr_lookup_index:]
        print >> self.out_f, 'Added filter that looks like: "%s"' % mod_ppf_str
        print >> self.out_f, 'Where, X is a postcard filter'

    def find_dup(self, pcard_lst, mod_pcard_str):
        '''Identify duplicate postcard IDs from packetpath'''
        assert len(pcard_lst) == len(mod_pcard_str)
        pcard_id_lst = list(mod_pcard_str)

        for i in range(len(pcard_lst)-1):
            for j in range(i+1, len(pcard_lst)):
                a = pcard_lst[i]
                b = pcard_lst[j]
                if a['dpid'] == b['dpid'] and \
                        a['inport'] == b['inport'] and \
                        a['outport'] == b['outport'] and \
                        a['version'] == b['version'] and \
                        a['pkt'].pack() == b['pkt'].pack():
                        pcard_id_lst[j] = pcard_id_lst[i]
        return ''.join(pcard_id_lst)

    def match(self, pcard_lst):
        '''Match PPF on a topo-sorted list of pcards'''
        # Assign unique id to each pcard in pcard_lst
        # Update mod_pcard_str while doing so
        mod_pcard_str = id_generator(len(pcard_lst) + 1) 

        false_pcard = mod_pcard_str[-1]
        mod_pcard_str = mod_pcard_str[:-1]

        # Identify duplicate IDs -- useful for loop detection
        mod_pcard_str = self.find_dup(pcard_lst, mod_pcard_str)

        # Create mod_ppf_str by replacing each pf_str by the ids of all the
        # matching pcards
        mod_ppf_str = ''
        ppf_re = re.compile('\{\{')
        curr_lookup_index = 0
        for m in ppf_re.finditer(self.ppf_str):
            mod_ppf_str += m.string[curr_lookup_index : m.start()]

            start_i = m.end()
            end_i = self.ppf_str.find('}}', start_i)
            pf_str = self.ppf_str[start_i:end_i]
            pf = PacketFilter(pf_str)

            # Don't enclose within brackets if already backet enclosed
            # We exploit the fact that there can't be nested brackets in PPFs
            left_str = self.ppf_str[:m.start()]
            right_str = self.ppf_str[end_i + 2:]

            left_bracket_enclosed = (left_str.rfind('[') >= 0) and \
                    (left_str.rfind('[') > left_str.rfind(']'))
            right_bracket_enclosed = (right_str.find(']') >= 0) and \
                    (right_str.find('[') < 0 or (right_str.find(']') <
                        right_str.find('[')))
            bracket_enclosed = left_bracket_enclosed and right_bracket_enclosed

            pcard_m = '[' if not bracket_enclosed else ''
            for i, pcard in enumerate(pcard_lst):
                if pf.match(pcard):
                    pcard_m += mod_pcard_str[i]

            # The last id is used as an alias for 'False'
            pcard_m += false_pcard 
            if not bracket_enclosed:
                pcard_m += ']'

            mod_ppf_str += pcard_m
            curr_lookup_index = end_i + 2
        
        mod_ppf_str += self.ppf_str[curr_lookup_index:]

#        print 'mod_pcard_str: "%s"' % mod_pcard_str
#        print 'mod_ppf_str: "%s"' % mod_ppf_str

        # Match mod_pcard_str against mod_ppf_str
        if re.match(mod_ppf_str, mod_pcard_str) is not None:
            return True
        return False

def packet_path_match(ppf, pcard_lst):
    '''Wrapper function useful for analysis'''
    return ppf.match(pcard_lst)

if __name__ == '__main__':
    import unittest
    import dpkt
    import struct

    def create_pp(pcap_fname):
        pcapReader = dpkt.pcap.Reader(file(pcap_fname))
        pp = []
        for ts, data in pcapReader:
            pkt = dpkt.ethernet.Ethernet(data)
            metadata = pkt.dst
            metadata = '\x00' + metadata
            version, port, dpid = struct.unpack('>IHB', metadata)
            pp.append({
                'pkt':pkt,
                'dpid': dpid,
                'inport': port,
                'outport': None,
                'version':version
                })
        return pp

    class PacketFilterTestCase(unittest.TestCase):

        def setUp(self):
            self.pcap_f = 'test/icmp_chain.pcap'
            self.pp = create_pp(self.pcap_f)
            self.pcard = self.pp[0]

        def tearDown(self):
            pass

        def test_pcap_parse(self):
            self.assertIs(type(self.pcard), dict, "pcard isn't of type dict")

        def test_null_pf(self):
            self.assertTrue(PacketFilter('').match(self.pcard), 
                    "Pcard doesn't match NULL filter")

        def test_icmp_pf(self):
            self.assertTrue(PacketFilter('icmp[icmptype] = icmp-echo').match(self.pcard), 
                    "Pcard doesn't match ICMP filter")

    class PacketPathFilterTestCase(unittest.TestCase):

        def setUp(self):
            self.pcap_f = 'test/icmp_request_chain.pcap'
            self.pp = create_pp(self.pcap_f)

        def tearDown(self):
            pass

        def test_pp_parse(self):
            self.assertIs(type(self.pp), list, "PacketPath isn't of type list")

        def test_null_ppf(self):
            ppf = ''
            self.assertTrue(PacketPathFilter(ppf).match(self.pp), 
                    "PacketPath doesn't match NULL filter")

        def test_icmp_ppf(self):
            ppf = '^{{--bpf "icmp[icmptype] = icmp-echo"}}{3}'
            self.assertTrue(PacketPathFilter(ppf).match(self.pp), 
                    "PacketPath doesn't match ICMP filter")
    unittest.main()
