import logging as L
import argparse
import StringIO
import sys
import base64
from subprocess import Popen
import os
from threading import Timer, Thread

from openfaucet import ofproto, ofmatch, buffer
import dpkt
import termcolor as T
from scapy.all import *
import bjsonrpc
from bjsonrpc.handlers import BaseHandler

from collector import Collector, get_tag
from timestamp import tstamp
import topo_sort
from nox_ext import NOXVendorHandler
from filter import PacketPathFilter, find_ipredicates
from analysis.postcards import clean_dstmac


L.basicConfig()
global args
args = None


def parse_args():
    parser = argparse.ArgumentParser(description="Simple NDB master collector implementation")

    parser.add_argument('--debug', '-d',
                        dest="debug",
                        action="store_true",
                        help="Print debug messages",
                        default=False)

    parser.add_argument('--pcap-out',
                        dest="pcap_out",
                        help="File to save all matched postcards",
                        default=None)

    parser.add_argument('--psid-out', 
                        dest="psid_out",
                        help="File to save psid->dpid mapping",
                        default=None)

    parser.add_argument('--noprintbt',
                        dest="noprintbt",
                        action="store_true",
                        help="Do not print the backtrace",
                        default=False)

    parser.add_argument('--db-host',
                        dest="db_host",
                        help="Host name/IP of the FlowTable database",
                        default='localhost')

    parser.add_argument('--db-port',
                        dest="db_port",
                        action="store",
                        help="Port of FlowTable database",
                        default=27017)

    parser.add_argument('--master-host',
                        dest="m_host",
                        help="Host name/IP of the Master collector",
                        default='0.0.0.0')

    parser.add_argument('--master-port',
                        dest="m_port",
                        action="store",
                        help="Port of Master collector",
                        default=10123)

    args = parser.parse_args()
    return args

# global variable for the master collector object
global c
c = None


def print_packetpath(pp, ft_db):
    def describe_postcard(pcard):
        pkt = pcard['pkt']
        dpid = pcard['dpid']
        inport = pcard['inport']
        outport = pcard['outport']
        ver = pcard['version']

        out = StringIO.StringIO()
        rec = ft_db.get_records({'dpid': dpid, 'version': ver})
        assert rec.count() == 1
        for r in rec:
            match_str = str(r['match'])
            actions_str = [str(a) for a in r['actions']]
        # Deserialize binary strings to corresponding Match/Action objects
        try:
            b = buffer.ReceiveBuffer()
            b.append(match_str)
            b.set_message_boundaries(len(match_str))
            match = ofmatch.Match.deserialize(b)
        except:
            match = match_str
        actions = []
        ofp = ofproto.OpenflowProtocol()
        nox_handler = NOXVendorHandler(ofp)
        ofp.set_vendor_handlers([nox_handler])
        for a_str in actions_str:
            try:
                b = buffer.ReceiveBuffer()
                b.append(a_str)
                b.set_message_boundaries(len(a_str))
                a = ofp.deserialize_action(b)
            except:
                print "Unexpected error:", sys.exc_info()[0]
                print 'Error: Could not deserialize action %s' % repr(a_str)
                a = a_str
            actions.append(a)

        print >>out, "Switch: dpid %s (0x%x)" % (dpid, dpid)
        print >>out, "\t", "packet: %s" % repr(pkt)
        print >>out, "\t", "match: %s" % repr(match)
        print >>out, "\t", "action: %s" % repr(actions)
        print >>out, "\t", "version: %s" % ver
        print >>out, "\t", "inport: %s" % (inport if inport else 'Unknown')
        print >>out, "\t", "outport: %s" % outport
        return out.getvalue()

    for pcard in pp:
        print T.colored(describe_postcard(pcard), 'red', 'on_yellow')


class Master(Collector):
    '''The master collector'''

    def __init__(self, args):
        super(Master, self).__init__(args)

        self.slave_conns = []
        self.pcap = args.pcap_out if args.pcap_out else None
        
        self.pp_filters = {}
        self.ppf_lst = []
        self.pp_filter_timers = {}
        self.ipredicates = {}
        self.matched_ipredicate_db = {}
        self.tstamp_dict = {}
        self.tstamp_logger = L.getLogger('TIMESTAMP')
        self.tstamp_logger.setLevel(L.INFO)

    def start(self):
        '''start the collector'''

        super(Master, self).start()

        self.p = Thread(target=self.start_rpc_server, args=())
        self.p.daemon = True
        self.p.start()

    def start_rpc_server(self):
        # create a jsonrpc server
        self.logger.info('Starting RPC server')
        self.s = bjsonrpc.createserver(host=args.m_host, port=args.m_port,
                handler_factory=MasterHandler)

        # All set: start the server
        self.s.serve()

    def handle_filter_timeout(self, ppf):
        if ppf in self.pp_filters:
            cb = self.pp_filters[ppf]
            if cb:
                cb(None)
            else:
                print T.colored('TIMEOUT: %s' % repr(ppf), 'red', 'on_yellow')
        if ppf in self.pp_filter_timers:
            del self.pp_filter_timers[ppf]

    def add_filter(self, ppf_str, cb=None, timeout=None):
        self.logger.debug('add_filter: "%s"' % ppf_str)
        ppf = PacketPathFilter(ppf_str)
        self.ppf_lst.append(ppf)
        self.pp_filters[ppf] = cb

        ipredicates = find_ipredicates(ppf_str)
        self.ipredicates[ppf] = ipredicates

        self.logger.debug('Sending ipredicates to %d slave(s).' %
                len(self.slave_conns))
        self.logger.debug('\t%s' % repr(ipredicates))
        for slave in self.slave_conns:
            slave.call.add_ipredicates(ppf_str, ipredicates)

        if timeout:
            t = Timer(timeout, self.handle_filter_timeout, (ppf, ))
            self.pp_filter_timers[ppf] = t
            t.start()

    def del_filter(self, ppf_str):
        for ppf in ppf_lst:
            if ppf.ppf_str == ppf_str:
                self.ppf_lst.remove(ppf)
                del self.pp_filters[ppf]
                del self.ipredicates[ppf]
                return
        self.logger.error('Could not find matching PPF: "%s"' % ppf_str)
        return

    def flush_filters(self):
        self.ppf_lst = []
        self.pp_filters = {}
        for slave in self.slave_conns:
            slave.call.del_ipredicates(self.ipredicates)
        self.ipredicates = {}

    def get_matching_packets(self, p_hash):
        """Returns a bunch of packets from the slave collectors that match @pkt in
        non-header fields."""
        pkts = []
        for slave in self.slave_conns:
            buf_lst = slave.call.get_pcards(p_hash)
            pkts += [dpkt.ethernet.Ethernet(base64.b64decode(buf)) for buf in buf_lst]

        ret = []
        for pkt in pkts:
            ret.append({
                    'pkt': pkt,
                    'tag': get_tag(pkt)
                    })
        return ret

    def topo_sort(self, matches):
        return topo_sort.topo_sort(matches, self.psid_to_dpid, self.topo, False,
                         self.ft_db, self.logger)

    def clean_dstmac(self, pp):
        clean_dstmac(pp)

    def handle_packetpath(self, p_hash):
        self.logger.debug('handle_packetpath: %s' % (str(p_hash),))

        if p_hash not in self.matched_ipredicate_db:
            self.logger.error('Error! p_hash not in matched_ipredicate_db.',  
                    'This shouldn\'t happen.')
            return

        if p_hash not in self.tstamp_dict:
            self.tstamp_dict[p_hash] = ''

        for i in self.matched_ipredicate_db[p_hash]:
            ppf = self.ppf_lst[i]
            self.logger.debug('Checking for match with PPF: %s' % ppf.ppf_str)
            matched_ipredicates = self.matched_ipredicate_db[p_hash][i]
            ipredicates = self.ipredicates[ppf]

            if len(matched_ipredicates) < len(ipredicates):
                self.logger.debug('Not enough matches: %d < %d' %
                        (len(matched_ipredicates), len(ipredicates)))
                continue

            out = False
            for ip in range(len(ipredicates)):
                if ip not in matched_ipredicates:
                    out = True
                    break
            if out:
                self.logger.debug('Not all ipredicates matched')
                continue

            self.tstamp_dict[p_hash] += '\nTIMESTAMP:' + tstamp('%s %s' % ('GET', p_hash))
            matches = self.get_matching_packets(p_hash)
            self.logger.debug('got %d postcards matching packet id %s' %
                    (len(matches), str(p_hash)))
            self.tstamp_dict[p_hash] += '\nTIMESTAMP:' + tstamp('%s %s' % ('RECEIVED', p_hash))
            pp = self.topo_sort(matches)
            self.clean_dstmac(pp)
            self.tstamp_dict[p_hash] += '\nTIMESTAMP:' + tstamp('%s %s' %
                    ('SORTED', p_hash))
            self.logger.debug('%s' % repr(pp))
            for (ppf, cb) in self.pp_filters.iteritems():
                if ppf.match(pp):
                    self.tstamp_dict[p_hash] += '\nTIMESTAMP:' + tstamp('%s %s' % ('MATCHED', p_hash))
                    if cb:
                        cb(pp)
		    elif not args.noprintbt:
                        print_packetpath(pp, self.ft_db)
                    # cleanup timers
                    if ppf in self.pp_filter_timers:
                        t = self.pp_filter_timers[ppf]
                        t.cancel()
                        del self.pp_filter_timers[ppf]

                    # dump pcards into outfile
                    # unforunately, we have to adopt this roundabout technique because
                    # dpkt.pcap.Writer doesn't work :(
                    if self.pcap:
                        bufs = [pcard['pkt'].pack() for pcard in pp]
                        scapy_pkts = [Ether(data) for data in bufs]
                        tmp_pcap = '/tmp/master_tmp.pcap'
                        old_tmp_pcap = '/tmp/master_old_tmp.pcap'
                        wrpcap(tmp_pcap, scapy_pkts)
                        if os.path.isfile(self.pcap):
                            Popen('cp %s %s' % (self.pcap, old_tmp_pcap), shell=True).wait()
                            Popen('mergecap -w %s %s %s' % (self.pcap, old_tmp_pcap, tmp_pcap), shell=True).wait()
                        else:
                            Popen('cp %s %s' % (tmp_pcap, self.pcap), shell=True).wait()
                        Popen('rm -rf %s %s' % (tmp_pcap, old_tmp_pcap), shell=True).wait()

                else:
                    self.logger.debug('packet path did not match the PPF "%s"' % ppf.ppf_str)
            del self.matched_ipredicate_db[p_hash]

            self.tstamp_logger.info(self.tstamp_dict[p_hash])
            del self.tstamp_dict[p_hash]


class MasterHandler(BaseHandler):
    '''JSON-RPC handler for the master collector'''

    def _setup(self):
        self.collector = c
        self.logger = L.getLogger('MasterHandler')
        self.logger.setLevel(L.INFO)
        if args.debug:
            self.logger.info('Setting log-level to debug')
            self.logger.setLevel(L.DEBUG)

    def _shutdown(self):
        self.logger.debug('Shutting down MasterHandler')
        if self._conn in self.collector.slave_conns:
            self.collector.slave_conns.remove(self._conn)

    def hello(self):
        '''Notification method for a slave to connect to the master'''
        # add _conn to the list of connections
        self.logger.info('Received HELLO message from slave no. %d' %
                (len(self.collector.slave_conns) + 1))
        if self._conn not in self.collector.slave_conns:
            self.logger.debug('... adding it to the list of slaves')
            self.collector.slave_conns.append(self._conn)

            # add existing PPFs and ipredicates to the slave
            if len(c.ppf_lst) > 0:
                self.logger.info('Adding existing ipredicates to the slave')
            for ppf in c.ppf_lst:
                self._conn.call.add_ipredicates(ppf.ppf_str, c.ipredicates[ppf])

    def matched(self, pkt_id, matched_ppf, matched_ipredicates):
        self.logger.debug('Received MATCHED message from a slave:')
        self.logger.debug('\tpkt_id: %s' % pkt_id)
        self.logger.debug('\tmatched_ppf: %s' % matched_ppf)
        self.logger.debug('\tmatched_ipredicates: %s' %
                repr(matched_ipredicates))
        if pkt_id not in self.collector.matched_ipredicate_db:
            self.collector.matched_ipredicate_db[pkt_id] = {}
            Timer(1.0, self.collector.handle_packetpath, (pkt_id, )).start()

        if matched_ppf not in self.collector.matched_ipredicate_db[pkt_id]:
            self.collector.matched_ipredicate_db[pkt_id][matched_ppf] = []

        self.collector.matched_ipredicate_db[pkt_id][matched_ppf] += matched_ipredicates

def interact(*__args):
    global c
    shell = code.InteractiveConsole(globals())
    shell.interact()

if __name__ == "__main__":
    args = parse_args()

    c = Master(args)
    c.start()
    interact()
