import logging as L
import dpkt
import argparse
from subprocess import Popen
import code
import sys
import os
import time
import shlex
import termcolor as T
from scapy.all import *
from threading import Timer, Thread
import base64
import bjsonrpc
from bjsonrpc.handlers import BaseHandler
from filter import PacketFilter
from collector import Collector 
from collector import pkt_hash, get_tag, describe_tag, sniff_pkts
from timestamp import tstamp

L.basicConfig()

global args
args = None

def parse_args():
    parser = argparse.ArgumentParser(description="Simple NDB slave collector implementation")
    parser.add_argument('--intf',
                        dest="intf",
                        help="Interface to run collector",
                        default=None)

    parser.add_argument('--outfile',
                        dest="outfile",
                        help="File to save all collected postcards",
                        default=None)

    parser.add_argument('--topo',
                        dest="topo",
                        help="Topology input in json format",
                        default=None)

    parser.add_argument('--debug',
                        dest="debug",
                        action="store_true",
                        help="Print debug messages",
                        default=False)

    parser.add_argument('--db-host',
                        dest="db_host",
                        help="Host name/IP of the FlowTable database",
                        default='localhost')

    parser.add_argument('--db-port',
                        dest="db_port",
                        action="store",
                        help="Port of FlowTable database",
                        default=27017)

    parser.add_argument('--master-host',
                        dest="m_host",
                        help="Host name/IP of the Master collector",
                        default='127.0.0.1')

    parser.add_argument('--master-port',
                        dest="m_port",
                        action="store",
                        help="Port of Master collector",
                        default=10123)

    args = parser.parse_args()
    return args

# debug space
debug_vlan = 0x2

# global variable for the slave collector object
global c
c = None

class Slave(Collector):
    '''The slave collector'''

    def __init__(self):
        super(Slave, self).__init__(args)

        self.file = args.outfile
        self.intf = args.intf
        self.p = None
        self.collect_vlan = debug_vlan
        self.tmp_pcap = '/tmp/collector_%s.pcap' % self.intf
        self.old_tmp_pcap = '/tmp/collector_old_%s.pcap' % self.intf
        self.tstamp_dict = {}
        self.tstamp_logger = L.getLogger('TIMESTAMP')
        self.tstamp_logger.setLevel(L.INFO)

    def start(self):
        '''start the collector'''

        super(Slave, self).start()

        # connect to the master
        self.connect_master()

        # Start the packet sniffer
        self.logger.info('Starting slave collector... sniffing packets on %s' % self.intf)
        self.p = Thread(target=sniff_pkts, args=(self.intf, self.handle_postcard,))
        self.p.daemon = True
        self.p.start()

    def stop(self):
        Popen('rm -rf %s %s' % (self.tmp_pcap, self.old_tmp_pcap), shell=True).wait()
        super(Slave, self).stop()

    def connect_master(self):
        '''Try connecting to the Master collector via an RPC channel'''

        self.ppf_lst = []
        self.ipredicates = {}
        self.ipredicate_pf = {}
        self.master = None

        self.logger.info('Attempting to connect to the master.')

        # connect to the server
        # NOTE: this statement should not be called in the constructor, because
        # SlaveHandler._setup needs a handle on global variable c
        self.connected = False
        retries = 0
        max_retries = 100
        while (not self.connected) and (retries < max_retries):
            try:
                self.master = bjsonrpc.connect(host=args.m_host, port=args.m_port,
                        handler_factory=SlaveHandler)
                self.connected = True
            except:
                print '.',
                sys.stdout.flush()
                retries += 1
                time.sleep(1)
        print

        if retries >= max_retries:
            self.logger.info('Exceeded max no. of retries. Exiting.')
            self.stop()
            sys.exit()

        # say hello to the master
        self.master.notify.hello()

        # Serve RPC calls from the other side in a separate thread
        self.server_thread = Thread(target=self.listen_master)
        self.server_thread.daemon = True
        self.server_thread.start()


    def listen_master(self):
        try:
            self.logger.info('Listening on the RPC channel to the master...')
            self.master.serve()
        except:
            self.logger.info('Master seems to have disconnected.\
                    Shutting down the RPC channel')
            self.connect_master()

    def handle_postcard(self, hdr, data):
        '''handler for each received packet'''
        self.logger.debug('handle_postcard: entering the function')
        buf = dpkt.ethernet.Ethernet(data)
        # collect only vlan-tagged postcards
        if not self.debug_vlan(buf):
            self.logger.debug('handle_postcard: VLAN tag not present')
            return
        h = pkt_hash(buf)
        tag = get_tag(buf)
        self.logger.debug('handle_postcard: %s: %s' % (str(h), describe_tag(tag)))
        self.logger.debug('handle_postcard: %s' % repr(buf))

        if h not in self.tstamp_dict:
            self.tstamp_dict[h] = ''
        self.tstamp_dict[h] = '\nTIMESTAMP:' + tstamp('%s %s' %('RECEIVED', h))

        # save the postcard in a temp datastructure
        pkt_list = self.pkt_db.get(h)
        if pkt_list:
            pkt_list.append(buf)
        else:
            self.pkt_db[h] = [buf,]
            # sleep for a while, and clear the packets at the end of it
            # NOTE: This value is totally arbitrary; needs more thought
            Timer(5.0, self.clear_postcards, (h,)).start()

        # create a pcard dict from the buf
        (buf_version, buf_outport, buf_psid) = tag
        buf_dpid = self.psid_to_dpid.get(buf_psid)
        pcard = {'pkt': buf, 
                'dpid': buf_dpid, 
                'inport': None, 
                'outport': buf_outport,
                'version': buf_version
                }

        # find the list of ipredicates that the postcard matches
        for (ppf_i, ppf) in enumerate(self.ppf_lst):
            matched_ipredicates = []
            self.logger.debug('handle_postcard: matching PPF "%s"' % ppf)
            for (i, pf) in enumerate(self.ipredicate_pf[ppf]):
                self.logger.debug('handle_postcard: matching ipredicate "%s"' %
                        pf)
                if pf.match(pcard):
                    self.logger.debug('handle_postcard: matched PPF no. %d, \
                            ipredicate no. %d' % (ppf_i, i))
                    matched_ipredicates.append(i)
                else:
                    self.logger.debug('handle_postcard: did not match PPF no. %d, \
                            ipredicate no. %d' % (ppf_i, i))

            # if the postcard matches any ipredicates, notify the master
            if self.master and (len(self.ipredicate_pf[ppf]) == 0 or len(matched_ipredicates) > 0):
                self.logger.debug('handle_postcard: notifying the master with \
                        matched message')
                self.tstamp_dict[h] += '\nTIMESTAMP:' + tstamp('%s %s' %('NOTIFY', h))
                self.master.notify.matched(h, ppf_i, matched_ipredicates)

        # Save packet in a pcap file
        if self.file is None:
            self.logger.debug('handle_postcard: returning')
            return

        scapy_pkt = Ether(data)
        wrpcap(self.tmp_pcap, scapy_pkt)
        if os.path.isfile(self.file):
            Popen('cp %s %s' % (self.file, self.old_tmp_pcap), shell=True).wait()
            Popen('mergecap -w %s %s %s' % (self.file, self.old_tmp_pcap, self.tmp_pcap), shell=True).wait()
        else:
            Popen('cp %s %s' % (self.tmp_pcap, self.file), shell=True).wait()
        Popen('rm -rf %s %s' % (self.tmp_pcap, self.old_tmp_pcap), shell=True).wait()

    def clear_postcards(self, p_hash):
        if p_hash in self.pkt_db:
            del self.pkt_db[p_hash]

        # Log the timestamp string when deleting it from the buffer
        if p_hash in self.tstamp_dict:
            self.tstamp_logger.info(self.tstamp_dict[p_hash])
            del self.tstamp_dict[p_hash]

    def debug_vlan(self, buf):
        try:
            tag = buf.tag
            vlan_id = tag & ((1 << 13) - 1)
            vlan_pcp = (tag & (0x7 << 13)) >> 13
            self.logger.debug('debug_vlan: vlan_id: %d, vlan_pcp: %d' % (vlan_id, vlan_pcp))
            return vlan_id == self.collect_vlan
        except:
            self.logger.debug('debug_vlan: %s' % sys.exc_info()[0])
            self.logger.debug('debug_vlan: VLAN tag not present')
            pass
        return False

class SlaveHandler(BaseHandler):
    '''JSON-RPC handler for the slave collector'''

    def _setup(self):
        global c
        self.collector = c
        self.logger = L.getLogger('SlaveHandler')
        self.logger.setLevel(L.INFO)
        if args.debug:
            self.logger.info('Setting log-level to debug')
            self.logger.setLevel(L.DEBUG)


    def add_ipredicates(self, ppf_str, ipredicates):
        self.logger.debug('Received add_ipredicates message from the Master')
        self.logger.debug('PPF: "%s"' % ppf_str)
        self.logger.debug('ipredicates: %s' % ipredicates)
        self.collector.ppf_lst.append(ppf_str)
        self.collector.ipredicates[ppf_str] = ipredicates
        self.collector.ipredicate_pf[ppf_str] = []
        for ip in self.collector.ipredicates[ppf_str]:
            # remove "--inport" from ipredicate... 
            # as we can't figure it out from a single postcard
            ip_lst = shlex.split(ip)
            if '--inport' in ip_lst:
                inport_i = ip_lst.index('--inport')
                del ip_lst[inport_i: inport_i + 2]
            mod_ip = ' '.join(ip_lst)

            self.collector.ipredicate_pf[ppf_str].append(PacketFilter(mod_ip))

    def del_ipredicates(self, ppf_str):
        if ppf_str in self.collector.ppf_lst:
            self.collector.ppf_lst.remove(ppf_str)
            del self.collector.ipredicates[ppf_str]
            del self.collector.ipredicate_pf[ppf_str]

    def get_pcards(self, pkt_id):
        self.logger.debug('get_pcards: %s' % pkt_id)
        # return the packets encoded in base64
        ret = [base64.b64encode(pkt.pack()) for pkt in self.collector.pkt_db.get(pkt_id, [])]
        self.logger.debug('Returning: %s' % repr(ret))
        return ret

def interact(*__args):
    global c
    shell = code.InteractiveConsole(globals())
    shell.interact()

if __name__ == "__main__":
    args = parse_args()

    c = Slave()
    c.start()
    interact()
