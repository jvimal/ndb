#include <crafter.h>
extern "C"
{
      #include "regexp.h"
}

/*
 * Code to read packet histories from a stored file
 * Assumed format:
 * The file has the following structure:
 *      Packet_History[]: A series of packet histories
 * Each history has the following structure:
 *      int len_history: No. of non-NULL postcards in the history
 *      Postcard[]: a series of "NULL-terminated" packet histories
 * Each postcard has the following structure:
 *      int len: length of the packet
 *      char *pkt: packet buffer (128 Bytes max)
 *      int dpid: datapath ID of the swtich
 *      int inport: input port
 *      int version: forwarding state version
 * The NULL postcard must return true with eo_postcard()
 * */

void
read_postcard(FILE *fp, Postcard *p)
{
    size_t ret = 0;

    ret = fread(&p->len, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not read Postcard.len\n");
        // reset all postcard fields
        set_null_postcard(p);
        return;
    }

    ret = fread(p->pkt, sizeof(char), p->len, fp);
    if(ret < (size_t) p->len) {
        fprintf(stderr, "Could not read Postcard.pkt\n");
        // reset all postcard fields
        set_null_postcard(p);
        return;
    }

    ret = fread(&p->dpid, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not read Postcard.dpid\n");
        // reset all postcard fields
        set_null_postcard(p);
        return;
    }

    ret = fread(&p->inport, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not read Postcard.inport\n");
        // reset all postcard fields
        set_null_postcard(p);
        return;
    }

    ret = fread(&p->version, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not read Postcard.version\n");
        // reset all postcard fields
        set_null_postcard(p);
        return;
    }
}

Postcard*
read_packet_history(FILE *fp)
{
    int len_history;
    int i;
    int ret = 0;
    Postcard *p = NULL;

    // read length of the packet history
    ret = fread(&len_history, sizeof(int), 1, fp);
    if(ret < 1) {
        return p;
    }

    p = (Postcard *)malloc(sizeof(Postcard) * (len_history + 1));

    for(i = 0; i < len_history + 1; i++) {
        read_postcard(fp, &p[i]);
    }

    return p;
}

void 
write_postcard(FILE *fp, Postcard *p)
{
    int ret = 0;

    ret = fwrite(&p->len, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not write Postcard.len\n");
        return;
    }

    ret = fwrite(p->pkt, sizeof(char), p->len, fp);
    if(ret < p->len) {
        fprintf(stderr, "Could not write Postcard.pkt\n");
        return;
    }

    ret = fwrite(&p->dpid, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not write Postcard.dpid\n");
        return;
    }

    ret = fwrite(&p->inport, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not write Postcard.inport\n");
        return;
    }

    ret = fwrite(&p->version, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not write Postcard.version\n");
        return;
    }
}

void
write_packet_history(FILE *fp, Postcard *p, int len_history)
{
    int i;
    int ret;

    ret = fwrite(&len_history, sizeof(int), 1, fp);
    if(ret < 1) {
        fprintf(stderr, "Could not write len_history\n");
        return;
    }

    for(i = 0; i < len_history + 1; i++) {
        write_postcard(fp, &p[i]);
    }
}

