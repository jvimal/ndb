import ctypes

class bpf_insn(ctypes.Structure):
	_fields_ = [("code",ctypes.c_short),("jt",ctypes.c_uint8),("jf",ctypes.c_uint8),("k",ctypes.c_int32)]
	# Instruction classes
	@staticmethod
	def BPF_CLASS(code):
		return code & 0x07
	def _class(self):
		return self.BPF_CLASS(self.code)
	BPF_LD,BPF_LDX,BPF_ST,BPF_STX,BPF_ALU,BPF_JMP,BPF_RET,BPF_MISC  = range(0,8)
	# ld/ldx fields
	def _size(self):
		return self.code & 0x18
	BPF_W,BPF_H,BPF_B  = (0x00,0x08,0x10)
	def _mode(self):
		return self.code & 0xe0
	BPF_IMM,BPF_ABS,BPF_IND,BPF_MEM,BPF_LEN,BPF_MSH = (0x00,0x20,0x40,0x60,0x80,0xa0)
	# alu/jmp fields 
	def _op(self):
		return self.code & 0xf0
	BPF_ADD,BPF_SUB,BPF_MUL,BPF_DIV,BPF_OR,BPF_AND,BPF_LSH,BPF_RSH,BPF_NEG = (0x00,0x10,0x20,0x30,0x40,0x50,0x60,0x70,0x80)
	BPF_JA,BPF_JEQ,BPF_JGT,BPF_JGE,BPF_JSET = (0x00,0x10,0x20,0x30,0x40)
	def _src(self):
		return code&0x08
	BPF_K,BPF_X = (0x00,0x08)
	# ret - BPF_K and BPF_X also apply 
	def _rval(self):
		return code & 0x18
	BPF_A = 0x10
	# misc 
	def _miscop(self):
		return code & 0xf8
	BPF_TAX,BPF_TXA = (0x00,0x80)
	_mnemonics = {	
		BPF_RET|BPF_K : ("ret","#%d"),
		BPF_RET|BPF_A : ("ret",""),
		BPF_LD|BPF_W|BPF_ABS : ("ld","[%d]"),
		BPF_LD|BPF_H|BPF_ABS : ("ldh","[%d]"),
		BPF_LD|BPF_B|BPF_ABS : ("ldb","[%d]"),
		BPF_LD|BPF_W|BPF_LEN : ("ld","#pktlen"),
		BPF_LD|BPF_W|BPF_IND : ("ld","[x + %d]"),
		BPF_LD|BPF_H|BPF_IND : ("ldh","[x + %d]"),
		BPF_LD|BPF_B|BPF_IND : ("ldb","[x + %d]"),
		BPF_LD|BPF_IMM : ("ld","#0x%x"),
		BPF_LDX|BPF_IMM : ("ldx","#0x%x"),
		BPF_LDX|BPF_MSH|BPF_B : ("ldxb","4*([%d]&0xf)"),
		BPF_LD|BPF_MEM : ("ld","M[%d]"),
		BPF_LDX|BPF_MEM : ("ldx","M[%d]"),
		BPF_ST : ("st","M[%d]"),
		BPF_STX : ("stx","M[%d]"),
		BPF_JMP|BPF_JA : ("ja","%d", lambda n,k,c: n + 1 + k),
		BPF_JMP|BPF_JGT|BPF_K : ("jgt","#0x%x"),
		BPF_JMP|BPF_JGE|BPF_K : ("jge","#0x%x"),
		BPF_JMP|BPF_JEQ|BPF_K : ("jeq","#0x%x"),
		BPF_JMP|BPF_JSET|BPF_K : ("jset","#0x%x"),
		BPF_JMP|BPF_JGT|BPF_X : ("jgt","x"),
		BPF_JMP|BPF_JGE|BPF_X : ("jge","x"),
		BPF_JMP|BPF_JEQ|BPF_X : ("jeq","x"),
		BPF_JMP|BPF_JSET|BPF_X : ("jset","x"),
		BPF_ALU|BPF_ADD|BPF_X : ("add","x"),
		BPF_ALU|BPF_SUB|BPF_X : ("sub","x"),
		BPF_ALU|BPF_MUL|BPF_X : ("mul","x"),
		BPF_ALU|BPF_DIV|BPF_X : ("div","x"),
		BPF_ALU|BPF_AND|BPF_X : ("and","x"),
		BPF_ALU|BPF_OR|BPF_X : ("or","x"),
		BPF_ALU|BPF_LSH|BPF_X : ("lsh","x"),
		BPF_ALU|BPF_RSH|BPF_X : ("rsh","x"),
		BPF_ALU|BPF_ADD|BPF_K : ("add","#%d"),
		BPF_ALU|BPF_SUB|BPF_K : ("sub","#%d"),
		BPF_ALU|BPF_MUL|BPF_K : ("mul","#%d"),
		BPF_ALU|BPF_DIV|BPF_K : ("div","#%d"),
		BPF_ALU|BPF_AND|BPF_K : ("and","#0x%x"),
		BPF_ALU|BPF_OR|BPF_K : ("or","#0x%x"),
		BPF_ALU|BPF_LSH|BPF_K : ("lsh","#%d"),
		BPF_ALU|BPF_RSH|BPF_K : ("rsh","#%d"),
		BPF_ALU|BPF_NEG : ("neg",""),
		BPF_MISC|BPF_TAX : ("tax",""),
		BPF_MISC|BPF_TXA : ("txa","")
	}

	def __repr__(self):
		return "{code} {jt} {jf} {k}".format(code=self.code,jt=self.jt,jf=self.jf,k=self.k)

	def dump(self,n=0):
		if self.code not in self._mnemonics:
			c = ("unimp","0x%x", lambda n,k,c: c)
		else:
			c = self._mnemonics[self.code]
		op = c[0]
		fmt = c[1]
		if len(c) == 3:
			v = c[2](n,self.k,self.code)
		else:
			v = self.k

		#  hack
		if '%' in fmt:
			operand = fmt % v
		else:
			operand = fmt
		if self._class() == self.BPF_JMP and self._op() != self.BPF_JA:
			return {'mnemonic':{'line':n, 'op':op, 'operand':operand, 'jt':n + 1 + int(self.jt), 'jf':n + 1 + int(self.jf)},'raw':{'code':self.code,'n':n,'k':self.k,'v':v}}
		else:
			return {'mnemonic':{'line':n, 'op':op, 'operand':operand},'raw':{'code':self.code,'n':n,'k':self.k,'v':v}}

	@staticmethod
	def _image(a):
		if len(a) == 3:
			return "({line:03d}) {op:<8s} {operand:s}".format(**a)
		else:
			return "({line:03d}) {op:<8s} {operand:<16s} jt {jt:d}\tjf {jf:d}".format(**a)
	
	def image(self, n=0):
		a = self.dump(n)
		return self._image(a['mnemonic'])

# required so we can access bpf_program->bf_insns
class bpf_program(ctypes.Structure):
	_fields_ = [("bf_len", ctypes.c_int),("bf_insns", ctypes.POINTER(bpf_insn))]

class bpf(object):
	pcap = ctypes.cdll.LoadLibrary("libpcap.so")
	pcap.bpf_image.restype=ctypes.c_char_p

	def pcap_compile_nopcap_errcheck(result,function,args):
		if result == -1:
			raise ValueError(args)

	pcap.pcap_compile_nopcap.errcheck = pcap_compile_nopcap_errcheck
	
	def __init__(self, snaplen=40,linktype=1,optimize=0,mask=0):
		self.program = bpf_program()
		self.snaplen = snaplen
		self.linktype = linktype
		self.optimize = optimize
		self.mask = mask

	def compile(self, pattern):
		buf = ctypes.c_char_p(pattern)
                self.pcap.pcap_compile_nopcap.argtypes = [ctypes.c_int,
                        ctypes.c_int, ctypes.c_void_p, ctypes.c_char_p,
                        ctypes.c_int, ctypes.c_int]
		self.pcap.pcap_compile_nopcap(self.snaplen, self.linktype,
                        ctypes.byref(self.program), buf, self.optimize, self.mask)

	def filter(self, buffer):

                self.pcap.bpf_filter.argtypes = [ctypes.c_void_p,
                        ctypes.c_char_p, ctypes.c_int, ctypes.c_int]
		return self.pcap.bpf_filter(self.program.bf_insns,
                        buffer, len(buffer), len(buffer))
                        

if __name__ == '__main__':

    xfilter = b'src port 445 and src net 127.0.0.0/8'
    xbuffer = b'E\x00\x00[~\x1f@\x00@\x06\xbe{\x7f\x00\x00\x01\x7f\x00\x00\x01\x01\xbd\xb0~\xe5#Q\xd3\xe5g\xce\xcf\x80\x18\x01\x82\xfeO\x00\x00' # IPv4/TCP


    e = bpf(linktype=12) # Raw on Linux
    e.compile(xfilter)

    if e.filter(xbuffer) > 0:
        print 'MATCHED'
    else:
        print 'DID NOT MATCH'
