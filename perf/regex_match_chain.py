#!/usr/bin/env python
"""Simple performance-testing script for chain topology."""

import sys
import copy
import time
import random
import logging

import os
from collections import OrderedDict

import ndb
from ndb.topology import NDBTopoFastSort
import ndb.topo_sort as topo_sort
from common_analysis import Analysis
from stat_fcns import STAT_FCNS

import regex.minregex as minre
from regex.minregex import Regex, Sequence, Postcard, Repetition

lg = logging.getLogger("regex_match_chain")


DEF_DATA_EXT = 'lats'  # latencies

DEF_CHAIN_LEN = 32
DEF_ITERS = 100
INT_ITERS = 100 # no. of internal iterations per time calculation
IGN_ITERS = 10 #no. of initial iterations to ignore (to get pypy to "warm up")


# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'chain': (lambda options: chain_analysis_all(options)),
}
ALL_DATA_ANALYSIS_NAMES = DATA_ANALYSIS_FCNS.keys()

is_pypy = '__pypy__' in sys.builtin_module_names

if is_pypy:
    MATCH_FCNS = OrderedDict([
        ('minre_match', minre.packet_path_match),
        ])
else:
    import ndb.filter as filter
    MATCH_FCNS = OrderedDict([
        ('old_match', filter.packet_path_match),
        ('minre_match', minre.packet_path_match),
        ])

DPI = 300

PPF_TYPES = ['.*X$', 'X.*X$', '.*X.*', 'X.*X.*X$']

# TODO: Generate more PPFs
def gen_minre(ppf_type, chain_len):
    n = int(chain_len)
    if ppf_type == '.*X$':
        return Sequence(Repetition(Postcard('.')),
                Postcard('--bpf ip --dpid %d --inport 2' % n))
    elif ppf_type == '.*X.*':
        return Sequence(Sequence(Repetition(Postcard('.')),
            Postcard('--bpf ip --dpid %d --inport 2' % (int(n/2) + 1))),
            Repetition(Postcard('.')))
    elif ppf_type == 'X.*X$':
        return Sequence(Sequence(Postcard('--bpf ip --dpid 1 --inport 1'), 
            Repetition(Postcard('.'))),
            Postcard('--bpf ip --dpid %d --inport 2' % n))
    elif ppf_type == 'X.*X.*X$':
        return Sequence(Sequence(Sequence(Sequence(Postcard('--bpf ip --dpid 1 --inport 1'), 
            Repetition(Postcard('.'))),
            Postcard('--bpf ip --dpid %d --inport 2' % (int(n/2) + 1))),
            Repetition(Postcard('.'))),
            Postcard('--bpf ip --dpid %d --inport 2' % n))
    else:
        print 'Unknown PPF type: %s' % ppf_type
        return None

# TODO: Generate more PPFs
def gen_ppf_str(ppf_type, chain_len):
    '''generate a list of PPF strings for a given chain length'''
    n = int(chain_len)
    if ppf_type == '.*X$':
        return '.*{{--bpf ip --dpid %d --inport 2}}$' % n
    elif ppf_type == '.*X.*':
        return '.*{{--bpf ip --dpid %d --inport 2}}.*' % (int(n/2) + 1)
    elif ppf_type == 'X.*X$':
        return '{{--bpf ip --dpid 1 --inport 1}}.*{{--bpf ip --dpid %d --inport 2}}$' % n
    elif ppf_type == 'X.*X.*X$':
        assert chain_len >= 3
        return '{{--bpf ip --dpid 1 --inport 1}}.*{{--bpf ip --dpid %d --inport 2}}.*{{--bpf ip --dpid %d \
    --inport 2}}$' % (int(n/2) + 1, n)
    return ''

class RegexMatchChainAnalysis(Analysis):

    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        from ndb.analysis.plot_defaults import apply_cdf_plot_defaults
        import matplotlib.pyplot as plt
        from ndb.plot.plot_helper import plotCDF
        from ndb.plot.plot_helper import linestyleGenerator, colorGenerator

        apply_cdf_plot_defaults()
        lsgen = linestyleGenerator()

        # data[analysis_name][match_fcn_name][ppf_type]
        for analysis_name, analysis_data in data.iteritems():
            plot_data_series = []
            for match_fcn_name, match_fcn_data in analysis_data.iteritems():
                linestyle = lsgen.next()
                cgen = colorGenerator()

                for ppf_type, ppf_data in match_fcn_data.iteritems():
                    yvals = [x * 1e6 for x in ppf_data]
                    plot_data_series.append({
                        'label': '%s-%s' % (ppf_type, match_fcn_name),
                        'y': yvals,
                        'opts': {'linestyle': linestyle, 'color': cgen.next()}
                    })

            fig = plotCDF(data = plot_data_series,
                    title = '%s latency' % analysis_name,
                    xlabel = 'latency (us)',
                    ylabel = 'fraction',
                    step = False,
                    xscale = 'log')
        
            if save_plot:
                if not os.path.exists(plot_dir):
                    os.makedirs(plot_dir)
                filepath = os.path.join(plot_dir, analysis_name + '.' + self.plot_ext)
                print "Writing file to %s" % filepath
                fig.savefig(filepath, dpi = DPI)
            if show_plot:
                plt.show()
    
            plt.close(fig)


    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        opts.add_option("--chain_len", type = 'int',
                        default = DEF_CHAIN_LEN,
                        help = "chain len")
        opts.add_option("--iters", type = 'int',
                        default = DEF_ITERS,
                        help = "number of iterations per test")
        opts.add_option("--match_fcns", type = 'str',
                        default = MATCH_FCNS,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


def run_test_random(match_fcn_name, ppf_type, ppf_str, ppf, chain_len, match_fcn, iters):
    '''Randomly ordered postcards.'''
    pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
    
    deltas = []
    
    for i in range(iters):
        start = time.time()
        # Run it internally a few times to get a more accurate time
        for i in xrange(INT_ITERS):
            match_result = match_fcn(ppf, pcards)
            assert match_result == True
        delta = time.time() - start
        deltas.append(delta/float(INT_ITERS))

    # ignore the first few iterations (pypy is doing some one-time stuff)
    return deltas[IGN_ITERS:]


def us_str(fraction, precision = 3):
    format_str = "%" + "0.%sf us" % str(precision)
    return format_str % (fraction * 1e6)


def chain_analysis_single(match_fcn_name, match_fcn, ppf_type, ppf_str, ppf, chain_len, iters):
    print "Running %s w/len = %s for %s iters" % (match_fcn_name, chain_len, iters)
    print "PPF TYPE: %s" % ppf_type
    print "PPF: %s" % ppf_str
    times = run_test_random(match_fcn_name, ppf_type, ppf_str, ppf, chain_len, match_fcn, iters)
    for name, fcn in STAT_FCNS.iteritems():
        print "%s: %s" % (name, us_str(fcn(times)))
    return times


def chain_analysis_all(options):
    # master dict w/all data.
    data = {}

    ppf_str_dict = {}
    for ppf_type in PPF_TYPES:
        ppf_str_dict[ppf_type] = gen_ppf_str(ppf_type, options.chain_len)

    #for match_fcn_name, match_fcn in MATCH_FCNS.iteritems():
    for match_fcn_name in options.match_fcns:
        match_fcn = MATCH_FCNS[match_fcn_name]
        print '='*50
        print '='*20, match_fcn_name, '='*20
        data[match_fcn_name] = {}
        for ppf_type, ppf_str in ppf_str_dict.iteritems():
            ppf = None
            if match_fcn_name == 'minre_match':
                ppf = gen_minre(ppf_type, options.chain_len)
            else:
                if not is_pypy:
                    if match_fcn_name == 'old_match':
                        ppf = filter.PacketPathFilter(ppf_str, open(os.path.devnull, 'w'),
                                syntax_check=False)
            vals = chain_analysis_single(match_fcn_name, match_fcn,
                    ppf_type, ppf_str, ppf,
                    options.chain_len, options.iters)
            data[match_fcn_name][ppf_type] = vals
                
    return data


if __name__ == '__main__':
    RegexMatchChainAnalysis(def_analyses = ALL_DATA_ANALYSIS_NAMES,
                          data_analysis_fcns = DATA_ANALYSIS_FCNS,
                          def_data_ext = DEF_DATA_EXT)
