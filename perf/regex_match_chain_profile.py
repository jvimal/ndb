#!/usr/bin/env python
"""Simple performance-testing script for chain topology."""

import copy
import time
import random
import logging
import os

from line_profiler import LineProfiler

import ndb.topo_sort as topo_sort
import ndb.filter as filter
import regex_match_chain
from regex_match_chain import MATCH_FCNS, gen_ppf_str

lg = logging.getLogger("topo_sort_chain")

PPF_TYPE = '.*X$'
DEF_CHAIN_LEN = 32
DEF_ITERS = 1000


def critical_region(match_fcn, ppf, pcards, iters):
    '''topo: NDBTopoFastSort'''
    for i in range(iters):
        match_result = match_fcn(ppf, pcards)


def run_test_random(ppf_str, chain_len, match_fcn, iters):
    '''Randomly ordered postcards.'''
    pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
    ppf = filter.PacketPathFilter(ppf_str, open(os.path.devnull, 'w'),
            syntax_check=False)
    critical_region(match_fcn, ppf, pcards, iters)

def do_stuff():
    ppf_str = gen_ppf_str(PPF_TYPE, DEF_CHAIN_LEN)
    run_test_random(ppf_str, DEF_CHAIN_LEN, MATCH_FCNS['match'], DEF_ITERS)


if __name__ == '__main__':
    profile = LineProfiler(filter.packet_path_match,
            filter.PacketPathFilter.match, 
            filter.PacketFilter.match, 
            critical_region)
    profile.run('do_stuff()')
    #profile.dump_stats('lp_output')
    profile.print_stats()
