# Unfortunately, numpy support in pypy is experimental.
# Since it's only used for convenience w/stats fcns, should be fine to replace.
USE_NUMPY = False

# from http://stackoverflow.com/questions/7578689/median-code-explanation
def median(a):
    ordered = sorted(a)
    length = len(a)
    return float((ordered[length/2] + ordered[-(length+1)/2]))/2

if USE_NUMPY:
    import numpy
    STAT_FCNS = {
        'median': numpy.median,
        'min': numpy.min
        #['mean', 'median', 'std', 'min', 'max']
    }
else:
    STAT_FCNS = {
        'median': median,
        'min': min
        #['mean', 'median', 'std', 'min', 'max']
    }