#!/usr/bin/env python
"""Sweep across different sizes for the chain topology."""

import logging
import os
from collections import OrderedDict

import ndb.topo_sort as topo_sort
from common_analysis import Analysis
from stat_fcns import STAT_FCNS

from topo_sort_chain import run_test_random, chain_analysis_single, us_str
from topo_sort_chain import DPI
import topo_sort_chain

lg = logging.getLogger("topo_sort_chain_sweep")


DEF_DATA_EXT = 'latsweep'  # dict of sizes to latencies

DEF_CHAIN_LENS = [2 ** i for i in range(1, 8)] + [255]
DEF_ITERS = 100
DEF_STATS_LST = STAT_FCNS.keys()


# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'chain_sweep': (lambda options: chain_sweep_all(options)),
}
ALL_DATA_ANALYSIS_NAMES = DATA_ANALYSIS_FCNS.keys()

NO_LEGEND = True


SORT_FCNS = topo_sort_chain.SORT_FCNS
#SORT_FCNS = {
#    'clean': topo_sort.topo_sort_clean_fasttopo,
#    'namedtuple': topo_sort.topo_sort_clean_namedtuple,
#}

SORT_FCNS = {
        'clean': topo_sort.topo_sort_clean_fasttopo,
        }

class TopoSortChainSweepAnalysis(Analysis):

    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        from ndb.analysis.plot_defaults import apply_time_series_plot_defaults
        import matplotlib.pyplot as plt
        from ndb.plot.plot_helper import plotTimeSeries

        apply_time_series_plot_defaults()

        for analysis_name, analysis_data in data.iteritems():

            # data[name][chain_len][stat] ...
            plot_data_series = []
            for sort_name, sort_data in analysis_data.iteritems():
                metrics = sort_data.itervalues().next().keys()
                data_series = {}
                for m in metrics:
                    data_series[m] = []
                for chain_len, chain_data in sort_data.iteritems():
                    for metric_name, metric_val in chain_data.iteritems():                
                        data_series[metric_name].append(metric_val)

                chain_lens = sort_data.keys()
                for m in metrics:
                    # stupid json stores int keys as strings... 
                    # this is a way to fix it
                    xyvals = zip(map(int, chain_lens), [d * 1e9 for d in
                        data_series[m]])
                    xyvals.sort(key=lambda t: t[0])
                    xvals, yvals = zip(*xyvals)

                    plot_data_series.append({
                        'label': sort_name + ' ' + m,
                        'x': xvals,
                        'y': yvals,
                        'opts': {'marker': 'o'},
                    })

            fig = plotTimeSeries(data = plot_data_series,
                title = '%s latency' % analysis_name,
                xlabel = 'Packet History Length',
                ylabel = 'History Assembly Latency (ns)',
                step = False,
                xscale = 'log',
                yscale = 'log')

            if NO_LEGEND:
                lgd = plt.legend()
                lgd.set_visible(False)
        
            if save_plot:
                if not os.path.exists(plot_dir):
                    os.makedirs(plot_dir)
                filepath = os.path.join(plot_dir, analysis_name + '.' + self.plot_ext)
                print "Writing file to %s" % filepath
                fig.savefig(filepath, dpi = DPI)
            if show_plot:
                plt.show()
    
            plt.close(fig)


    def process_options(self, options):
        options = super(TopoSortChainSweepAnalysis, self).process_options(options)
        # Convert from comma-sep string into list
        if options.chain_lens:
            options.chain_lens = options.chain_lens.split(',')
            options.chain_lens = [int(i) for i in options.chain_lens]
        else:
            options.chain_lens = DEF_CHAIN_LENS
        return options


    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        opts.add_option("--chain_lens", type = 'str',
                        default = None,
                        help = "comma-sep list of chain lens")
        opts.add_option("--iters", type = 'int',
                        default = DEF_ITERS,
                        help = "number of iterations per test")
        opts.add_option("--stats", type = 'str',
                        default = DEF_STATS_LST,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


def chain_sweep_all(options):
    # master dict w/all data.
    data = {}
    for name, sort_fcn in SORT_FCNS.iteritems():
        data[name] = OrderedDict({})
        for chain_len in options.chain_lens:
            data[name][chain_len] = {}
            vals = chain_analysis_single(name, sort_fcn, chain_len, options.iters)
            for stat in options.stats:
                fcn = STAT_FCNS[stat]
                stat_val = fcn(vals)
                data[name][chain_len][stat] = stat_val
            
    return data


if __name__ == '__main__':
    TopoSortChainSweepAnalysis(def_analyses = ALL_DATA_ANALYSIS_NAMES,
                              data_analysis_fcns = DATA_ANALYSIS_FCNS,
                              def_data_ext = DEF_DATA_EXT)
