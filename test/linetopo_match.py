#!/usr/bin/python

import sys
sys.path.append('../')

import os
import dpkt
import argparse
from collections import namedtuple
import random
import datetime
import json

import master
from master import Master
# TODO: rename mn-linetopo
linetopo = __import__('mn-linetopo')
from dump_topo import dump_out_band_topo
from filter import PacketPathFilter 
from collector import get_tag
from topology import NDBTopo

def parse_args():
    parser = argparse.ArgumentParser(description="Test packet path matching time")
    parser.add_argument('--n', '-n',
                        dest="n",
                        type=int,
                        help="Length of the linetopo",
                        default=2)

    parser.add_argument('--pcap', '-p',
                        dest="pcap",
                        help="Pcap file of the collected postcards",
                        required=True)

    parser.add_argument('--psid', 
                        dest="psid",
                        help="json file of the psid->dpid mapping",
                        required=True)

    parser.add_argument('--randomize', '-r',
                        dest="randomize",
                        action="store_true",
                        help="Randomize postcards?",
                        default=False)

    args = parser.parse_args()
    return args

def create_postcards(filename, randomize=False):
    pcards = []
    pcapReader = dpkt.pcap.Reader(file(filename))
    for ts_, data in pcapReader:
        pkt = dpkt.ethernet.Ethernet(data)
        pcards.append({
                'pkt': pkt,
                'tag': get_tag(pkt)
                })
    if randomize:
        random.shuffle(pcards)
    return pcards

if __name__ == '__main__':
    args = parse_args()
    topo = linetopo.LineTopo(args.n)
    topo_json = dump_out_band_topo(topo, os.path.devnull)
    psid_json = json.load(open(args.psid))
    psid_to_dpid = {}
    for k, v in psid_json.iteritems():
        psid_to_dpid[int(k)] = v

    MasterArgs = namedtuple('MasterArgs', ['db_host', 'db_port',
        'debug', 'pcap_out'])

    ma = MasterArgs(db_host='localhost',
            db_port=27017,
            debug=False,
            pcap_out=None,
            )

    c = Master(ma)
    c.topo = NDBTopo(topo_json=topo_json)
    c.psid_to_dpid = psid_to_dpid

    ppf_str = '.*{{--dpid %d --inport 1}}' % args.n
    ppf = PacketPathFilter(ppf_str, open(os.path.devnull, 'w'))
    pcards = create_postcards(args.pcap, args.randomize)

    t1 = datetime.datetime.now()

    pp = c.topo_sort(pcards)
    c.clean_dstmac(pp)
    ppf.match(pp)

    t2 = datetime.datetime.now()
    print (t2 - t1).total_seconds()


